import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsistenciaHistoricoComponent } from './asistencia-historico.component';

describe('AsistenciaHistoricoComponent', () => {
  let component: AsistenciaHistoricoComponent;
  let fixture: ComponentFixture<AsistenciaHistoricoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsistenciaHistoricoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsistenciaHistoricoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

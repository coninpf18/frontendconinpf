import { SesionService } from '../../../../servicios/sesion.service';
import { Component, OnDestroy, OnInit, Input, OnChanges, Inject, LOCALE_ID } from '@angular/core';
import { formatDate } from '@angular/common';
import { AsistenciaService, Asistencia, CausaInasistencia } from '../../../../servicios/asistencia.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-asistencia-historico',
  templateUrl: './asistencia-historico.component.html',
  styleUrls: ['./asistencia-historico.component.css']
})
export class AsistenciaHistoricoComponent implements OnDestroy, OnChanges, OnInit {

  @Input() idPac: number;
  private historicoAsistencias: Asistencia[] = [];
  private unsubscribeX: Subscription = new Subscription();
  private causasInasistencia: CausaInasistencia[] = [];
  private causaSeleccionada: CausaInasistencia;
  private asistSeleccionada: Asistencia = { "fechaAsistencia": "2018-10-10", "asistio": false };
  //@Inject(LOCALE_ID) public locale: string;

  constructor(private sesionSrv: SesionService, private asistSrv: AsistenciaService, private router: Router, @Inject(LOCALE_ID) private locale: string) { }

  ngOnInit() {
    this.getHistorico();
    this.buscarCausasInasistencia();
  }

  ngOnDestroy() {
    this.unsubscribeX.unsubscribe();
  }

  ngOnChanges() {
    // this.getHistorico();
  }

  buscarCausasInasistencia() {
    let unsubscribe =
      this.asistSrv.getCausasInasistencia().subscribe((data: any) => {
        new Promise((resolve) => { resolve(data) }).then((data: any) => {
          this.causasInasistencia = [];
          this.unsubscribeX.add(unsubscribe);
          data.contenido.forEach(causa => {
            this.causasInasistencia.push(causa);
          })

          console.log(this.causasInasistencia);

        })
      }, error => {
        this.unsubscribeX.add(unsubscribe);
        console.log("Error obteniendo las causas de inasistencia ", error)
      });
  }

  getHistorico() {
    let unsubscribe =
      this.asistSrv.getHistoricoAsistencia(this.idPac)
        .subscribe((data: Asistencia[]) => {
          new Promise((resolve) => { resolve(data) })
            .then((data: Asistencia[]) => {
              this.historicoAsistencias = [];

              console.log(data);

              this.unsubscribeX.add(unsubscribe);
              data.forEach((asist: Asistencia) => {
                this.historicoAsistencias.push(asist);
              });

            })
        }, error => {
          this.unsubscribeX.add(unsubscribe)
          console.log("Error obteniendo el historial de asist", error)
        });
  }

  getCausaInasistencia(idCausa: number): string {
    let causa = this.causasInasistencia.find(causa => causa.idCausaInasistencia == idCausa);
    if (causa == null) {
      return "";
    }
    return this.causasInasistencia.find(causa => causa.idCausaInasistencia == idCausa).nombreCausaInasistencia;
  }

  setAsistSeleccionada(asist: Asistencia) {
    this.asistSeleccionada = asist;
    this.causaSeleccionada = this.causasInasistencia.find(causa => causa.idCausaInasistencia == asist.causaInasistencia);
  }

  editarCausaInasistencia() {
    let idCausa = this.causaSeleccionada.idCausaInasistencia;
    let idAsist = this.asistSeleccionada.idAsistencia;
    if (idCausa != this.asistSeleccionada.causaInasistencia) { //cambió
      let unsubscribe = this.asistSrv.putEditarCausaInasistencia(idCausa, idAsist).subscribe(() => {
        this.unsubscribeX.add(unsubscribe);
        Swal({ title: 'Causa de la inasistencia del día ' + formatDate(this.asistSeleccionada.fechaAsistencia.slice(0, 10), 'EEEE, d \'de\' MMMM', this.locale) + " asignada.", type: 'success', showConfirmButton: true });
        this.getHistorico();
      }, () => this.unsubscribeX.add(unsubscribe));
    }

  }
}

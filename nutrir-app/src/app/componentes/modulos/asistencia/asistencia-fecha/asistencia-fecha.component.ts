import { SesionService } from '../../../../servicios/sesion.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AsistenciaService } from '../../../../servicios/asistencia.service';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { PacienteService, Paciente } from '../../../../servicios/paciente.service';
import { Subscription } from 'rxjs';
import { DiaService } from '../../../../servicios/dia.service';

@Component({
selector: 'app-asistencia-fecha',
templateUrl: './asistencia-fecha.component.html'
})

export class AsistenciaFechaComponent implements OnDestroy, OnInit {

  private fecha= new Date();
  private fechaACastear: any;
  private fechaParaBuscar: Date;
  private fechaBusqueda: any;
  private unsubscribeY: Subscription= new Subscription();
  private diaSemana: number;
  private errorAsistencia: boolean;
  private pacientes: Paciente[];
  private asistenciaForm = new FormGroup({
    fechaBuscarAsistencias: new FormControl()
  });
  private rol:any=""

  constructor(private sesionSrv: SesionService, private asistSrv: AsistenciaService, private router: Router, 
              private pacienteService: PacienteService, private _diaSrv: DiaService) {
                this.rol=sesionSrv.getProfesional().user.username
  }

  ngOnInit(){
    console.log("dentro de asistencia fecha", this.rol);
    
    this.errorAsistencia= false;
  }

  ngOnDestroy(){
    if(this.unsubscribeY){
      this.unsubscribeY.unsubscribe();
    }
  }

  calcularEdad(fechaNacimiento) {
    return this.pacienteService.calcularEdad(fechaNacimiento);
  }

  navegarFechaBusqueda(fechaBuscarAsistencias) {
    this.router.navigate(['home','asistencia-fecha'], {queryParams: {fechaBuscar : fechaBuscarAsistencias }})
  }

  asignarFecha(fecha: string){
 
    this.fechaBusqueda= fecha
    let fechaString: string= this.fechaBusqueda
    this.fechaACastear= this.fechaBusqueda + 'T00:00:00-03:00';
    this.fechaBusqueda= new Date(this.fechaACastear)
   
    this.diaSemana= this.fechaBusqueda.getDay(); 

    //  Solucione el problema suscribiendome tanto en el acierto como error, 
    //  sólo me tomaba la subscripción en el acierto, no error por eso se trababa.
      this.unsubscribeY= this.asistSrv.getAsistentesFecha(fechaString).subscribe((datos: Paciente[])=> {
        
        this.pacientes= [];
        this.asistSrv.subject.next(
          datos.forEach(paciente=> {
            this.pacientes.push(paciente)
          })
        )
    }, ()=> {
      this.asistSrv.subject.next(
        this.errorAsistencia= true
      )
    });
  }

  irPaciente(idPaciente: number){
    this.router.navigate(['home','paciente-detalle'], {queryParams: {idPac: +idPaciente}});
  }

}

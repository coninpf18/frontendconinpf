import { Component, OnInit, Input, OnChanges, OnDestroy, EventEmitter, Output } from '@angular/core';
import { DiaService, Dia } from '../../../../servicios/dia.service';
import { AsistenciaService } from '../../../../servicios/asistencia.service';
import { NotificacionService } from '../../../../servicios/notificacion.service';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modificar-dias',
  templateUrl: './modificar-dias.component.html'
})
export class ModificarDiasComponent implements OnInit, OnChanges, OnDestroy {

  @Input() idPaciente: number;
  private diasPacientes: Dia[];
  private unsubscribe: Subscription= new Subscription();
  @Output() propagarDiasVisita= new EventEmitter<Dia>();
  
  constructor( private diaService: DiaService, private asistenciaService: AsistenciaService, 
               private notifSrv: NotificacionService) {

  }

  ngOnInit() {
    this.getDiaPaciente();
  }

  ngOnChanges() {
    this.getDiaPaciente();
  }

  ngOnDestroy(){
    this.unsubscribe.unsubscribe();
  }

  getDiaPaciente(){
    let unsubscribe= this.diaService.getDiaPaciente(this.idPaciente).subscribe((data: any)=> {
        this.diasPacientes= [];
        data.forEach(diaPaciente => {
          this.diasPacientes.push(diaPaciente)
        })
        this.unsubscribe.add(unsubscribe)
      }, ()=> this.unsubscribe.add(unsubscribe));
  }

  asignarAsistencia(idDia: number, evento: boolean){
    if(evento=== true){
      let unsubscribe= this.asistenciaService.postGenerarAsistente(this.idPaciente, idDia).subscribe(()=> {
        this.unsubscribe.add(unsubscribe);
        Swal({title: 'Día agregado.', type: 'success', showConfirmButton: true});
        for(let i=0; i< this.diasPacientes.length; i++){
          if(this.diasPacientes[i].idDia=== idDia){
            this.propagarDiasVisita.emit(this.diasPacientes[i]);
          }
        }
      }, ()=> this.unsubscribe.add(unsubscribe));
    }else {
      let unsubscribe= this.asistenciaService.eliminarDiaAsistencia(this.idPaciente, idDia).subscribe(()=> {
        this.unsubscribe.add(unsubscribe)
        Swal({title: 'Día eliminado.', type: 'success', showConfirmButton: true});
        for(let i=0; i< this.diasPacientes.length; i++){
          if(this.diasPacientes[i].idDia=== idDia){
            this.propagarDiasVisita.emit(this.diasPacientes[i]);
          }
        }
      }, ()=> this.unsubscribe.add(unsubscribe));
    }
  }
}



import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { AsistenciaService } from '../../../servicios/asistencia.service';
import { FormGroup, FormControl } from '@angular/forms';
import { PacienteService, Paciente } from '../../../servicios/paciente.service';
import { Router } from '@angular/router'
import { DiaService } from '../../../servicios/dia.service';
import { AreaService, Area } from '../../../servicios/area.service';
import { SesionService } from '../../../servicios/sesion.service';
import { AsistenciaFechaComponent } from './asistencia-fecha/asistencia-fecha.component'
import { formatDate } from '@angular/common';
import { NotificacionService } from '../../../servicios/notificacion.service';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2';

declare let $: any;

@Component({
  selector: 'app-asistencia',
  templateUrl: './asistencia.component.html'
})

export class AsistenciaComponent implements OnInit, OnDestroy {

  @ViewChild('child') child: AsistenciaFechaComponent;
  private mostrarAsist: boolean = false;
  private fecha = formatDate(new Date(), 'yyyy-MM-dd HH:mm:ssZ', "en-US");
  private fechaString: string = formatDate(new Date(), 'yyyy-MM-dd HH:mm:ssZ', "en-US");
  private diaSemana = new Date().getDay();
  private diaNombre = this.diaPalabras(this.diaSemana);
  private pacientes: Paciente[];
  private loading: boolean;
  private areas: any[];
  private errorPacientes: boolean;
  private unsubscribeX: Subscription = new Subscription();
  private fechaFiltro: string;
  private asistenciaForm = new FormGroup({ fechaBuscarAsistencias: new FormControl() });
  private rol: string;

  constructor(private asistSrv: AsistenciaService, private pacienteService: PacienteService,
    private areaService: AreaService, private router: Router, private diaService: DiaService,
    private _notifSrv: NotificacionService, private _sesionSrv: SesionService) {

    this.loading = true;
    pacienteService.setPacNavbar(null);
    _sesionSrv.setNombreModulo('Asistencia');

  }

  ngOnInit() {

    this.rol = this._sesionSrv.getProfesional().user.username;

    this.getPacientes();
    this.getAreaPrincipal();

    let ayer = new Date();
    ayer.setDate(ayer.getDate() - 1);
    this.fechaFiltro = ayer.toISOString().slice(0, 10);

  }

  ngOnDestroy() {

    console.log('destruye el componente asistencia.ts');

    if (this.unsubscribeX) {
      this.unsubscribeX.unsubscribe();
    }

  }

  getPacientes() {
    let fechaAsistencia: string = this.fecha.slice(0, 10);
    this.pacientes = [];

    this.unsubscribeX = this.asistSrv.getAsistentesFecha(fechaAsistencia).subscribe((datos: Paciente[]) => {
      new Promise((resolve) => { resolve(datos) }).then(() => {
        datos.forEach(paciente => {
          this.pacientes.push(paciente)
        })
      })
    }, error => {
      this.errorPacientes = true;
      console.log("Error obteniendo Pacientes de hoy", error)
    });
  }

  getAreaPrincipal() {

    let unsubscribe =
      this.areaService.getAreaPrincipal()
        .subscribe((data: Area[]) => {

          this.areas = [];
          this.unsubscribeX.add(unsubscribe);

          data.forEach(area => {
            this.areas.push(area);
          });

          this.loading = false;
          
        }, error => {

          this.loading = false;
          this.unsubscribeX.add(unsubscribe)
          console.log("Error obteniendo area principal del profesional", error)

        });

  }

  generarAsistente(idPaciente: number, diaAsistencia) {
    let unsubscribe = this.asistSrv.postGenerarAsistente(idPaciente, diaAsistencia).subscribe((data: JSON) => {
      console.log(data)
      this.unsubscribeX.add(unsubscribe)
    }, () => this.unsubscribeX.add(unsubscribe));
  }

  calcularEdad(fechaNacimiento) {
    let edadPaciente = this.pacienteService.calcularEdad(fechaNacimiento);
    return edadPaciente;
  }

  modificarAsistencia(paciente: Paciente, asistio: boolean) {

    console.log(paciente);
    let unsubscribe = this.asistSrv.postMarcarAsistencia(asistio, this.fechaString, paciente.idPaciente).subscribe(() => {
      this.unsubscribeX.add(unsubscribe);
      if (asistio === true) {
        Swal({ title: 'Asistencia asignada a ' + paciente.nombrePaciente + ' ' + paciente.apellidoPaciente + '.', type: 'success', showConfirmButton: true })
      } else {
        Swal({ title: 'Asistencia desasignada a ' + paciente.nombrePaciente + ' ' + paciente.apellidoPaciente + '.', type: 'success', showConfirmButton: true })
      }
    }, () => this.unsubscribeX.add(unsubscribe));
  }

  navegarAsistenciaArea(idArea: number) {
    this.router.navigate(['home', 'asistencia-area'], { queryParams: { areaProf: idArea } })
  }

  ocultarCollapse() {
    $(document).on('click', '#ocultarBoton', function () {
      $('#verAsistencia').collapse('hide');
      $('#buscarBoton').prop('disabled', false);
      $('#fechaInput').prop('disabled', false);
    });
  }

  diaPalabras(diaSemana): String {
    return this.diaService.diaPalabras(diaSemana);
  }

  asignarFecha(formFecha) {
    let fechaHoy = formatDate(new Date(), 'yyyy-MM-dd HH:mm:ssZ', "en-US")

    if (fechaHoy <= formFecha.value.fecha) {
      Swal({ title: 'Ingrese una fecha ya transcurrida.', type: 'warning', showConfirmButton: true });
    } else {
      $('#buscarBoton').prop('disabled', true);
      $('#fechaInput').prop('disabled', true);
      this.child.asignarFecha(formFecha.value.fecha); //método child.
      this.asistSrv.observable.subscribe(() => $(function () { $('#verAsistencia').collapse('show') }))
    }
  }

  irPaciente(idPaciente: number) {
    this.router.navigate(['home', 'paciente-detalle'], { queryParams: { idPac: +idPaciente } });
  }

}

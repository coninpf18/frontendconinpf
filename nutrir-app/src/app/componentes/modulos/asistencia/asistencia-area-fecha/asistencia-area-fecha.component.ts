import { SesionService } from '../../../../servicios/sesion.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AsistenciaService } from '../../../../servicios/asistencia.service';
import { PacienteService, Paciente } from '../../../../servicios/paciente.service';
import { AreaService } from '../../../../servicios/area.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DiaService } from '../../../../servicios/dia.service';

@Component({
selector: 'app-asistencia-area-fecha',
templateUrl: './asistencia-area-fecha.component.html'
})

export class AsistenciaAreaFechaComponent implements OnDestroy, OnInit {

  private fechaString: string;
  private asistentes: Paciente[];
  private pacientesConResponsable: Paciente[];
  private areaProfesional: string;
  private unsubscribe: Subscription= new Subscription();
  private errorAsistencia: boolean;
  private diaSemana: number;

  constructor(private sesionSrv: SesionService, private asistSrv: AsistenciaService, private pacienteService: PacienteService, 
              private areaService: AreaService, private router: Router, private _diaSrv: DiaService) {
      
                
  }

  ngOnInit(){
    this.errorAsistencia= false;
  }

  ngOnDestroy(){
    if(this.unsubscribe){
      this.unsubscribe.unsubscribe();
    }
  }

  asignarFecha(fechaBusquedaString: string, idArea: number){

    this.fechaString= fechaBusquedaString;
    let fechaString= fechaBusquedaString;
    let fechaBusqueda: Date= new Date(fechaString + 'T00:00:00-03:00');
    
    
    this.diaSemana= fechaBusqueda.getDay();

    let unsubscribe= this.areaService.getAreaProfesional(idArea).subscribe((data: any) => {
        this.areaProfesional= data.Area.nombreArea;
        this.unsubscribe.add(unsubscribe);
    });

    //  Solucione el problema suscribiendome tanto en el acierto como error, 
    //  sólo me tomaba la subscripción en el acierto, no error por eso se trababa.
    this.unsubscribe= this.asistSrv.getAsistentesArea(this.fechaString, idArea).subscribe((datos: Paciente[]) => {
        this.pacientesConResponsable= [];
        this.asistSrv.subject2.next(
          datos.forEach((paciente)=>{
            this.pacientesConResponsable.push(paciente);  
          })
        );
    }, ()=> this.asistSrv.subject2.next(this.errorAsistencia= true));
  }

  calcularEdad(fechaNacimiento) {
    return this.pacienteService.calcularEdad(fechaNacimiento);
  }

  irPaciente(idPaciente: number){
    this.router.navigate(['home','paciente-detalle'], {queryParams: {idPac: +idPaciente}});
  }

}

import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PacienteService, Paciente } from 'src/app/servicios/paciente.service';
import { AsistenciaService } from 'src/app/servicios/asistencia.service';
import { DiaService } from '../../../../servicios/dia.service';
import { AreaService } from '../../../../servicios/area.service';
import { SesionService } from '../../../../servicios/sesion.service';
import { formatDate } from '@angular/common';
import { NgForm } from '@angular/forms';
import { NotificacionService } from '../../../../servicios/notificacion.service';
import { AsistenciaAreaFechaComponent } from '../asistencia-area-fecha/asistencia-area-fecha.component';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2';

declare let $ : any;

@Component({
  selector: 'app-asistencia-area',
  templateUrl: './asistencia-area.component.html'
})

export class AsistenciaAreaComponent implements OnInit, OnDestroy {

  @ViewChild('child') child: AsistenciaAreaFechaComponent;
  private pacientes: Paciente [];
  private fechaString: string= formatDate(new Date(),'yyyy-MM-dd HH:mm:ssZ',"en-US");
  private diaSemana= new Date().getDay();
  private idAreaProfesional: number;
  private areaProfesional: string;
  private fechaBusqueda: string;
  private loading: boolean;
  private noHayPacientes: boolean;
  private unsubscribe: Subscription= new Subscription();
  private fechaFiltro: string;

  constructor(private asistSrv: AsistenciaService, private pacienteService: PacienteService, private router: Router,
              private route: ActivatedRoute, private diaService: DiaService, private areaService: AreaService,
              private _notifSrv: NotificacionService, private sesionServ: SesionService) {
               
              }

  ngOnInit(){

    this.loading= true;

    this.route.queryParams.subscribe(params=> {
      this.idAreaProfesional= params['areaProf'];
     
    });

    this.unsubscribe= this.areaService.getAreaProfesional(this.idAreaProfesional).subscribe((data: any)=> {
        this.areaProfesional= data.Area.nombreArea;
    });

    let ayer= new Date();
    ayer.setDate(ayer.getDate() - 1);
    this.fechaFiltro= ayer.toISOString().slice(0,10);

    let unsubscribe= this.asistSrv.getAsistentesArea(this.fechaString.slice(0,10), this.idAreaProfesional).subscribe((data: Paciente[])=> {
        this.pacientes= [];
        data.forEach(paciente => {
          this.pacientes.push(paciente)
        })
        this.loading= false;
        this.noHayPacientes= false;
        this.unsubscribe.add(unsubscribe);
        console.log(this.pacientes[0]);
    }, (error)=> {
      this.loading= false;
      this.unsubscribe.add(unsubscribe)
      this.noHayPacientes= true;
      console.log(error, 'traer pacientes.');
    });
  }

  ngOnDestroy(){
    this.unsubscribe.unsubscribe();
  }

  modificarAsistencia(paciente: Paciente, asistio: boolean) {

    let unsubscribe= this.asistSrv.postMarcarAsistenciaArea(asistio, this.fechaString, paciente.idPaciente, this.idAreaProfesional).subscribe(() => {
      this.unsubscribe.add(unsubscribe)
      if(asistio=== true){
        Swal({title: 'Asistencia asignada a '+paciente.nombrePaciente + ' ' + paciente.apellidoPaciente + ' en '+ this.areaProfesional +'.', type: 'success', showConfirmButton: true})
      } else{
        Swal({title: 'Asistencia desasignada a ' + paciente.nombrePaciente + ' ' + paciente.apellidoPaciente + ' en ' +this.areaProfesional+'.', type: 'success', showConfirmButton: true})
      }
    }, (error)=> {
      Swal({title: 'Error al modificar la asistencia. Intente nuevamente.', type: 'error', showConfirmButton: true})
      console.log(error, 'error put asistencia-area');
      
    }, ()=> this.unsubscribe.add(unsubscribe));
  }

  calcularEdad(fechaNacimiento) {
    let edadPaciente= this.pacienteService.calcularEdad(fechaNacimiento);
    return edadPaciente;
  }

  diaPalabras(diaSemana): string {
    return this.diaService.diaPalabras(diaSemana);
  }

  navegarFechaBusqueda(formFecha: NgForm) {
    this.fechaBusqueda= formFecha.value.fecha
    let fechaHoy= formatDate(new Date(),'yyyy-MM-dd HH:mm:ssZ',"en-US")
   
    if(fechaHoy <= formFecha.value.fecha){
      Swal({title: 'Ingrese una fecha transcurrida.', type: 'warning', showConfirmButton: true});

    } else{
      $('#buscarBoton').prop('disabled', true);
      $('#inputFecha').prop('disabled', true);
      this.child.asignarFecha(this.fechaBusqueda, this.idAreaProfesional); // método child.

      let unsubscribe= this.asistSrv.observable2.subscribe(()=> {
        $(function() { $('#collapseAsistAreaFecha').collapse('show') })
        this.unsubscribe.add(unsubscribe)
      })
    }
  }

  ocultarCollapse(){
    $(document).on('click', '#ocultarBoton', function(){
      $('#collapseAsistAreaFecha').collapse('hide');
      $('#buscarBoton').prop('disabled', false);
      $('#inputFecha').prop('disabled', false);
    });
  }

  irPaciente(idPaciente: number){
    this.router.navigate(['home','paciente-detalle'], {queryParams: {idPac: +idPaciente}});
  }

  navegarAsistencia() {
    this.router.navigate(['home','asistencia'])
  }

}

import { SesionService } from '../../../../../servicios/sesion.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { PacienteService, Paciente } from "../../../../../servicios/paciente.service";
import { DomicilioService, Domicilio, Pais, Provincia, Localidad } from '../../../../../servicios/domicilio.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { CentroService, Centro } from '../../../../../servicios/centro.service';
import { NotificacionService } from '../../../../../servicios/notificacion.service';
import { Observable, Subscription } from 'rxjs';
import { FamiliaService } from 'src/app/servicios/familia.service';
import Swal, { SweetAlertResult } from 'sweetalert2'
import { formatDate } from '@angular/common';

declare let $: any;
const maxDateValue: string = formatDate(new Date(), 'yyyy-MM-dd', "en-US")

@Component({
  selector: 'app-nuevointegrante',
  templateUrl: './nuevoIntegrante.component.html',
  styles: [`
  input.ng-invalid.ng-touched{ 
    border: 1px solid red;
  }
  select.ng-invalid.ng-touched{ 
    border: 1px solid red;
  }
  .dropdownSize{
    height: 10px;
    width: 194px;
  }
  ` ]
})

export class NuevoIntegranteComponent implements OnInit, OnDestroy {

  placeholder_tipopac: string = "Niño: Integrante menor a 5 años. \nMujer: Integrante femenina mayor de 9 años. \nOtro: Integrante que no es del tipo niño o mujer."
  private idFam: number
  private formulariointegrante: FormGroup;
  private residenciaFormulario: FormGroup;
  private responsableFormulario: FormGroup;
  private tiposPac: Array<{ id: number, nombre: string }> = [];
  private listaCentros: Centro[];
  private listaPaises: Pais[];
  private listaProvincias: Provincia[];
  private listaLocalidades: Localidad[];
  private listaIntegrantes: Array<{ id: number, nombre: string }> = [];
  private listaTiposrel: Array<{ id: number, nombre: string }> = [];
  private domicilioActualizado: number;
  private loading: boolean;
  private domiciliosFam: Domicilio[];
  private promesaPostPaciente: Observable<number> = new Observable();
  private domicilio: Domicilio;
  private mostrarDesahacer: boolean;
  private unsubscribe: Subscription = new Subscription();
  private fechaFiltro: string;

  constructor(private pacServ: PacienteService, private domServ: DomicilioService,
    private route: ActivatedRoute, private router: Router,
    private _centro: CentroService, private _notifSrv: NotificacionService, private famServ: FamiliaService,
    private sesionSrv: SesionService) {
    sesionSrv.nombreModulo = "Familia";

    this.formulariointegrante = new FormGroup({
      idPaciente: new FormControl(null),
      nombrePaciente: new FormControl('', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ \s]*')]),
      apellidoPaciente: new FormControl('', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ \s]*')]),
      dniPaciente: new FormControl(null, [Validators.pattern('[0-9]*'), Validators.minLength(7)]),
      nroHistoriaClinica: new FormControl(''),
      telefonoPaciente: new FormControl('', [Validators.pattern('[0-9]*'), Validators.minLength(6)]),
      tipoPaciente_id: new FormControl(null, Validators.required),
      fechaNacimiento: new FormControl('', Validators.required),
      centro: new FormControl(null, Validators.required),
      sexo: new FormControl(null, Validators.required),
      grupoFamilia: new FormControl(null, Validators.required),
      domicilio_id: new FormControl(null),
      pais: new FormControl(null, Validators.required),
    }, { validators: this.edadTipoPac });

    this.residenciaFormulario = new FormGroup({
      domicilioForm: new FormGroup({
        idDomicilio: new FormControl(null),
        calle: new FormControl('', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ \s]*')]),   //Agregar validación.
        departamento: new FormControl(null, [Validators.pattern('[0-9]*'), Validators.min(-1)]),
        nroDomicilio: new FormControl(null, [Validators.required, Validators.pattern('[0-9]*'), Validators.min(0)]),
        piso: new FormControl(null, [Validators.pattern('[0-9]*'), Validators.min(-1)]),
        localidad: new FormGroup({
          idLocalidad: new FormControl(null, Validators.required),
          nombreLocalidad: new FormControl(''),
          provincia: new FormControl(null)
        }),
      }),

      provinciaForm: new FormGroup({
        idProvincia: new FormControl(null, Validators.required),
        nombreProvincia: new FormControl(''),
        pais: new FormControl(null)
      }),

      paisForm: new FormGroup({
        idPais: new FormControl(null, Validators.required),
        nombrePais: new FormControl('')
      })
    });

    this.responsableFormulario = new FormGroup({
      integrante: new FormControl(null, Validators.required),
      tiporel: new FormControl(null, Validators.required)
    })
  }

  ngOnInit() {

    this.loading = true;
    this.mostrarDesahacer = false;
    this.domicilioActualizado = null;
    this.route.queryParams
      .subscribe(params => { this.idFam = +params['idFam']; });

    let ayer = new Date();
    ayer.setDate(ayer.getDate() - 1);
    this.fechaFiltro = ayer.toISOString().slice(0, 10);

    this.formulariointegrante.get('grupoFamilia').setValue(this.idFam);
    this.traerTiposPaciente();
    this.traerPaisLista();
    this.traerCentrosSalud();
    this.obtenerDomicilioFamiliar();
    this.obtenerInfoRelacion(); //obtiene los integrantes posibles para seleccionar un repsonsabel y los tipos de relacion
    this.domicilio = { idDomicilio: null, calle: '', departamento: null, nroDomicilio: null, piso: null, localidad_id: null };

  }

  ngOnDestroy() {
    if (this.unsubscribe) {
      this.unsubscribe.unsubscribe();
    }
  }

  obtenerInfoRelacion() { //made by juampi
    this.famServ.getTiposRelacion().subscribe((data) => {
      data.forEach((elem) => {
        let tiporelElem: { id: number, nombre: string } = { id: null, nombre: "" }
        tiporelElem.id = elem.idTipoRelacion
        tiporelElem.nombre = elem.nombreTipoRelacion
        this.listaTiposrel.push(tiporelElem)
      })
    })

    this.famServ.getFlia(this.idFam).subscribe(datos => {
      datos['grupofamilia'].integrantesFam.forEach((elem: any) => {
        if (((new Date().getTime() - new Date(elem.fechaNacimiento).getTime()) / (1000 * 60 * 60 * 24 * 365)) >= 16) {
          let integElem: { id: number, nombre: string } = { id: null, nombre: "" }
          integElem.id = elem.idPaciente
          integElem.nombre = elem.nombrePaciente + " " + elem.apellidoPaciente
          this.listaIntegrantes.push(integElem)
        }
      })
      console.log(this.listaIntegrantes);
    })
  }

  traerTiposPaciente() {
    this.unsubscribe = this.pacServ.getTiposPaciente().subscribe((data: any) => {
      data.contenido.forEach(tipoPac => {
        let tip = { nombre: tipoPac.nombreTipoPaciente, id: tipoPac.idTipoPaciente }
        this.tiposPac.push(tip);
      });
    })
  }

  traerCentrosSalud() {
    let unsubscribe = this._centro.getCentros().subscribe((datos: Centro[]) => {
      this.listaCentros = datos;
      this.unsubscribe.add(unsubscribe);
    }, () => this.unsubscribe.add(unsubscribe));
  }

  traerPaisLista() {
    let unsubscribe = this.domServ.getPais().subscribe((datos: Pais[]) => {
      this.listaPaises = datos;
      this.unsubscribe.add(unsubscribe);
    }, () => this.unsubscribe.add(unsubscribe));
  }

  traerProvLista(idPais: number) {

    let unsubscribe = this.domServ.getProvinciaIdPais(idPais).subscribe((datos: Provincia[]) => {

      if (this.residenciaFormulario.get('paisForm.idPais').dirty) {

        this.residenciaFormulario.get('provinciaForm.idProvincia').setValue(null);
        this.residenciaFormulario.get('domicilioForm.localidad.idLocalidad').setValue(null);
      }

      this.listaProvincias = datos;
      this.unsubscribe.add(unsubscribe);

    }, () => this.unsubscribe.add(unsubscribe));
  }

  traerLocLista(idProv: number) {

    let unsubscribe = this.domServ.getLocalidadIdProv(idProv).subscribe((datos: Localidad[]) => {

      if (this.residenciaFormulario.get('paisForm.idPais').dirty) {
        this.residenciaFormulario.get('domicilioForm.localidad.idLocalidad').setValue(null);
      }

      this.listaLocalidades = datos;
      this.unsubscribe.add(unsubscribe);

    }, () => this.unsubscribe.add(unsubscribe));
  }

  obtenerDomicilioFamiliar() {
    let idFamilia: number = this.idFam;
    let promesa: Promise<{}>

    promesa = new Promise((resolve) => {
      let unsubscribe = this.domServ.getDomicilioFamilia(idFamilia).subscribe((data: any) => {
        this.domiciliosFam = [];

        data.contenido.forEach(domicilioX => {
          if (!this.domiciliosFam.includes(domicilioX)) {
            this.domiciliosFam.push(domicilioX);
          }
        });
        this.unsubscribe.add(unsubscribe);
        console.log(this.domiciliosFam);
        resolve();
      }, () => {
        (<any>$('#domFam')).prop('disabled', true);
        this.unsubscribe.add(unsubscribe);
        resolve();
      });
    })

    promesa.then(() => {
      this.loading = false;
    })
  }

  ActualizarDom(idDomicilio: number) {

    this.domicilioActualizado = idDomicilio;
    this.mostrarDesahacer = true;

    $(document).on('click', '#asignaDomFam', function () {
      (<any>$('#domFamiliares')).collapse('hide');
      (<any>$('#nuevoDomicilio')).prop('disabled', true);
      (<any>$('#domFamiliar')).prop('disabled', true);
    });
  }

  desahacerCambiosDom() {
    this.domicilioActualizado = null;
    this.residenciaFormulario.reset();
    $(document).on('click', '#desahacerDomFam', function () {
      (<any>$('#domFamiliares')).collapse('hide');
      (<any>$('#nuevoDom')).collapse('hide');
      (<any>$('#nuevoDomicilio')).prop('disabled', false);
      (<any>$('#domFamiliar')).prop('disabled', false);
    });

    this.mostrarDesahacer = false;
    setTimeout(() => {
      $('#desahacerDomFam').prop('checked', false);
    }, 1100);
  }

  confirmarIntegrante() {

    let pacientePost: Paciente = {
      idPaciente: null,
      nombrePaciente: '',
      apellidoPaciente: '',
      dniPaciente: null,
      fechaNacimiento: null,
      telefonoPaciente: "",
      tipoPaciente_id: null,
      centro: null,
      sexo: null,
      domicilio_id: null,
      pais: null,
      grupoFamilia: null
    }

    pacientePost = this.formulariointegrante.value;

    //set valor del formulario.

    if (this.domicilioActualizado !== null) {

      pacientePost.domicilio_id = this.domicilioActualizado;
      this.guardarPaciente(pacientePost);

    } else {

      this.guardarDom();
      //suscripción para ejecutar el método asincrona.
      this.promesaPostPaciente
        .subscribe((idDomicilio) => {
          pacientePost.domicilio_id = +idDomicilio;
          this.guardarPaciente(pacientePost)
        });
    }

  }

  guardarPaciente(paciente: Paciente) {

    let unsubscribe = this.pacServ.postPaciente(paciente)
      .subscribe((idPacientePost: any) => {
        if (paciente.tipoPaciente_id === 1) {

          this.famServ.postRelacion({
            pacienteUno_id: this.responsableFormulario.controls['integrante'].value, pacienteDos_id: idPacientePost['id'],
            responsable: true, tipoRelacion_id: this.responsableFormulario.controls['tiporel'].value
          })
            .subscribe((dat) => {
              Swal({ title: 'Integrante creado.', type: 'success', showConfirmButton: true })
                .then((rdo: SweetAlertResult) => {
                  if (rdo.value) this.router.navigate(['home', 'verFlia'], { queryParams: { idFam: paciente.grupoFamilia } });
                })

            }, (error) => {
              console.log('error creando responsable', error);
              Swal({ title: 'Error al crear integrante.', type: 'error', showConfirmButton: true });

            })

        } else {
          Swal({ title: 'Integrante creado.', type: 'success', showConfirmButton: true })
            .then((rdo: SweetAlertResult) => {
              this.unsubscribe.add(unsubscribe);
              if (rdo.value) this.router.navigate(['home', 'verFlia'], { queryParams: { idFam: paciente.grupoFamilia } });
            });
        }
      }, (error) => {
        console.log('error post paciente', error);
        this.unsubscribe.add(unsubscribe);
        Swal({ title: 'Error al crear integrante.', type: 'error', showConfirmButton: true })
      })
  }

  guardarDom() {

    this.promesaPostPaciente = Observable.create((idDomicilio) => {
      this.domicilio.calle = this.residenciaFormulario.get('domicilioForm').value.calle;
      this.domicilio.departamento = this.residenciaFormulario.get('domicilioForm').value.departamento;
      this.domicilio.nroDomicilio = this.residenciaFormulario.get('domicilioForm').value.nroDomicilio;
      this.domicilio.piso = this.residenciaFormulario.get('domicilioForm').value.piso;
      this.domicilio.localidad_id = this.residenciaFormulario.get('domicilioForm.localidad').value.idLocalidad;

      let unsubscribe = this.domServ.postDomicilio(this.domicilio).subscribe((idDom: any) => {
        this.unsubscribe.add(unsubscribe);
        idDomicilio.next(+idDom.id);
        idDomicilio.complete();
      }, (error) => {

        Swal({ title: 'Error al guardar domicilio.', type: 'error', showConfirmButton: true })
        this.unsubscribe.add(unsubscribe);

      });
    })
  }

  edadTipoPac(group: FormGroup): { [key: string]: boolean } | null {
    let tipoPac = group.controls['tipoPaciente_id']
    let fechaNac = group.controls['fechaNacimiento']

    if (tipoPac.value == 2) {
      if (fechaNac.value == undefined || (((new Date().getTime() - new Date(fechaNac.value).getTime()) / (1000 * 60 * 60 * 24 * 365)) < 9)) {
        fechaNac.setErrors({ 'edadMinMadre': true })
      }
    }
    if (tipoPac.value == 1) {
      if (fechaNac.value == undefined || (((new Date().getTime() - new Date(fechaNac.value).getTime()) / (1000 * 60 * 60 * 24 * 365)) > 5)) {
        fechaNac.setErrors({ 'edadMaxNino': true })
      }
    }

    if (tipoPac.value != 2 && (fechaNac.errors != null && fechaNac.errors.edadMinMadre)) {
      fechaNac.reset()
    }

    if (tipoPac.value != 1 && (fechaNac.errors != null && fechaNac.errors.edadMaxNino)) {
      fechaNac.reset()
    }

    return null;
  }

}

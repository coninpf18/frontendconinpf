import { SesionService } from '../../../../../servicios/sesion.service';
import { Component, OnInit } from '@angular/core';
import { PacienteService, Paciente } from "../../../../../servicios/paciente.service";
import { DomicilioService, Domicilio, Pais, Provincia, Localidad } from '../../../../../servicios/domicilio.service';
import { Router, ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CentroService, Centro } from '../../../../../servicios/centro.service';
import { NotificacionService } from '../../../../../servicios/notificacion.service';
import { Subscription, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { FamiliaService } from 'src/app/servicios/familia.service';
import Swal, { SweetAlertResult } from 'sweetalert2';

declare let $: any;

@Component({
  selector: 'app-modificar-integrante',
  templateUrl: './modificar-integrante.component.html',
  styles: [`
  input.ng-invalid.ng-touched{ 
    border: 1px solid red;
  }
  select.ng-invalid.ng-touched{ 
    border: 1px solid red;
  }
  .dropdownSize{
    height: 10px;
    width: 194px;
  }
  ` ]
})
export class ModificarIntegranteComponent implements OnInit {

  placeholder_tipopac: string = "Niño: Integrante menor a 5 años. \nMujer: Integrante femenina mayor de 9 años. \nOtro: Integrante que no es del tipo niño o mujer."

  //para agregar domicilio
  private listaPaises: Pais[];
  private listaProvincias: Provincia[];
  private listaLocalidades: Localidad[];
  private listaCentros: Centro[];

  private formulariointegrante: FormGroup;
  private residenciaFormulario: FormGroup;
  private responsableFormulario: FormGroup;
  private nuevaResidenciaFormulario: FormGroup;

  //integrante
  private tiposPac: { id: number, nombre: String };
  private domiciliosFam: Domicilio[];
  private listaIntegrantes: Array<{ id: number, nombre: string }> = [];
  private listaTiposrel: Array<{ id: number, nombre: string }> = [];
  private pacienteIngresado: boolean = false;

  //para rutear con param
  private idParam: number;
  private idFam: number;

  //variables varias.
  private loading: boolean;
  private promesaPostModif: Promise<{}>
  private inscripcionesObs: Subscription = new Subscription();
  private fechaFiltro: string

  // private fragment: string;
  private promesaPostPaciente: Observable<number> = new Observable();
  private unsubscribe: Subscription = new Subscription();
  private domicilioNuevo: Domicilio;


  constructor(private pacServ: PacienteService, private domServ: DomicilioService,
    private route: ActivatedRoute, private router: Router, private _centro: CentroService,
    private _notifSrv: NotificacionService, private famServ: FamiliaService, private sesionSrv: SesionService) {
    sesionSrv.nombreModulo = "Familia";
    this.formulariointegrante = new FormGroup({
      idPaciente: new FormControl(null),
      nombrePaciente: new FormControl('', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ \s]*')]),
      apellidoPaciente: new FormControl('', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ \s]*')]),
      fechaNacimiento: new FormControl('', Validators.required),
      dniPaciente: new FormControl(null, [Validators.pattern('[0-9]*'), Validators.minLength(7)]),
      nroHistoriaClinica: new FormControl(''),
      telefonoPaciente: new FormControl('', [Validators.pattern('[0-9]*'), Validators.minLength(6)]),
      tipoPaciente_id: new FormControl(null, Validators.required), //NO esta en la llamada a modificarPac, revisar.
      centro: new FormControl(null),
      sexo: new FormControl(null, Validators.required),
      grupoFamilia: new FormControl(null),
      domicilio_id: new FormControl(null),
      pais: new FormControl(null, Validators.required)
    });

    this.residenciaFormulario = new FormGroup({
      domicilioForm: new FormGroup({
        idDomicilio: new FormControl(null),
        calle: new FormControl('', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ \s]*')]),   //Agregar validación.
        departamento: new FormControl(null, [Validators.pattern('[0-9]*'), Validators.min(-1)]),
        nroDomicilio: new FormControl(null, [Validators.required, Validators.pattern('[0-9]*'), Validators.min(0)]),
        piso: new FormControl(null, [Validators.pattern('[0-9]*'), Validators.min(-1)]),
        localidad: new FormGroup({
          idLocalidad: new FormControl(null, Validators.required),
          nombreLocalidad: new FormControl(''),
          provincia: new FormControl(null)
        }),
      }),

      provinciaForm: new FormGroup({
        idProvincia: new FormControl(null, Validators.required),
        nombreProvincia: new FormControl(''),
        pais: new FormControl(null)
      }),

      paisForm: new FormGroup({
        idPais: new FormControl(null, Validators.required),
        nombrePais: new FormControl('')
      })
    });

    this.responsableFormulario = new FormGroup({
      integrante: new FormControl(null, Validators.required),
      tiporel: new FormControl(null, Validators.required)
    });

    this.nuevaResidenciaFormulario = new FormGroup({
      domicilioForm: new FormGroup({
        idDomicilio: new FormControl(null),
        calle: new FormControl('', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ \s]*')]),   //Agregar validación.
        departamento: new FormControl(null, [Validators.pattern('[0-9]*'), Validators.min(-1)]),
        nroDomicilio: new FormControl(null, [Validators.required, Validators.pattern('[0-9]*'), Validators.min(0)]),
        piso: new FormControl(null, [Validators.pattern('[0-9]*'), Validators.min(-1)]),
        localidad: new FormGroup({
          idLocalidad: new FormControl(null, Validators.required),
          nombreLocalidad: new FormControl(''),
          provincia: new FormControl(null)
        }),
      }),

      provinciaForm: new FormGroup({
        idProvincia: new FormControl(null, Validators.required),
        nombreProvincia: new FormControl(''),
        pais: new FormControl(null)
      }),

      paisForm: new FormGroup({
        idPais: new FormControl(null, Validators.required),
        nombrePais: new FormControl('')
      })
    });
  }

  ngOnInit() {

    this.loading = true;

    this.route.queryParams.subscribe(params => {
      this.idParam = +params['idPac'];
      this.idFam = +params['idFam'];
    });

    let ayer = new Date();
    ayer.setDate(ayer.getDate() - 1);
    this.fechaFiltro = ayer.toISOString().slice(0, 10);

    this.formulariointegrante.controls['grupoFamilia'].setValue(this.idFam);
    this.obtenerDomicilioFamiliar();
    this.mostrarIntegrante(this.idParam);
    this.domicilioNuevo = { idDomicilio: null, calle: '', departamento: null, nroDomicilio: null, piso: null, localidad_id: null };
  }

  ngOnDestroy() {
    if (this.inscripcionesObs) {
      this.inscripcionesObs.unsubscribe();
    }
  }

  mostrarIntegrante(idPac: number) {
    this.inscripcionesObs = this.pacServ.getPacienteByID(idPac).subscribe((datos: Paciente) => {

      this.getEstadoTipoPaciente(datos.idPaciente)
      this.formulariointegrante.controls['nombrePaciente'].setValue(datos.nombrePaciente);
      this.formulariointegrante.controls['apellidoPaciente'].setValue(datos.apellidoPaciente);
      this.formulariointegrante.controls['fechaNacimiento'].setValue(formatDate(datos.fechaNacimiento, 'yyyy-MM-dd', "en-US"));
      this.formulariointegrante.controls['dniPaciente'].setValue(datos.dniPaciente);
      this.formulariointegrante.controls['nroHistoriaClinica'].setValue(datos.nroHistoriaClinica);
      this.formulariointegrante.controls['telefonoPaciente'].setValue(datos.telefonoPaciente);
      this.formulariointegrante.controls['centro'].setValue(datos.centro);
      this.formulariointegrante.controls['sexo'].setValue(datos.sexo);
      this.formulariointegrante.controls['tipoPaciente_id'].setValue(datos.tipoPaciente.idTipoPaciente);
      this.formulariointegrante.controls['grupoFamilia'].setValue(datos.grupoFamilia);
      this.formulariointegrante.controls['idPaciente'].setValue(datos.idPaciente);
      this.formulariointegrante.controls['pais'].setValue(datos.pais);
      this.formulariointegrante.controls['domicilio_id'].setValue(datos.domicilio_id);

      this.traerTiposPaciente();
      this.traerCentrosSalud();
      this.obtenerDomicilio(this.formulariointegrante.value.idPaciente);
      if (datos.tipoPaciente.idTipoPaciente == 1) {

        this.obtenerInfoRelacion()
      }
    }, () => {
      console.log('error en el getPaciente de inicio');
      this.loading = false;
      Swal({ title: 'Error de conexión. Reintente luego.', type: 'error', showConfirmButton: true })
    }, () => this.loading = false
    );
  }
  getEstadoTipoPaciente(idPaciente) {
    this.pacServ.getTipoPacienteEstado(idPaciente).subscribe((datos: any) => {

      if (datos.estadoHistorial.estadoPaciente.nombreEstadoPaciente == "Ingresado") {
        this.pacienteIngresado = true

      }
    })
  }
  obtenerDomicilio(id: number) {
    let unsubscribe = this.domServ.getDomicilioPaciente(id).subscribe((data: Domicilio) => {
      this.setDomicilioFormulario(data);
      this.inscripcionesObs.add(unsubscribe)
    });
  }

  setDomicilioFormulario(data: Domicilio) {
    //Set domicilio en formularios.
    this.formulariointegrante.get('domicilio_id').setValue(data.idDomicilio);
    this.residenciaFormulario.get('domicilioForm.idDomicilio').setValue(data.idDomicilio); this.residenciaFormulario.get('domicilioForm.nroDomicilio').setValue(data.nroDomicilio)
    this.residenciaFormulario.get('domicilioForm.calle').setValue(data.calle); this.residenciaFormulario.get('domicilioForm.departamento').setValue(data.departamento)
    this.residenciaFormulario.get('domicilioForm.piso').setValue(data.piso)

    //Set localidad en formularios.
    this.residenciaFormulario.get('domicilioForm.localidad.idLocalidad').setValue(data.localidad.idLocalidad);
    this.residenciaFormulario.get('domicilioForm.localidad.provincia').setValue(data.localidad.provincia);
    this.residenciaFormulario.get('domicilioForm.localidad.nombreLocalidad').setValue(data.localidad.nombreLocalidad);

    let unsubscribe = this.domServ.getProvinciaId(data.localidad.provincia).pipe(flatMap((data: Provincia) => {
      this.residenciaFormulario.get('provinciaForm.idProvincia').setValue(data.idProvincia);
      this.residenciaFormulario.get('provinciaForm.nombreProvincia').setValue(data.nombreProvincia); //set provincia.
      this.residenciaFormulario.get('provinciaForm.pais').setValue(data.pais);

      return this.domServ.getPaisId(data.pais);

    })).subscribe((data: Pais) => {
      this.residenciaFormulario.get('paisForm.idPais').setValue(data.idPais);
      this.residenciaFormulario.get('paisForm.nombrePais').setValue(data.nombrePais);   //set pais.
      this.traerPaisLista();
      this.inscripcionesObs.add(unsubscribe)
    });
  }

  traerTiposPaciente() {
    let unsubscribe = this.pacServ.getTiposPaciente().subscribe((data: any) => {

      for (let i = 0; i < data.contenido.length; i++) {
        if (data.contenido[i].idTipoPaciente === this.formulariointegrante.controls['tipoPaciente_id'].value) {
          this.formulariointegrante.controls['tipoPaciente_id'].setValue(data.contenido[i].nombreTipoPaciente);
          break;
        }
      }

      this.inscripcionesObs.add(unsubscribe)
    })
  }

  obtenerInfoRelacion() { //made by juampi
    this.pacServ.getResponsable(this.idParam).subscribe((data: any) => {

      this.responsableFormulario.controls['integrante'].setValue(data.paciente.idPaciente);
      this.responsableFormulario.controls['tiporel'].setValue(data.relacion.idTipoRelacion)
    })

    this.famServ.getTiposRelacion().subscribe((data) => {
      data.forEach((elem) => {
        let tiporelElem: { id: number, nombre: string } = { id: null, nombre: "" }
        tiporelElem.id = elem.idTipoRelacion
        tiporelElem.nombre = elem.nombreTipoRelacion
        this.listaTiposrel.push(tiporelElem)
      })
    })
    this.famServ.getFlia(this.idFam).subscribe(datos => {
      datos['grupofamilia'].integrantesFam.forEach((elem: any) => {
        if (((new Date().getTime() - new Date(elem.fechaNacimiento).getTime()) / (1000 * 60 * 60 * 24 * 365)) >= 16) {
          let integElem: { id: number, nombre: string } = { id: null, nombre: "" }
          integElem.id = elem.idPaciente
          integElem.nombre = elem.nombrePaciente + " " + elem.apellidoPaciente
          this.listaIntegrantes.push(integElem)
        }
      })
    })
  }

  traerCentrosSalud() {
    let unsubscribe = this._centro.getCentros().subscribe((datos: Centro[]) => {
      this.listaCentros = datos;
      this.inscripcionesObs.add(unsubscribe)
    })
  }

  traerPaisLista() {
    let unsubscribe = this.domServ.getPais().subscribe((datos: Pais[]) => {
      this.listaPaises = datos;
      this.inscripcionesObs.add(unsubscribe)
    })

  }

  traerProvLista(idPais: number) {
    let unsubscribe = this.domServ.getProvinciaIdPais(idPais).subscribe((datos: Provincia[]) => {
      this.listaProvincias = datos;
      this.inscripcionesObs.add(unsubscribe)
    })
  }

  traerLocLista(idProv: number) {
    let unsubscribe = this.domServ.getLocalidadIdProv(idProv).subscribe((datos: Localidad[]) => {
      this.listaLocalidades = datos;
      this.inscripcionesObs.add(unsubscribe)
    })
  }

  obtenerDomicilioFamiliar() {

    let unsubscribe = this.domServ.getDomicilioFamilia(+this.idFam).subscribe((data: any) => {
      this.domiciliosFam = [];
      //revisar que entre sólo cuando hay modificaciones en el arreglo.
      data.contenido.forEach(domicilioX => {
        if (!this.domiciliosFam.includes(domicilioX)) {
          this.domiciliosFam.push(domicilioX);
        }
      });

      this.inscripcionesObs.add(unsubscribe)
    });
  }

  guardarIntegrante() {
    // Para seteo de input de domicilio, debo buscar una localidad en caso que cambie la localidad, por eso busco una localidad acá (con el ID seleccionado por el usuario).
    // Debo inicializar en null una Interface antes de usarla sino no anda.
    let pacientePut: Paciente = {
      idPaciente: null,
      nombrePaciente: '',
      apellidoPaciente: '',
      dniPaciente: null,
      fechaNacimiento: null,
      nroHistoriaClinica: '',
      telefonoPaciente: '',
      // tipoPaciente_id: null,
      centro: null,
      sexo: null,
      domicilio_id: null,
      pais: null,
      grupoFamilia: null
    }

    let domicilio: Domicilio = {
      idDomicilio: null,
      calle: '',
      departamento: null,
      nroDomicilio: null,
      piso: null,
      localidad_id: null,
    };

    pacientePut.idPaciente = this.formulariointegrante.get('idPaciente').value;
    pacientePut.nombrePaciente = this.formulariointegrante.get('nombrePaciente').value;
    pacientePut.apellidoPaciente = this.formulariointegrante.get('apellidoPaciente').value;
    pacientePut.dniPaciente = this.formulariointegrante.get('dniPaciente').value;
    pacientePut.fechaNacimiento = this.formulariointegrante.get('fechaNacimiento').value;
    pacientePut.nroHistoriaClinica = this.formulariointegrante.get('nroHistoriaClinica').value;
    pacientePut.telefonoPaciente = this.formulariointegrante.get('telefonoPaciente').value;
    pacientePut.centro = this.formulariointegrante.get('centro').value;
    pacientePut.sexo = this.formulariointegrante.get('sexo').value;
    // pacientePut.tipoPaciente_id = this.formulariointegrante.get('tipoPaciente_id').value;
    pacientePut.pais = this.formulariointegrante.get('pais').value;
    pacientePut.grupoFamilia = this.formulariointegrante.get('grupoFamilia').value;
    pacientePut.domicilio_id = this.formulariointegrante.get('domicilio_id').value;

    domicilio.localidad_id = this.residenciaFormulario.get('domicilioForm.localidad.idLocalidad').value;
    domicilio.calle = this.residenciaFormulario.get('domicilioForm.calle').value;
    domicilio.departamento = this.residenciaFormulario.get('domicilioForm.departamento').value;
    domicilio.idDomicilio = this.residenciaFormulario.get('domicilioForm.idDomicilio').value;
    domicilio.nroDomicilio = this.residenciaFormulario.get('domicilioForm.nroDomicilio').value;
    domicilio.piso = this.residenciaFormulario.get('domicilioForm.piso').value;

    if (this.responsableFormulario.dirty && this.responsableFormulario.valid) {
      this.famServ.postRelacion({
        pacienteUno_id: this.responsableFormulario.controls['integrante'].value, pacienteDos_id: this.idParam,
        responsable: true, tipoRelacion_id: this.responsableFormulario.controls['tiporel'].value
      }).subscribe(() => { }, (error) => { console.log(error); Swal({ title: 'Error al guardar responsable.', type: 'error', showConfirmButton: true }) })
    }

    let unsubscribe = this.pacServ.putPaciente(pacientePut).subscribe(() => {
      this.inscripcionesObs.add(unsubscribe);
      this.guardarDom(domicilio);
    }, error => {
      console.log('error put paciente', error);
      this.inscripcionesObs.add(unsubscribe);
      Swal({ title: 'Error al modificar integrante.', type: 'error', showConfirmButton: true })
    });
  }

  ActualizarDom(domicilio: Domicilio) {
    this.setDomicilioFormulario(domicilio);
    $('#accordion').collapse('hide');
  }

  guardarDom(domicilio: Domicilio) {
    this.domServ.putDomicilio(domicilio).subscribe(() => {

    }, (error) => {
      console.log('error putDom', error);
      Swal({ title: 'Error al modificar domicilio.', type: 'error', showConfirmButton: true })
    }, () => {

      Swal({ title: 'Integrante modificado.', type: 'success', showConfirmButton: true })
        .then((rdo: SweetAlertResult) => {
          if (rdo.value) this.router.navigate(['home', 'verFlia'], { queryParams: { idFam: this.formulariointegrante.get('grupoFamilia').value } });
        })

    });
  }

  guardarNuevoDom() {

    let domicilio: Domicilio = {
      calle: this.nuevaResidenciaFormulario.get('domicilioForm').value.calle,
      departamento: this.nuevaResidenciaFormulario.get('domicilioForm').value.departamento,
      nroDomicilio: +(this.nuevaResidenciaFormulario.get('domicilioForm').value.nroDomicilio),
      piso: this.nuevaResidenciaFormulario.get('domicilioForm').value.piso,
      localidad_id: +(this.nuevaResidenciaFormulario.get('domicilioForm.localidad').value.idLocalidad),
    };

    let unsubscribe = this.domServ.postDomicilio(domicilio).pipe(flatMap((idDom: any) => {
      let paciente: Paciente = {
        domicilio_id: +idDom.id,
        idPaciente: +this.idParam
      }

      return this.pacServ.putNuevoDomPaciente(paciente);
    })).subscribe(() => {
      this.unsubscribe.add(unsubscribe);
      Swal({ title: 'Domicilio creado.', type: 'success', showConfirmButton: true })
      this.actualizarHTMLdomicilio(domicilio);
      $('#accordion').collapse('hide');
    }, (error) => {
      this.unsubscribe.add(unsubscribe)
      console.log(error, 'error al modificar integrante creando nuevo domicilio');
      Swal({ title: 'Error al crear domiclio.', type: 'error', showConfirmButton: true })
    });
  }

  actualizarHTMLdomicilio(domicilio: Domicilio) {

    this.residenciaFormulario.get('domicilioForm.calle').setValue(domicilio.calle);
    this.residenciaFormulario.get('domicilioForm.nroDomicilio').setValue(domicilio.nroDomicilio);
    this.residenciaFormulario.get('domicilioForm.piso').setValue(domicilio.piso);
    this.residenciaFormulario.get('domicilioForm.departamento').setValue(domicilio.departamento);

    for (let i = 0; i < this.listaLocalidades.length; i++) {
      if (this.listaLocalidades[i].idLocalidad === +this.nuevaResidenciaFormulario.get('domicilioForm.localidad.idLocalidad').value) {
        this.residenciaFormulario.get('domicilioForm.localidad.nombreLocalidad').setValue(this.listaLocalidades[i].nombreLocalidad);
        this.residenciaFormulario.get('domicilioForm.localidad.provincia').setValue(this.listaLocalidades[i].provincia);
        this.residenciaFormulario.get('domicilioForm.localidad.idLocalidad').setValue(this.listaLocalidades[i].idLocalidad);
      }
    }

    for (let i = 0; i < this.listaProvincias.length; i++) {
      if (this.listaProvincias[i].idProvincia === +this.nuevaResidenciaFormulario.get('provinciaForm.idProvincia').value) {
        this.residenciaFormulario.get('provinciaForm.nombreProvincia').setValue(this.listaProvincias[i].nombreProvincia);
        this.residenciaFormulario.get('provinciaForm.pais').setValue(this.listaProvincias[i].pais);
        this.residenciaFormulario.get('provinciaForm.idProvincia').setValue(this.listaProvincias[i].idProvincia);
      }
    }

    for (let i = 0; i < this.listaPaises.length; i++) {
      if (this.listaPaises[i].idPais === +this.nuevaResidenciaFormulario.get('paisForm.idPais').value) {
        this.residenciaFormulario.get('paisForm.idPais').setValue(this.listaPaises[i].idPais);
        this.residenciaFormulario.get('paisForm.nombrePais').setValue(this.listaPaises[i].nombrePais);
      }
    }
  }

}

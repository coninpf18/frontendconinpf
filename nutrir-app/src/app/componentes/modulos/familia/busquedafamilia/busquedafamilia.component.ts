import { SesionService } from '../../../../servicios/sesion.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Familia, FamiliaService } from '../../../../servicios/familia.service';
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-busqueda-familia',
  templateUrl: './busquedafamilia.component.html'
})

export class BusquedaFamiliaComponent implements OnDestroy, OnInit {

  private familias: Familia[];
  private familiasTodas: Familia[];
  private unsubscribe: Subscription;
  private noFlia: boolean;
  private pageActual: number = 1;
  private mostrarPaginacion: boolean;

  constructor(private famService: FamiliaService, private router: Router, private sesionSrv: SesionService) {

  }

  ngOnInit() {

    this.unsubscribe = new Subscription();
    this.mostrarPaginacion = false;

  }

  ngOnDestroy() {
    if (this.unsubscribe) {
      this.unsubscribe.unsubscribe();
    }
  }

  buscarFamilia(evento: NgForm) {

    let termino: string = evento.value.familia;

    this.familias = [];
    var replaced = termino.split(' ').join('-');

    if (replaced.length >= 2) {
      this.unsubscribe = this.famService.getFliaTermino(replaced).subscribe((data: Familia[]) => {
        this.familias = data;
        this.noFlia = false;
        this.mostrarPaginacion = true;
      }, () => {
        this.noFlia = true;
      });
    } else {
      this.mostrarPaginacion = false;
    }
  }

  verflia(id: any) {
    this.router.navigate(['home', 'verFlia'], { queryParams: { idFam: +id } });
  }

  buscarTodasFamilias() {

    this.familias = [];

    this.famService.getTodasFamilias().subscribe((data: Familia[]) => {
      this.familias = data;
      this.mostrarPaginacion = true;
    });
  }

  fechaLinda(date) {
    let linda = formatDate(date, 'dd/MM/yyyy', "en-US");
    return linda;
  }

}

import { Component, OnInit } from '@angular/core';
import { FamiliaService, Integrante, Familia, Relacion, TiposRelacion } from '../../../../servicios/familia.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PacienteService } from '../../../../servicios/paciente.service';
import { Subscription } from 'rxjs';
import { SesionService } from 'src/app/servicios/sesion.service';

declare let $: any;

@Component({
  selector: 'app-ver-flia',
  templateUrl: './verflia.component.html'
})

export class VerFliaComponent implements OnInit{
 
  private familia: Familia;
  private idParam: number;
  private relaciones: Relacion[];
  private tiposRelacion: TiposRelacion[];
  private pacientes: Integrante[];
  private loading: boolean;
  private unsubscribe: Subscription= new Subscription();
  private responsable: Integrante[];
  private mostrarTabla: boolean= false; // y esto??????
 
  constructor(private fliaServ: FamiliaService, private route: ActivatedRoute, private _pacSrv: PacienteService,
              private router: Router, private _sesionSrv: SesionService) {
     
    _sesionSrv.setNombreModulo('Familia');
    _sesionSrv.getNombreModulo();
    _pacSrv.setPacNavbar(null);

  }

  ngOnInit(){
    
    this.route.queryParams.subscribe(params => {
      this.idParam= +params['idFam'];
    });

    this.loading= true;
    this.mostrarFamilia(this.idParam);  

  }

  ngOnDestroy(){
    if(this.unsubscribe){
      this.unsubscribe.unsubscribe();
    }
  }

  mostrarFamilia(idFam: number){

    let int: Integrante[]= [];
    let flia: Familia= { idGrupoFamilia: null, integrantesFam: int, apellidoGrupoFamilia: null, fecha_creado: null };
    this.relaciones= [];
    this.pacientes= [];
    this.responsable= [];
  
    let unsubscribe= this.fliaServ.getFlia(idFam).subscribe((data: any)=> {
      new Promise((resolve)=> {resolve(data)}).then((data: any)=> {
        flia.idGrupoFamilia= data.grupofamilia.idGrupoFamilia;
        flia.apellidoGrupoFamilia= data.grupofamilia.apellidoGrupoFamilia; 
        flia.fecha_creado= data.grupofamilia.fecha_creado; 

        data.grupofamilia.integrantesFam.forEach(integrante=> {
          let integrantes: Integrante= {
            nombre: integrante.nombrePaciente,
            idPaciente: +integrante.idPaciente,
            tipoPaciente: { idTP: +integrante.tipoPaciente.idTipoPaciente, nombreTP: integrante.tipoPaciente.nombreTipoPaciente },
            apellido: integrante.apellidoPaciente,
            dni: integrante.dniPaciente,
            fechaNac: integrante.fechaNacimiento,
            nHC: integrante.nroHistoriaClinica,
            telef: integrante.telefonoPaciente,
            pais: +integrante.pais,
            centro: +integrante.centro,
            sexo: +integrante.sexo
          }
          
          flia.integrantesFam.push(integrantes);
          this.pacientes.push(integrantes);
        });     

        data.relaciones.forEach(relacion => {
          
          
          let pac1: Integrante= {
            nombre: relacion.pacienteUno.nombrePaciente,
            idPaciente: +relacion.pacienteUno.idPaciente,
            tipoPaciente: 	{idTP: +relacion.pacienteUno.tipoPaciente.idTipoPaciente, nombreTP: relacion.pacienteUno.tipoPaciente.nombreTipoPaciente},
            apellido: relacion.pacienteUno.apellidoPaciente,
            dni: relacion.pacienteUno.dniPaciente,
            fechaNac: relacion.pacienteUno.fechaNacimiento,
            nHC: relacion.pacienteUno.nroHistoriaClinica,
            telef: relacion.pacienteUno.telefonoPaciente,
            pais: +relacion.pacienteUno.pais,
            centro: +relacion.pacienteUno.centro,
            sexo: +relacion.pacienteUno.sexo
          };

          let pac2: Integrante= {
            nombre: relacion.pacienteDos.nombrePaciente,
            idPaciente: +relacion.pacienteDos.idPaciente,
            tipoPaciente:	{idTP: +relacion.pacienteDos.tipoPaciente.idTipoPaciente, nombreTP:relacion.pacienteDos.tipoPaciente.nombreTipoPaciente},
            apellido: relacion.pacienteDos.apellidoPaciente,
            dni: relacion.pacienteDos.dniPaciente,
            fechaNac: relacion.pacienteDos.fechaNacimiento,
            nHC: relacion.pacienteDos.nroHistoriaClinica,
            telef: relacion.pacienteDos.telefonoPaciente,
            pais: +relacion.pacienteDos.pais,
            centro: +relacion.pacienteDos.centro,
            sexo: +relacion.pacienteDos.sexo
          };

          let relac: Relacion= {
            idRelacion: +relacion.idRelacion,
            paciente1: pac1,
            paciente2: pac2,
            tipoRelacion: {
              idTipoRelacion: +relacion.tipoRelacion.idTipoRelacion,
              nombre: relacion.tipoRelacion.nombreTipoRelacion,
              unico: relacion.tipoRelacion.unico
            },
            responsable: relacion.responsable
          }

          this.relaciones.push(relac);

          if(relac.responsable){
            flia.integrantesFam.forEach(integrante => {
              if (relac.paciente2.idPaciente===integrante.idPaciente) {
                integrante.responsable= relac.paciente1.nombre+' '+relac.paciente1.apellido
              }
              
            })
            this.responsable.push(pac2);
          }

        });

        //Set datos de la variable (famSrv) para pasar las relaciones al componente editarRelFam.
        this.fliaServ.relacionesFamEditar= [];
        this.fliaServ.relacionesFamEditar= data;

      });

      this.unsubscribe.add(unsubscribe);
    });
  
    this.familia= flia;  

    //Set de datos para pasar al componente editarRelFam.
    this.fliaServ.familia= null;  this.fliaServ.familia= this.familia;

    this.obtenerTiposRelacion();
  }

  obtenerResponsable(idPaciente: number): string {
    console.log(this.relaciones);
    
    let mensaje: string;
   
    for(let i=0; i< this.relaciones.length; i++){
      if(this.relaciones[i].paciente2.idPaciente=== idPaciente){
        if(this.relaciones[i].responsable){
          mensaje= this.relaciones[i].paciente1.nombre + ' '+ this.relaciones[i].paciente1.apellido;
        } else{
          mensaje= " "
        }
      }
    }
     setTimeout(() => {
      this.mostrarTabla=true
    }, 5000);
  
    return mensaje;
  }

  editarIntegrante(id: number){
    this.router.navigate(['home','mod-integrante'], {queryParams: {idPac: id, idFam: this.familia.idGrupoFamilia}});
  }

  nuevoIntegrante(idfam: number){
    this.router.navigate(['home','integrante'], {queryParams: {idFam: idfam }});
  }

  obtenerTiposRelacion(){

    this.tiposRelacion= [];

    let unsubscribe= this.fliaServ.getTiposRelacion().subscribe((data: any)=> {
      for(let i=0; i<data.length; i++){
        let tipoRel= {
          idTipoRelacion: data[i].idTipoRelacion,
          nombre: data[i].nombreTipoRelacion,
          unico: data[i].unico
        }
          this.tiposRelacion.push(tipoRel);    
      }

      this.loading= false
      this.unsubscribe.add(unsubscribe);
    });
  }

  irPaciente(idPaciente: number){  
    this.router.navigate(['home','paciente-detalle'],{queryParams: {idPac: idPaciente}});
  }

  irEditarRelFam(){
    this.router.navigate(['home','editarRelFam'], {queryParams: {idFam: this.idParam}});   
  }

}
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FamiliaService, Relacion, TiposRelacion, Familia } from 'src/app/servicios/familia.service';
import Swal, { SweetAlertResult } from 'sweetalert2';
import { Subscription, Observable } from 'rxjs';

declare let $: any;

@Component({
  selector: 'app-editar-rel-fam',
  templateUrl: './editar-rel-fam.component.html',
  styles: []
})

export class EditarRelFamComponent implements OnInit {

  private familia: Familia;
  private idParam: number;
  private relaciones: Relacion[];
  private nuevaRelacion = { id: null, paciente1_id: null, paciente2_id: null, tipoRelacion_id: null };
  private tiposRelacion: TiposRelacion[];
  private loading: boolean;
  private tiposRelacionUnica: number[];
  private relacionesCargadas = new Array<Relacion>();
  private relacionEliminar: Relacion;
  private unsubscribe: Subscription = new Subscription();
  private relacionesModificadas: Relacion[];
  private idRelMod: number[];

  constructor(private fliaServ: FamiliaService, private route: ActivatedRoute, private router: Router) {

  }

  ngOnInit() {

    this.route.queryParams.subscribe(params => {
      this.idParam = +params['idFam'];
    });

    this.loading = true;
    this.relaciones = this.fliaServ.relacionesFamEditar.relaciones;
    this.familia = this.fliaServ.relacionesFamEditar.grupofamilia;

    this.obtenerTiposRelacion();
  }

  ngOnDestroy() {
    if (this.unsubscribe) {
      this.unsubscribe.unsubscribe();
    }
  }

  obtenerTiposRelacion() {

    this.tiposRelacion = [];
    this.tiposRelacionUnica = [];

    let unsubscribe = this.fliaServ.getTiposRelacion().subscribe((data: any) => {
      for (let i = 0; i < data.length; i++) {
        let tipoRel = {
          idTipoRelacion: data[i].idTipoRelacion,
          nombre: data[i].nombreTipoRelacion,
          unico: data[i].unico
        }
        this.tiposRelacion.push(tipoRel);

        if (tipoRel.unico == true) {
          this.tiposRelacionUnica.push(tipoRel.idTipoRelacion);
        }
      }
      this.loading = false
      this.unsubscribe.add(unsubscribe);
    });

    this.relacionesModificadas = [];
    this.idRelMod = [];
  }

  guardarRelaciones() {

    let invalidezNuevaRel: boolean;
    let invalidezRelaciones: boolean[] = [];
    this.relacionesCargadas = [];
    let relacionesAGuardar: Relacion[] = [];

    new Promise((resolve) => {

      //Primero guarda las relaciones modificadas, y luego guarda las relaciones restantes con los 2 for(). Verificando no incluir duplicadas.
      for (let c = 0; c < this.relacionesModificadas.length; c++) {

        let relacion: Relacion = {
          idRelacion: this.relacionesModificadas[c].idRelacion,
          pacienteUno: this.relacionesModificadas[c].paciente1,
          pacienteDos: this.relacionesModificadas[c].paciente2,
          tipoRelacion: this.relacionesModificadas[c].tipoRelacion
        }

        relacionesAGuardar.push(relacion);
      }

      for (let i = 0; i < this.relaciones.length; i++) {
        if (!(this.idRelMod.includes(this.relaciones[i].idRelacion))) {
          relacionesAGuardar.push(this.relaciones[i]);
        }
      }

      for (let i = 0; i < relacionesAGuardar.length; i++) {
        let relacion: Relacion = {
          idRelacion: relacionesAGuardar[i].idRelacion,
          pacienteUno_id: relacionesAGuardar[i].pacienteUno.idPaciente,
          pacienteDos_id: relacionesAGuardar[i].pacienteDos.idPaciente,
          tipoRelacion_id: relacionesAGuardar[i].tipoRelacion.idTipoRelacion
        }

        invalidezRelaciones.push(this.validacionRelaciones(relacion));
      }

      resolve();

    })
      .then(() => {
        if (!invalidezRelaciones.includes(true)) {
          Observable.create((resultado) => {
            invalidezNuevaRel = this.guardarNuevaRelacion();
            resultado.next(invalidezNuevaRel);
          })
            .subscribe((invalido) => {
              if (!invalido) {
                this.hacerPutRelacion(this.relacionesModificadas);
              }
            });
        }
      });
  }

  guardarNuevaRelacion(): boolean {

    let invalidezRelacion: boolean;

    if (this.nuevaRelacion.paciente1_id != null && this.nuevaRelacion.paciente2_id != null && this.nuevaRelacion.tipoRelacion_id != null) {

      let relacionPost: Relacion = {
        pacienteUno_id: +this.nuevaRelacion.paciente1_id,
        pacienteDos_id: +this.nuevaRelacion.paciente2_id,             //Relación para el POST.
        tipoRelacion_id: +this.nuevaRelacion.tipoRelacion_id,
      }

      let relacion: Relacion = {
        idRelacion: null,
        pacienteUno: null,
        pacienteDos: null,                              //Relación para agregar al arreglo y visualizar por pantalla (HTML).
        tipoRelacion: null
      }

      invalidezRelacion = this.validacionRelaciones(relacionPost);

      if (!invalidezRelacion) {
        let unsubscribe = this.fliaServ.postRelacion(relacionPost)
          .subscribe((idRel: any) => {

            relacion.idRelacion = idRel.id;

            for (let i = 0; i < this.familia.integrantesFam.length; i++) {

              if (relacionPost.pacienteUno_id === this.familia.integrantesFam[i].idPaciente) {
                relacion.pacienteUno = this.familia.integrantesFam[i];
              } else if (relacionPost.pacienteDos_id === this.familia.integrantesFam[i].idPaciente) {
                relacion.pacienteDos = this.familia.integrantesFam[i];
              }

              for (let c = 0; c < this.tiposRelacion.length; c++) {
                if (relacionPost.tipoRelacion_id === this.tiposRelacion[c].idTipoRelacion) {
                  relacion.tipoRelacion = this.tiposRelacion[c];
                }
              }
            }

            this.relaciones.push(relacion);
            this.unsubscribe.add(unsubscribe);
            Swal({ title: 'Relación familiar creada.', type: 'success', showConfirmButton: true })
            $('#nuevaRelacion').collapse('hide');
            this.nuevaRelacion = { id: null, paciente1_id: null, paciente2_id: null, tipoRelacion_id: null };

          }, (error) => {

            console.log(error);
            this.unsubscribe.add(unsubscribe);
            this.nuevaRelacion = { id: null, paciente1_id: null, paciente2_id: null, tipoRelacion_id: null };
            Swal({ title: 'Error al guardar nueva relación familiar.', type: 'error', showConfirmButton: true })

          });

      } else {
        invalidezRelacion = true;
      }
    } else {
      invalidezRelacion = false;
    }

    $('#collapseTwo').collapse('hide');
    $('#accordion').collapse('hide');                   // id="accordion"

    return invalidezRelacion;
  }

  hacerPutRelacion(relaciones: Relacion[]) {

    let contador: number = 0;
    if (this.relacionesModificadas.length !== 0) {
      for (let i = 0; i < relaciones.length; i++) {

        let relacion: Relacion = {
          idRelacion: relaciones[i].idRelacion,
          pacienteDos_id: relaciones[i].paciente2.idPaciente,
          pacienteUno_id: relaciones[i].paciente1.idPaciente,
          tipoRelacion_id: relaciones[i].tipoRelacion.idTipoRelacion
        }

        this.fliaServ.putRelacion(relacion, relacion.idRelacion)
          .subscribe(() => {

            if (contador === (this.relacionesModificadas.length - 1)) {
              Swal({ title: 'Relaciones guardadas.', type: 'success', showConfirmButton: true })
                .then((rdo: SweetAlertResult) => {
                  if (rdo.value) this.router.navigate(['home', 'verFlia'], { queryParams: { idFam: this.idParam } })
                })
            }

            contador++;

          }, (error) => {
            Swal({ title: 'Error al guardar relaciones.', type: 'error', showConfirmButton: true })
            console.log('error guardar relación: ', error);
          });
      }
    } else {

      Swal({ title: 'Relaciones guardadas.', type: 'success', showConfirmButton: true })
        .then((rdo: SweetAlertResult) => {
          if (rdo.value) this.router.navigate(['home', 'verFlia'], { queryParams: { idFam: this.idParam } })
        })

    }
  }


  //Actualiza en caso que haya cambios de valores en los select del botón "editar relaciones"
  refreshTipoRel(idTipoRelCambiada: number, idx: number) {

    for (let i = 0; i < this.tiposRelacion.length; i++) {
      if (+idTipoRelCambiada === this.tiposRelacion[i].idTipoRelacion) {
        let relacion: Relacion = {
          idRelacion: this.relaciones[idx].idRelacion,
          paciente1: this.relaciones[idx].pacienteUno,
          paciente2: this.relaciones[idx].pacienteDos,
          tipoRelacion: this.tiposRelacion[i],
        }
        this.metodoRelacionesParaPut(relacion);
      }
    }
  }

  refreshPacUno(idPac1Cambiado: number, idx: number) {

    for (let i = 0; i < this.familia.integrantesFam.length; i++) {
      if (+idPac1Cambiado === this.familia.integrantesFam[i].idPaciente) {
        let relacion: Relacion = {
          idRelacion: this.relaciones[idx].idRelacion,
          paciente1: this.familia.integrantesFam[i],
          paciente2: this.relaciones[idx].pacienteDos,
          tipoRelacion: this.relaciones[idx].tipoRelacion,
        }

        this.metodoRelacionesParaPut(relacion);
      }
    }
  }

  refreshPacDos(idPac2Cambiado: number, idx: number) {

    for (let i = 0; i < this.familia.integrantesFam.length; i++) {
      if (+idPac2Cambiado === this.familia.integrantesFam[i].idPaciente) {
        let relacion: Relacion = {
          idRelacion: this.relaciones[idx].idRelacion,
          paciente1: this.relaciones[idx].pacienteUno,
          paciente2: this.familia.integrantesFam[i],
          tipoRelacion: this.relaciones[idx].tipoRelacion,
        }

        this.metodoRelacionesParaPut(relacion);
      }
    }
  }

  metodoRelacionesParaPut(relacion: Relacion) {

    let idxRelaciones: number = this.idRelMod.indexOf(relacion.idRelacion);

    if (idxRelaciones > -1) {
      this.relacionesModificadas[idxRelaciones] = relacion;  // reemplaza una relación ya modificada previamente con su nueva modificación.
    } else {
      this.idRelMod.push(relacion.idRelacion);
      this.relacionesModificadas.push(relacion);            // Inserta una nueva relación modificada.
    }
  }

  validacionRelaciones(relacion: Relacion): boolean {
    let duplicado: boolean = true;
    let arregloInterno: boolean[] = [];

    if (relacion.pacienteDos_id != relacion.pacienteUno_id) {       //Validar distinto paciente en la misma relacion.

      if (this.relacionesCargadas.length === 0) {
        this.relacionesCargadas.push(relacion);
        return false;
      }

      for (let i = 0; i < this.relacionesCargadas.length; i++) {

        if (!((relacion.pacienteUno_id === this.relacionesCargadas[i].pacienteUno_id && relacion.pacienteDos_id === this.relacionesCargadas[i].pacienteDos_id
          && relacion.tipoRelacion_id === this.relacionesCargadas[i].tipoRelacion_id) || (relacion.pacienteUno_id == this.relacionesCargadas[i].pacienteDos_id && relacion.pacienteDos_id === this.relacionesCargadas[i].pacienteUno_id
            && relacion.tipoRelacion_id === this.relacionesCargadas[i].tipoRelacion_id))) {    //mismos integrantes no estén en 2 relaciones distintas

          if (!((relacion.pacienteUno_id === this.relacionesCargadas[i].pacienteUno_id && relacion.pacienteDos_id === this.relacionesCargadas[i].pacienteDos_id)
            || (relacion.pacienteUno_id == this.relacionesCargadas[i].pacienteDos_id && relacion.pacienteDos_id === this.relacionesCargadas[i].pacienteUno_id))) {    //relación no sea repetida.

            if (!(relacion.pacienteDos_id === this.relacionesCargadas[i].pacienteDos_id && relacion.responsable === true && relacion.responsable === this.relacionesCargadas[i].responsable)) {  //mismo integrante no tenga 2 responsables

              if (this.tiposRelacionUnica.includes(relacion.tipoRelacion_id) && relacion.tipoRelacion_id === this.relacionesCargadas[i].tipoRelacion_id
                && relacion.pacienteDos_id === this.relacionesCargadas[i].pacienteDos_id) {     //relación unica no repetida.
                arregloInterno.push(true);
                Swal({ title: 'Tipo de relación familiar única ya asignada a otro integrante.', type: 'info', showConfirmButton: true })
              } else {                           //Acá se chequearía que todo esté bien para que sea válida la relación.
                arregloInterno.push(false);
              }
            } else {
              arregloInterno.push(true);
              Swal({ title: 'Integrante familiar con responsable ya asignado.', type: 'info', showConfirmButton: true })
              //CON LA IDEA ACTUAL, ESTO NO SE VERIFICA NUNCA. SE DEBE HACER ANTES.
            }
          } else {
            arregloInterno.push(true);
            Swal({ title: 'Integrantes ya incluidos en otra relación familiar.', type: 'info', showConfirmButton: true })
          }
        } else {
          arregloInterno.push(true);
          Swal({ title: 'Relación familiar repetida.', type: 'info', showConfirmButton: true });
        }
      }

      if (!arregloInterno.includes(true)) {
        duplicado = false;
        this.relacionesCargadas.push(relacion);
      }
    } else {
      Swal({ title: 'Mismo paciente ocupando ambos roles de la relación.', type: 'info', showConfirmButton: true })
    }
    return duplicado;
  }

  eliminarRelacion() {

    if (!this.relacionEliminar.responsable) {
      let unsubscribe = this.fliaServ.deleteRelacion(this.relacionEliminar.idRelacion).subscribe(() => {
        let index = this.relaciones.indexOf(this.relacionEliminar);
        if (index > -1) {
          this.relaciones.splice(index, 1);       //Elimina la relación del array de relaciones.
        }
        Swal({ title: 'Relación familiar eliminada.', type: 'success', showConfirmButton: true })
        this.borrarCheckBoxChecked();
        $('#collapseTwo').collapse('hide');
        $('#accordion').collapse('hide');
        this.unsubscribe.add(unsubscribe);
      }, (error) => {
        console.log('Error al eliminar relación', error);
        Swal({ title: 'Error al eliminar relación.', type: 'error', showConfirmButton: true });
        this.unsubscribe.add(unsubscribe);
      });
    } else {
      this.borrarCheckBoxChecked();
      Swal({ title: 'No se puede eliminar una relación asignada como responsable.', type: 'info', showConfirmButton: true })
    }
  }

  modalDeleteRelacion(valor: boolean, idx: number) {
    if (valor === true) {
      this.relacionEliminar = this.relaciones[idx];
      (<any>$('#modalEliminar')).modal('show');
    }
  }

  borrarCheckBoxChecked() {
    $('input[id= checkBoxE]').prop('checked', false);
  }

  reiniciarNuevaRelForm() {
    this.nuevaRelacion = { id: null, paciente1_id: null, paciente2_id: null, tipoRelacion_id: null }
  }
}
import { SesionService } from '../../../servicios/sesion.service';
import { Component } from '@angular/core';
import { PacienteService } from '../../../servicios/paciente.service';

@Component({
  selector: 'app-familia',
  templateUrl: './familia.component.html'
})

export class FamiliaComponent {


  constructor(private sesionSrv: SesionService, private _pacSrv: PacienteService) {

  	this.sesionSrv.setNombreModulo('Familia');
    this.sesionSrv.getNombreModulo();
    _pacSrv.setPacNavbar(null);

  }

}

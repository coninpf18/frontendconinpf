import { Component, OnDestroy } from '@angular/core';
import { NuevafliaService } from "../../../../servicios/nuevaflia.service";
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { SesionService } from '../../../../servicios/sesion.service';
import Swal, { SweetAlertResult } from 'sweetalert2';

@Component({
  selector: 'app-nuevaflia',
  templateUrl: './nuevaflia.component.html'
})
export class NuevafliaComponent implements OnDestroy {

  private unsubscribe: Subscription = new Subscription();

  constructor(private nuevafliaService: NuevafliaService, private router: Router, private sesionSrv: SesionService) {

    sesionSrv.nombreModulo = "Familia";
  
  }

  ngOnDestroy() {
    if (this.unsubscribe) {
      this.unsubscribe.unsubscribe();
    }
  }

  crearFlia(lastname: string) {
    let apellido = lastname.toString();
    this.unsubscribe = this.nuevafliaService.postFlia(apellido)
      .subscribe((data: any) => {

        Swal({ title: 'Familia ' + apellido + ' creada.', type: 'success', showConfirmButton: true })
          .then((rdo: SweetAlertResult) => {
            if (rdo.value) this.router.navigate(['/home', 'verFlia'], { queryParams: { idFam: data.id }, skipLocationChange: false });
          });

      }, (error) => {

        console.log(error);
        Swal({ title: 'Error al crear familia.', type: 'error', showConfirmButton: true })

      });
  }

}

import { SesionService } from '../../../servicios/sesion.service';
import { Component, OnInit, TemplateRef } from '@angular/core';
import {Validators,FormControl,FormGroup,FormBuilder} from '@angular/forms';
import { EstadisticaService, EstadisticaMetadata, Chart, Estadistica, EstadisticaCabecera} from '../../../servicios/estadistica.service';
import {SelectItem} from 'primeng/api';
import {formatDate, NgIf } from '@angular/common';
// import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { PacienteService } from '../../../servicios/paciente.service';
import html2canvas from 'html2canvas';
import { Router } from '@angular/router';

declare var $:any;
declare var pdfMake: any;
const mensual="mensual"
const PrimaryWhite = '#ffffff';
const SecondaryGrey = '#ccc';
const PrimaryRed = '#dd0031';
const SecondaryBlue = '#006ddd';
@Component({
  selector: 'app-estadistica',
  templateUrl: './estadistica.component.html',
  styleUrls: ['./estadistica.component.css']
})
export class EstadisticaComponent implements OnInit {
  //pdf
  docDefinition: any;
  subtitulo:string="este es el subtitulo"
  //loading
  
  public primaryColour = PrimaryRed;
  public secondaryColour = SecondaryBlue;

  //Cabecera
  estadisticaCabeceraDesnutridos:EstadisticaCabecera={nombreEstadistica:'',valorEstadistica:null,valorEstadisticaUltMes:null,valorDiferenciaUltMes:null}
  estadisticaCabecera:EstadisticaCabecera[]=[]
  fechaUltimoValor:any=formatDate(new Date(),'yyyy-MM-dd',"en-US")
  fechaMesAnt:any=""

  //Formulario para obtener estadisticas
  estadisticaform: FormGroup;
  //Externas al componente
  private loadingDownload:boolean=false;
  private loading: boolean= false;
  private loadingEstadistica: boolean=false;
  private loadingPdf: boolean = false;
  //Estadisticas
  nombreEstadisticas=[];
  estadisticas:SelectItem[];
  //Fechas
  maxDateValue:Date=new Date()
  fechaHoy:Date=new Date();
  fechaMesAnterior:any=new Date()
  fechaMesAn:any=new Date()
  //Periodo
  periodo:SelectItem[];
  //Seleccion del tipo de fechas
  periodoP="diaria"
  mensual:string="mensual"
  // periodoDos:['diaria', 'semanal', 'quincenal', 'mensual', 'anual']
  //Seleccion de chart
  chartSeleccionado:string="Niños en el programa";
  //Chart
   title=""
   chart: Estadistica[]=[]
   view: any[] = [900, 500];
   showXAxis = true;
   showYAxis = true;
   gradient = false;
   showLegend = true;
   showXAxisLabel = true;
   xAxisLabel = '';
   showYAxisLabel = true;
   yAxisLabel = '';
   colorScheme = 'picnic';

  constructor(private fb: FormBuilder, sesionSrv: SesionService, private _estadisticaService:EstadisticaService, private _pacSrv: PacienteService, private router: Router) {
    this.loading=true
    //NavBar
    sesionSrv.setNombreModulo('Estadistica');
    sesionSrv.getNombreModulo();   
    _pacSrv.setPacNavbar(null);

    this.estadisticaform = this.fb.group({
      'periodo': new FormControl('', Validators.required),
      'fechadesde': new FormControl('', Validators.required),
      'fechahasta': new FormControl('', Validators.required)
    });
    //Periodo
    this.periodo=[
      {label:'Diario', value:{id:1, name: 'diaria'}},
      {label:'Semanal', value:{id:2, name: 'semanal'}},
      {label:'Quincenal', value:{id:3, name: 'quincenal'}},
      {label:'Mensual', value:{id:4, name: 'mensual'}}
    ]
  }
  
  ngOnInit() {
    //Fecha mes anterior para traer los valores principales de la cabecera
    this.fechaMesAn= formatDate(this.fechaMesAn.setMonth(this.fechaHoy.getMonth()-1),'yyyy-MM-dd',"en-US")
    //Buscar nombre de las estadisticas que van a aparecer en lista.
    this.buscarNombreEstadisticas();
    //Setear formato de la fecha de 3 meses atras para hacer la busqueda inicial de estadisticas.
    this.fechaMesAnterior= formatDate(this.fechaMesAnterior.setMonth(this.fechaHoy.getMonth()-3),'yyyy-MM',"en-US")
    //Busqueda inicial de estadisticas.
    this.busquedaInicial()
  }
  
  busquedaInicial(){
    const fechaHoy=formatDate(this.fechaHoy,'yyyy-MM',"en-US")
    
    this._estadisticaService.getEstadistica("mensual", this.fechaMesAnterior, fechaHoy).subscribe((data)=>{
      // console.log(data);
      this.loading=false
      setTimeout(() => {
        this.title="Mensual - "+this.fechaMesAnterior+" a "+fechaHoy
      }, 500);
      data.forEach(element => {
      let estadisticaChart:Estadistica = {nombre: '', xAxisLabel: '', yAxisLabel: '', data:[]};
      estadisticaChart.nombre=element.nombre;
      estadisticaChart.xAxisLabel=element.xAxisLabel;
      estadisticaChart.yAxisLabel=element.yAxisLabel;
       for (let i = 0; i < element.data.length; i++) {
         estadisticaChart.data.push(element.data[i])
       }
       this.chart.push(estadisticaChart)

      });
      
    })
  }

  buscarNombreEstadisticas(){ //Busca el nombre todas la estadisticas disponibles
    let desnutridos:number=0;
    
    this._estadisticaService.getEstadisticaMetadata()
      .subscribe((data: EstadisticaMetadata[]) => {

        // console.log("dentro de buscar nombre estadsitcia",data);
        for (let i = 0; i < data.length; i++) {
          let nombreEstad={name: data[i].nombreEstadistica, idEstadistica:data[i].idEstadistica}
          this.nombreEstadisticas= this.nombreEstadisticas.concat([nombreEstad])
          let estadisticaCabecera:EstadisticaCabecera={nombreEstadistica:'',valorEstadistica:null,valorEstadisticaUltMes:null,valorDiferenciaUltMes:null}
          if (data[i].nombreEstadistica=="Niños en el programa" || data[i].nombreEstadistica=="Madres en el programa" || data[i].nombreEstadistica=="Desnutridos leves" || data[i].nombreEstadistica=="Desnutridos moderados" || data[i].nombreEstadistica=="Desnutridos graves" || data[i].nombreEstadistica=="Beneficiarios indirectos") {
            this._estadisticaService.getEstadisticaId(data[i].idEstadistica,"diaria",this.fechaMesAn,this.fechaUltimoValor).subscribe((data:Estadistica)=>{
              if (data.nombre=="Desnutridos leves" || data.nombre=="Desnutridos moderados" || data.nombre=="Desnutridos graves") {
                if (data.nombre=="Desnutridos leves") {
                  estadisticaCabecera.nombreEstadistica="Desnutridos" //Necesito que una sola vez se haga la asignacion a estadisticaCabecera.nombreEstadistica para que no se repita 3 veces en el html
                }
                // console.log("dentro de la busqueda por id", data);
                this.estadisticaCabeceraDesnutridos.nombreEstadistica="Niños desnutridos"
                data.data.forEach((element, index, array) => {
                  if (index==0) {
                    //Valor del mes anterior de la estadistica
                    this.estadisticaCabeceraDesnutridos.valorEstadisticaUltMes=this.estadisticaCabeceraDesnutridos.valorEstadisticaUltMes+element.value
                  }
                  if (index === (array.length -1)) {
                    //Valor actual de la estadistica
                    this.estadisticaCabeceraDesnutridos.valorEstadistica=this.estadisticaCabeceraDesnutridos.valorEstadistica+element.value
                    this.estadisticaCabeceraDesnutridos.valorDiferenciaUltMes=this.estadisticaCabeceraDesnutridos.valorEstadistica-this.estadisticaCabeceraDesnutridos.valorEstadisticaUltMes
                  }
              });
              }
              else{
                estadisticaCabecera.nombreEstadistica=data.nombre
              
              this.loading=false
              
              data.data.forEach((element, index, array) => {
                  if (index==0) {
                    //Valor del mes anterior de la estadistica
                    estadisticaCabecera.valorEstadisticaUltMes=element.value
                    // console.log("dentro de if por desnutridos", data.nombre);
                  }
                  if (index === (array.length -1)) {
                    //Valor actual de la estadistica
                    estadisticaCabecera.valorEstadistica=element.value
                    estadisticaCabecera.valorDiferenciaUltMes=estadisticaCabecera.valorEstadistica-estadisticaCabecera.valorEstadisticaUltMes
                  }
              });}
            })
            this.estadisticaCabecera.push(estadisticaCabecera)  
          }
        }
      });
  }
  
  buscarEstadisticas(form){
    // console.log(form);
    
    this.loadingEstadistica=true
    let fechaDesde
    let fechaHasta
    if (form.value.periodo.name=="mensual") {
      fechaDesde= formatDate(form.value.fechadesde,'yyyy-MM',"en-US")
      fechaHasta= formatDate(form.value.fechahasta,'yyyy-MM',"en-US")
      
      
      setTimeout(() => {
        console.log(form.value.periodo.name||"Mensual"+" - "+fechaDesde+" a "+fechaHasta);
        
        this.title="Mensual"+" - "+fechaDesde+" a "+fechaHasta
      }, 500);  
      
    }
    else{
      fechaDesde= formatDate(form.value.fechadesde,'yyyy-MM-dd',"en-US")
      fechaHasta= formatDate(form.value.fechahasta,'yyyy-MM-dd',"en-US")
      let periodo= ""
      if (form.value.periodo.name) {
        periodo = form.value.periodo.name.charAt(0).toUpperCase() + form.value.periodo.name.slice(1)  
      }
      
      else{
        periodo = "Diaria"
      }
      
      
      setTimeout(() => {
        console.log("fecha desde",fechaDesde+" fecha hasta "+fechaHasta);
        this.title=periodo+" - "+fechaDesde+" a "+fechaHasta
        // console.log(this.title);
        
      }, 500);
      
    }
    
    this.chart=[]
    this._estadisticaService.getEstadistica(form.value.periodo.name||"diaria", fechaDesde, fechaHasta).subscribe((data)=>{
      this.loadingEstadistica=false
      data.forEach(element => {
      let estadisticaChart:Estadistica = {nombre: '', xAxisLabel: '', yAxisLabel: '', data:[]};
      estadisticaChart.nombre=element.nombre;
      estadisticaChart.xAxisLabel=element.xAxisLabel;
      estadisticaChart.yAxisLabel=element.yAxisLabel;
       for (let i = 0; i < element.data.length; i++) {
         estadisticaChart.data.push(element.data[i])
        
       }
       this.chart.push(estadisticaChart)
       this.subtitulo="cambio subtitulo" 
      });
      // console.log(this.chart);
      })
    }
  cambiarFecha(periodo){
    this.periodoP=periodo
    // console.log(this.periodoP);
  }
  
  visualizarGrafico(estadistica){
    this.chartSeleccionado=estadistica
  } 

  setLoading(loading){
    this.loadingPdf = loading;
  }

  
    
}




// //   obtenerTodasEstadisticas(form){ // todas las estadisticas
//     //this.router.navigate(['estadistica-detalle']);
//     this.formulario=form;
//     //validacion fecha
//     let fechaHoy = formatDate(new Date(),'yyyy-MM-dd',"en-US")
//     if (fechaHoy <= form.value.fechaDesde || fechaHoy <= form.value.fechaHasta || form.value.fechaHasta  <= form.value.fechaDesde ) {
//         this.limpiar()
//         this.alertEstadisticas=true
//         if (this.alertEstadisticas) {
//         return this._notifSrv.showalert('La fecha ingresada es incorrecta.','danger', 1500);
//         }
//     }
//     this.estadisticaArray=[];


//     this._estadisticaService.getEstadistica(form.value.fechaDesde,form.value.fechaHasta).subscribe(
//         ( data: Estadistica[] ) => {
//           let promesa = new Promise((resolve)=>{resolve(data);})

//           promesa.then((data:Estadistica[])=>{
//             console.log("entra en la promise");


//             let i = 0;
//             let single = []
//             this.cantidadEstadisticas=0
//             data.forEach(element =>{
//                 this.cantidadEstadisticas++
//               //let estadisticas:Estadistica;
//                 let contador =0;
//                 console.log(element)
//                 let estadis:Estadistica = {nombre: '', xAxisLabel: '', yAxisLabel: '', data:[], valorMaximo:0, valorMinimo:0, promedio:0, totalValor:0, ultimo:0};

//                 estadis.nombre=element.nombre
//                 estadis.xAxisLabel=element.xAxisLabel
//                 estadis.yAxisLabel=element.yAxisLabel
//                 for (let i = 0; i < element.data.length; i++) {
//                   estadis.data.push(element.data[i])
//                   estadis.totalValor+=element.data[i].value
//                   estadis.ultimo=estadis.valorMinimo=element.data[i].value
//                   console.log("element data: ", element.data[i].value, "estadis: ", estadis.valorMinimo)
//                   if (element.data[i].value < estadis.valorMinimo || estadis.valorMinimo==0) {
//                     estadis.valorMinimo=element.data[i].value
//                   }
//                   if (estadis.valorMaximo < element.data[i].value) {
//                     estadis.valorMaximo=element.data[i].value
//                   }
//                   contador++;
//                 }
//                 ++i;
//                 estadis.promedio=estadis.totalValor/contador
//                 this.estadisticaArray.push(estadis)
//             })

//             this.estadisticaArray.forEach(element => {
//               this.buildGraph(element.data)
//             });

//           })
//       }
//     );
//     setTimeout(() => {
//       this.render()
//     }, 5000);
//   }


// options
// showXAxis = true;
// showYAxis = true;
// gradient = false;
// showLegend = true;
// showXAxisLabel = true;
// xAxisLabel = '';
// showYAxisLabel = true;
// yAxisLabel = '';
// legendTitle = "Referencias";
// roundEdges = true;
// animations = true;
// colorScheme = 'picnic';
// // formulario:any;
// //Datos estadísticos
// cantidadEstadisticas=0;
// sumaDatos=0;
// valorMinimo=0;
// valorMaximo=0;
// promedio=0;

  //Fecha desde y hasta para obtener estadisticas
  // fechaDesde: Date;
  // fechaHasta: Date;
  //fecha desde y hasta para calcular estadisticas
  // fechaDesdeCalculo: Date;
  // fechaHastaCalculo: Date;
  //fecha desde y hasta para que aparezcan inicialmente estadisticas al ingresar al modulo
  // fechaDesdeInit:Date=new Date();
  // fechaHastaInit:Date=new Date();

  // Mes anterior
  // this.fechaDesdeInit.setMonth(this.fechaDesdeInit.getMonth()-1)

  
  
  
  
  // obtenerEstadistica(){
  //  // validacion fecha
  // //  console.log(this.selected)
  
  //  if (!this.fechaDesde || !this.fechaHasta || this.selected.length==0) {
  //  this.alertEstadistica=true
  //  setTimeout(()=>{
  //    return this._notifSrv.showalert('Faltan datos por ingresar','danger')
  //   },200)
  //   return this._notifSrv.showalert('Faltan datos por ingresar','danger')
  //  }

  //  let fechaDesde=formatDate(new Date(this.fechaDesde),'yyyy-MM-dd',"en-US")
  //  let fechaHasta=formatDate(new Date(this.fechaHasta),'yyyy-MM-dd',"en-US")
  //  let fechaHoy= formatDate(new Date(),'yyyy-MM-dd',"en-US")
  // //  console.log(fechaHoy, fechaDesde, fechaHasta);

  //  this.loading=true
  //  if (fechaHoy <= fechaDesde || fechaHoy <= fechaHasta || fechaHasta <= fechaDesde || !fechaDesde || !fechaHasta ) {
  //       this.loading=false
  //       this.alertEstadistica=true
  //       setTimeout(()=>{
  //         return this._notifSrv.showalert('La fecha ingresada es incorrecta.','danger')
  //       },1000)
  //   }
  //   else{
  //     this.estadisticaArray=[]
  //     for (let i = 0; i < this.selected.length; i++) {
  //       let contador=0;
  //       this.cantidadEstadisticas=0
  //       console.log("test");

  //       this._estadisticaService.getEstadisticaPorId(fechaDesde,fechaHasta, this.selected[i]).subscribe(
  //         ( datos: Estadistica ) => {
  //           let promesa = new Promise((resolve)=>{resolve(datos);})
  //           promesa.then((datos:Estadistica)=>{
  //             this.loading=false
  //             let estad:Estadistica = {nombre: '', xAxisLabel: '', yAxisLabel: '', data:[], valorMaximo:0, valorMinimo:0, promedio:0, totalValor:0, ultimo:0};
  //             estad.nombre=datos.nombre
  //             estad.xAxisLabel=datos.xAxisLabel
  //             estad.yAxisLabel=datos.yAxisLabel
  //             this.cantidadEstadisticas++
  //             for (let i = 0  ; i < datos.data.length; i++) {
  //               estad.data.push(datos.data[i])
  //               estad.totalValor+=datos.data[i].value
  //               estad.ultimo=datos.data[i].value
  //               console.log("datos data: ", datos.data[i].value, "estad: ", estad.valorMinimo)
  //               if (datos.data[i].value < estad.valorMinimo || estad.valorMinimo==0) {
  //                 estad.valorMinimo=datos.data[i].value
  //               }
  //               if (estad.valorMaximo < datos.data[i].value) {
  //                 estad.valorMaximo=datos.data[i].value
  //               }
  //               contador++;
  //             }
  //             estad.promedio=estad.totalValor/contador
              
  //             this.estadisticaArray.push(estad)
  //             this.estadisticaArray.forEach(element => {
  //               this.buildGraph(element.data)
  //             });
  //           })
  //         }, (error)=> {
  //           if (error.status == 404) {
  //             this.loading=false
  //             this.alertEstadistica=true
  //             setTimeout(()=>{
  //               this.ocultarExportarPdf=true
  //               return this._notifSrv.showalert('No existen datos para la fecha ingresada.','danger')
  //             },1000)
  //           }
  //           else{
  //             this.loading=false
  //             this.alertEstadistica=true
  //             setTimeout(()=>{
  //               return this._notifSrv.showalert('Ocurrió un error. Vuelve a intentarlo.','danger')
  //             },1000)
  //           }
  //         }

  //       );
  //       setTimeout(() => {
  //           this.render(fechaDesde, fechaHasta)
  //     }, 5000);
  //     }
  //   }

  // }




  // calcularEstadisticas(){
  //   if (!this.fechaDesdeCalculo || !this.fechaHastaCalculo) {
  //     this.alertEstadistica=true
  //     setTimeout(()=>{
  //       return this._notifSrv.showalert('Faltan datos por ingresar','danger')
  //      },200)
  //      return this._notifSrv.showalert('Faltan datos por ingresar','danger')
  //     }
  //   // console.log(formCalculo)
  //   // console.log(formCalculo.form.controls.fechaDesde.touched)
  //   let fechaHoy = formatDate(new Date(),'yyyy-MM-dd',"en-US")
  //   let fechaDesdeCalculo=formatDate(new Date(this.fechaDesdeCalculo),'yyyy-MM-dd',"en-US")
  //   let fechaHastaCalculo=formatDate(new Date(this.fechaHastaCalculo),'yyyy-MM-dd',"en-US")
  //   if (fechaHoy < fechaDesdeCalculo || fechaHoy < fechaHastaCalculo || fechaHastaCalculo  <= fechaDesdeCalculo ) {
  //       this.limpiar()
  //       this.alertEstadisticas=true
  //       return this._notifSrv.showalert('La fecha ingresada es incorrecta.','danger', 1500);
  //   }
  //   this._estadisticaService.calculoEstadistica(fechaDesdeCalculo, fechaHastaCalculo)
  //   .subscribe(
  //     (data: JSON)=> {
  //       console.log("Solicitud enviada, para visualizar, aguarde un instante",data)
  //       $('#ModalCalculoEstadisticas').modal('hide');
  //       // this.alertEstadistica==true
  //       return this._notifSrv.showalert('Procesando cálculo. <br> En minutos podrás obtener este reporte.','success', 3500);
  //     }, error=>{
  //       // this.alertEstadisticas=true
        
  //       this._notifSrv.showalert('No se pudo calcular la estadística.','danger', 1500);
  //       console.log("error al calcular recordatorio", error);

  //     })
  // }


    // rederBarChart(){
  // html2canvas(document.getElementById('barChart'), {height: 1000})
  //   .then((canvas) => {
  //     document.body.appendChild(canvas);
  //   })
  // }
  // rederGroupedBarChart(){
  //   html2canvas(document.getElementById('groupedBarChart'), {height: 1000})
  //     .then((canvas) => {
  //       document.body.appendChild(canvas);
  //     });
  // }


    // buildGraph(data){
  //   new Promise((resolve)=>{
  //     let single=data;
  //     Object.assign(this, {single})
  //     resolve(single)}).then((single)=>{
  //       console.log("se resuelve con ", single);
  //       if(single){
  //         this.activarGraf=true}
  //     })
  //   this.selected=[]
  // }

    // downloadChart() {
  //   // Download PDF
  //   console.log(this.docDefinition)
  //   if (this.docDefinition) {
  //       pdfMake.createPdf(this.docDefinition).download('Reporte '+formatDate(new Date(),'dd/MM/yyyy','en-US')+ '.pdf');
  //   } else {
  //     this._notifSrv.showalert('El gráfico no pudo ser renderizado, vuelve intentarlo','danger');
  //     console.log('Chart is not yet rendered!');
  //   }

  // }

  // render(fechaDesde, fechaHasta){
  //   // setTimeout(() => {
  //         // Charts are now rendered
  //         console.log("renderizando")
  //         const chart = document.getElementById('chart');
  //         html2canvas(chart, {
  //           // height: 1200,
  //           height: 600,
  //           width: 1000,
  //           scale: 3,
  //           backgroundColor: null,
  //           logging: false,
  //           onclone: (document) => {
  //             // console.log(document.getElementById('chart'))
  //             document.getElementById('chart').style.visibility = 'visible';
  //           }
  //         }).then((canvas) => {
  //           // Get chart data so we can append to the pdf
  //           const chartData = canvas.toDataURL();
  //           // Prepare pdf structure
  //           const docDefinition = { content: [],
  //             styles: {
  //               subheader: {
  //                 fontSize: 16,
  //                 bold: true,
  //                 margin: [0, 10, 0, 15],
  //                 alignment: 'center'
  //               },
  //               subsubheader: {
  //                 fontSize: 12,
  //                 italics: true,
  //                 margin: [0, 10, 0, 5],
  //                 alignment: 'left'
  //               },
  //               footer: {
  //                 fontSize: 8,
  //                 italics: true,
  //                 margin: [210, 160, 0, 5],
  //                 alignment: 'left'
  //               },
  //               footerdos: {
  //                 fontSize: 8,
  //                 italics: true,
  //                 margin: [210, 15, 0, 5],
  //                 alignment: 'left'
  //               }
  //             },
  //             defaultStyle: {
  //               // alignment: 'justify'
  //             }
  //           };

  //           // Add some content to the pdf
  //           // const title = {text: 'Reporte estadísticas CENTRO PREVENCION Las Heras', style: 'subheader'};
  //           // console.log(this.formulario)
  //           const title = {text: 'Reporte de estadísticas', style: 'subheader'};
  //           const fechagen = {text: 'Fecha de generación:  '+formatDate(new Date(fechaDesde),'dd/MM/yyyy','en-US')+'   --   '+formatDate(new Date(fechaHasta),'dd/MM/yyyy','en-US'), style: 'subsubheader'};
  //           docDefinition.content.push(title);
  //           docDefinition.content.push(fechagen);
  //           // Push image of the chart
  //           docDefinition.content.push({image: chartData, width: 500, margin:[60,40,0,15]});
  //           const fechaimp = {text: 'Fecha de impresión: '+this.fechaHoy, style: 'footer'};
  //           docDefinition.content.push(fechaimp);
  //           // this.docDefinition = docDefinition;
  //           // pdfMake.createPdf(docDefinition).download('chartToPdf' + '.pdf');
  //         });
  //       // }, 1100);
  //       // setTimeout(() => {this.downloadChart();}, 15000);
  //       // this.downloadChart()
  // }

    // docDefinition: any;
    // view: any[] = [700, 400];

    // pruebaConPacientes(){
    //   //los pacientes que te trae la consulta, los arme asi rapidamente.
    //   let guachin:{nombre:string, apellido:string, dia:string}[]
    //   guachin=[
    //     {nombre:'lucas',apellido:'alguien', dia:'Lunes'},
    //     {nombre:'Martin',apellido:'alguien', dia:'Martes'},
    //     {nombre:'Julian',apellido:'alguien', dia:'Miercoles'},
    //     {nombre:'Fernando',apellido:'alguien', dia:'Jueves'},
    //     {nombre:'Fabio',apellido:'alguien', dia:'Viernes'},
    //     {nombre:'Juan',apellido:'alguien', dia:'Lunes'},
    //     {nombre:'Benjamin',apellido:'alguien', dia:'Martes'},
    //     {nombre:'Ricardo',apellido:'alguien', dia:'Miercoles'},
    //     {nombre:'Franco',apellido:'alguien', dia:'Jueves'},
    //     {nombre:'Yair',apellido:'alguien', dia:'Viernes'}
    //   ]
      
    //   //haces un for por cada dia y por cada guachin que te trajo la consulta
      
    //   for (let i = 0; i < this.asistenciaPacientes.length; i++) {
    //     for (let j = 0; j < guachin.length; j++) {
    //       if (guachin[j].dia=this.asistenciaPacientes[i].dia) {      
    //         this.asistenciaPacientes[i].paciente.push()
    //       }
    //     }  
    //   }
      
    
    // }

  //    //Prueba con paciente.
  // asistenciaPacientes:PacienteAsistencia[]=[];
  // dia:string[]=["Lunes","Martes","Miercoles","Jueves","Viernes"]
  // single: any[]= [
  //   {
  //     "name": "Germany",
  //     "value": 8940000
  //   },
  //   {
  //     "name": "USA",
  //     "value": 5000000
  //   },
  //   {
  //     "name": "France",
  //     "value": 7200000
  //   },
  //   {
  //     "name": "Franch",
  //     "value": 1200000
  //   },
  //   {
  //     "name": "quisiyo",
  //     "value": 1900000
  //   },
  //   {
  //     "name": "noslosrepresentantes",
  //     "value": 3000000
  //   },
  //   {
  //     "name": "noslosprepesentan",
  //     "value": 700000
  //   },
  //   {
  //     "name": "noslosrepresen",
  //     "value": 2200000
  //   },
  //   {
  //     "name": "noslosrepre",
  //     "value": 8200000
  //   },
  //   {
  //     "name": "noslosresd",
  //     "value": 6200000
  //   },
  //   {
  //     "name": "noslosasd",
  //     "value": 5200000
  //   },
  //   {
  //     "name": "nosasd",
  //     "value": 3200000
  //   },
  //   {
  //     "name": "nodss",
  //     "value": 2800000
  //   },
  //   {
  //     "name": "nosas",
  //     "value": 7200000
  //   }
  // ];






 /* render(fechaDesde, fechaHasta){
    // setTimeout(() => {
          console.log("renderizando")
          
          const chart = document.getElementById('charter');
          html2canvas(chart, {
            height: 600,
            width: 1000,
            scale: 3,
            backgroundColor: null,
            logging: false,
            onclone: (document) => {
              console.log(document.getElementById('charter'))
              document.getElementById('charter').style.visibility = 'visible';
            }
          }).then((canvas) => {
            // Get chart data so we can append to the pdf
            const chartData = canvas.toDataURL();
            // Prepare pdf structure
            const docDefinition = { content: [],
              styles: {
                subheader: {
                  fontSize: 16,
                  bold: true,
                  margin: [0, 10, 0, 15],
                  alignment: 'center'
                },
                subsubheader: {
                  fontSize: 12,
                  italics: true,
                  margin: [0, 10, 0, 5],
                  alignment: 'left'
                },
                footer: {
                  fontSize: 8,
                  italics: true,
                  margin: [210, 160, 0, 5],
                  alignment: 'left'
                },
                footerdos: {
                  fontSize: 8,
                  italics: true,
                  margin: [210, 15, 0, 5],
                  alignment: 'left'
                }
              },
              defaultStyle: {
                // alignment: 'justify'
              }
            };

            // Add some content to the pdf
            // const title = {text: 'Reporte estadísticas CENTRO PREVENCION Las Heras', style: 'subheader'};
            // console.log(this.formulario)
            const title = {text: 'Reporte de estadísticas', style: 'subheader'};
            const fechagen = {text: 'Fecha de generación:  '+formatDate(new Date(fechaDesde),'dd/MM/yyyy','en-US')+'   --   '+formatDate(new Date(fechaHasta),'dd/MM/yyyy','en-US'), style: 'subsubheader'};
            docDefinition.content.push(title);
            docDefinition.content.push(fechagen);
            // Push image of the chart
            docDefinition.content.push({image: chartData, width: 500, margin:[60,40,0,15]});


            // if (this.cantidadEstadisticas==1) {
            //   const fechaimp = {text: 'Fecha de impresión: '+this.fechaHoy, style: 'footer'};
            //   docDefinition.content.push(fechaimp);
            // }
            // else{
            //   const fechaimp = {text: 'Fecha de impresión: '+this.fechaHoy, style: 'footerdos'};
            //   docDefinition.content.push(fechaimp);
            // }


            this.docDefinition = docDefinition;
            // pdfMake.createPdf(docDefinition).download('chartToPdf' + '.pdf');
          });
        // }, 1100);
        // setTimeout(() => {this.downloadChart();}, 15000);
        // this.downloadChart()
  }

  downloadChart() {
    this.loadingDownload=true
    setTimeout(() => {
      this.loadingDownload=false
      // Download PDF
    console.log(this.docDefinition)
    if (this.docDefinition) {
        pdfMake.createPdf(this.docDefinition).download('Reporte '+formatDate(new Date(),'dd/MM/yyyy','en-US')+ '.pdf');
    } else {
      // this._notifSrv.showalert('El gráfico no pudo ser renderizado, vuelve intentarlo','danger');
      console.log('Chart is not yet rendered!');
    }
    }, 2500);

    

  }*/
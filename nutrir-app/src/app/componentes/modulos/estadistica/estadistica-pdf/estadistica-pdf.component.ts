import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import html2canvas from 'html2canvas';
import { formatDate } from '@angular/common';
import { EstadisticaService, Estadistica } from 'src/app/servicios/estadistica.service';
declare var pdfMake: any;
@Component({
  selector: 'app-estadistica-pdf',
  templateUrl: './estadistica-pdf.component.html',
  styleUrls: ['./estadistica-pdf.component.css']
})
export class EstadisticaPdfComponent implements OnInit {
  //pdf
  docDefinition: any;
  docDefinition2:any;
  //Loading
  private loadingDownload:boolean=false;
  //fechas
  fechaHoy:Date=new Date();
  //chart a exportar
  @Input() chartExportar:Estadistica[]=[]
  @Input() titleExportar:string=""
  private subtitulo:string;
  view: any[] = [900, 500];
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = '';
  showYAxisLabel = true;
  yAxisLabel = '';
  colorScheme = 'picnic';
  //evento
  @Output() loading = new EventEmitter<boolean>();
  constructor(private _estadisticaService:EstadisticaService) { 
    
  }
  
  ngOnInit() {
    console.log(this.chartExportar);
    console.log(this.titleExportar);

    //  setTimeout(() => {
    //   this.render();
      
    //       }, 2500);
  }
  ngOnChanges() {
    this.subtitulo=this.titleExportar;
    console.log("ngonchanges", this.titleExportar);
    
  }
  render(){
          console.log("renderizando")
            this.loading.emit(true);

            const title = {text: 'Reporte de estadísticas - NUTRIR', style: 'subheader'};
            // const fechagen = {text: 'Fecha de generación:  '+formatDate(new Date(fechaDesde),'dd/MM/yyyy','en-US')+'   --   '+formatDate(new Date(fechaHasta),'dd/MM/yyyy','en-US'), style: 'subsubheader'};
            const fecha = {text: this.titleExportar, style: 'subsubheader'}
            const docDefinition2={ content: [],
              styles: {
                subheader: {
                  fontSize: 16,
                  bold: true,
                  margin: [0, 10, 0, 15],
                  alignment: 'center'
                },
                subsubheader: {
                  fontSize: 12,
                  italics: true,
                  margin: [0, 10, 0, 5],
                  alignment: 'left'
                },
                footer: {
                  fontSize: 8,
                  italics: true,
                  margin: [210, 160, 0, 5],
                  alignment: 'left'
                },
                footerdos: {
                  fontSize: 8,
                  italics: true,
                  margin: [210, 15, 0, 5],
                  alignment: 'left'
                }
              },
              defaultStyle: {
                // alignment: 'justify'
              }
            }

          setTimeout(() => {

          docDefinition2.content.push(title);
          docDefinition2.content.push(fecha);          
          Array.from(document.getElementsByClassName("printChart")).forEach(function(item) {
                      html2canvas(item, {
                      height: 1000,
                      width: 1000,
                      scale: 3,
                      backgroundColor: null,
                      logging: false,
                      onclone: (document) => {
                        console.log(document.getElementById(item.id))
                        document.getElementById(item.id).style.visibility = 'visible';
                      }
                    }).then((canvas) => {
                      const chartData = canvas.toDataURL();
                      docDefinition2.content.push({image: chartData, width: 500, margin:[30,40,0,15]});
                    });

            console.log(item.id);
         });
         
                    }, 2300);
                    setTimeout(() => {
                      console.log(docDefinition2.content);  
                      console.log("pasando variable a ambiente global");
                      pdfMake.createPdf(docDefinition2).download('Reporte de estadísticas - NUTRIR' + '.pdf');
                      
                      setTimeout(() => {this.loading.emit(false)}, 2000);
                      // this.docDefinition2=docDefinition2
                    }, 20000);
          

  }

  exportarAPdf(){
    

    this.loading.emit(true)
    console.log("doc2", this.docDefinition2)
    setTimeout(() => {
      
      this.loading.emit(false);
      pdfMake.createPdf(this.docDefinition2).download('Reporte de estadísticas' + '.pdf');
          }, 5000);
          
  }
  renderSingle(i){
   
          console.log("renderizando")
          const docDefinition2 = { content: [],
              styles: {
                subheader: {
                  fontSize: 16,
                  bold: true,
                  margin: [0, 10, 0, 15],
                  alignment: 'center'
                },
                subsubheader: {
                  fontSize: 12,
                  italics: true,
                  margin: [0, 10, 0, 5],
                  alignment: 'left'
                },
                footer: {
                  fontSize: 8,
                  italics: true,
                  margin: [210, 160, 0, 5],
                  alignment: 'left'
                },
                footerdos: {
                  fontSize: 8,
                  italics: true,
                  margin: [210, 15, 0, 5],
                  alignment: 'left'
                }
              },
              defaultStyle: {
                // alignment: 'justify'
              }
            };

            const title = {text: 'Reporte de estadísticas', style: 'subheader'};
            // const fechagen = {text: 'Fecha de generación:  '+formatDate(new Date(fechaDesde),'dd/MM/yyyy','en-US')+'   --   '+formatDate(new Date(fechaHasta),'dd/MM/yyyy','en-US'), style: 'subsubheader'};
            const fecha = {text: this.titleExportar, style: 'subsubheader'}
            docDefinition2.content.push(title);
            docDefinition2.content.push(fecha);
          
          const chart = document.getElementById('chart'+i);
          html2canvas(chart, {
            height: 1000,
            width: 1000,
            scale: 3,
            backgroundColor: null,
            logging: false,
            onclone: (document) => {
              console.log(document.getElementById('chart0'))
              document.getElementById('chart0').style.visibility = 'visible';
            }
          }).then((canvas) => {
            // Get chart data so we can append to the pdf
            const chartData = canvas.toDataURL();
            // Prepare pdf structure
            

            // const title = {text: 'Reporte estadísticas CENTRO PREVENCION Las Heras', style: 'subheader'};
            // console.log(this.formulario)
            
            // docDefinition.content.push(fechagen);
            // Push image of the chart
            docDefinition2.content.push({image: chartData, width: 500, margin:[30,40,0,15]});


            // if (this.cantidadEstadisticas==1) {
            //   const fechaimp = {text: 'Fecha de impresión: '+this.fechaHoy, style: 'footer'};
            //   docDefinition.content.push(fechaimp);
            // }
            // else{
            //   const fechaimp = {text: 'Fecha de impresión: '+this.fechaHoy, style: 'footerdos'};
            //   docDefinition.content.push(fechaimp);
            // }
            console.log(docDefinition2);
            pdfMake.createPdf(docDefinition2).download('Reporte de estadísticas - NUTRIR' + '.pdf');
            // this.docDefinition = docDefinition2
          });


          

        // this.downloadChart()
  }

  downloadChart() {
    this.loading.emit(true);
    setTimeout(() => {
      this.loading.emit(false);
      // Download PDF
    console.log(this.docDefinition)
    if (this.docDefinition) {
      pdfMake.createPdf(this.docDefinition).download('Reporte '+formatDate(new Date(),'dd/MM/yyyy','en-US')+ '.pdf');
    } else {
      // this._notifSrv.showalert('El gráfico no pudo ser renderizado, vuelve intentarlo','danger');
      console.log('Chart is not yet rendered!');
    }
    }, 2500);
  }
}

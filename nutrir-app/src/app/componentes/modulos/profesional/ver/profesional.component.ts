import { Component, OnInit, OnDestroy } from '@angular/core';
import { SesionService } from '../../../../servicios/sesion.service';
import { ProfesionalService, Profesional } from '../../../../servicios/profesional.service';
import { DomicilioService, Pais, Provincia, Domicilio } from '../../../../servicios/domicilio.service';
import { CentroService, Centro } from '../../../../servicios/centro.service';
import { Subscription } from 'rxjs';
import { PacienteService } from 'src/app/servicios/paciente.service';

@Component({
  selector: 'app-profesional',
  templateUrl: './profesional.component.html'
})
export class ProfesionalComponent implements OnInit, OnDestroy {

  private inscripcionesObs: Subscription = new Subscription();
  private profesional: Profesional;
  private sexoProf: string;
  private domProf: Domicilio;
  private provProf: Provincia;
  private paisProf: Pais;
  private idPais: number;
  private idProv: number;
  private centroProf: string;
  private loading: boolean;
  private rolProf: string;
  private promise: Promise<{}>

  constructor(private _sesionSrv: SesionService, private _profSrv: ProfesionalService,
    private _dom: DomicilioService, private _centro: CentroService, private pacSrv: PacienteService) {

    this.loading = true;
    pacSrv.setPacNavbar(null);
    _sesionSrv.setNombreModulo(null);
    this.traerProfesional();
  }

  ngOnInit() {
    this.promise.then((profesional: Profesional) => {
      this.traerDom(profesional);
      this.traerSexo(profesional);
      this.traerCentro(profesional);
    })
  }

  ngOnDestroy() {
    if (this.inscripcionesObs) {
      this.inscripcionesObs.unsubscribe();
    }
  }

  traerProfesional() {
    this.promise = new Promise((resolve) => {
      this.inscripcionesObs = this._profSrv.getProfesionalid(this._sesionSrv.getProfesional().idProfesional).subscribe((datos: Profesional) => {
        this.profesional = datos;
        resolve(this.profesional);
      })
    })
  }

  traerSexo(profesional: Profesional) {
    var inscripcionesObs = this._profSrv.getSexoId(profesional.sexo).subscribe((datos: any) => {
      this.sexoProf = datos.nombreSexo;
      this.inscripcionesObs.add(inscripcionesObs);
    }, (error) => console.log('Error al traer el sexo del profesional', error));
  }

  traerCentro(profesional: Profesional) {
    var inscripcionesObs = this._centro.getCentroId(profesional.centro).subscribe((datos: Centro) => {
      this.centroProf = datos.nombreCentro;
      this.inscripcionesObs.add(inscripcionesObs);
    }, error => console.log('Error al traer el sexo del profesional', error));
  }

  traerDom(profesional: Profesional) {
    var inscripcionesObs = this._dom.getDomicilioProfesionalid(profesional.domicilio).subscribe((datos: any) => {
      this.domProf = datos;
      this.idProv = datos.localidad.provincia;
      this.inscripcionesObs.add(inscripcionesObs);
      this.traerProv(this.idProv);
    }, error => console.log('Error al traer el domicilio del profesional', error));
  }

  traerPais(id: number) {
    var inscripcionesObs = this._dom.getPaisId(id).subscribe((datos: Pais) => {
      this.paisProf = datos;
      this.inscripcionesObs.add(inscripcionesObs);
    }, error => console.log('Error al traer el pais de residencia del profesional', error));

    this.loading = false; //Terminar loading.
  }

  traerProv(id: number) {
    var inscripcionesObs = this._dom.getProvinciaId(id).subscribe((datos: Provincia) => {
      this.provProf = datos;
      this.idPais = datos.pais;
      this.inscripcionesObs.add(inscripcionesObs);
      this.traerPais(this.idPais);
    }, error => console.log('Error al traer la provincia del profesional', error));
  }

  getidProv() {
    return this.idProv;
  }

  getidPais() {
    return this.idPais;
  }

}
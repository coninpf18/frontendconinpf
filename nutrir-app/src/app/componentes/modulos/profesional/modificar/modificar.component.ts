import { Component, OnInit } from '@angular/core';
import { SesionService } from '../../../../servicios/sesion.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProfesionalService, Profesional } from '../../../../servicios/profesional.service';
import { DomicilioService, Provincia, Localidad, Pais, Domicilio } from '../../../../servicios/domicilio.service';
import { Router } from '@angular/router';
import { Centro, CentroService } from '../../../../servicios/centro.service';
import { formatDate } from '@angular/common';
import { flatMap } from 'rxjs/operators';
import { BackService } from 'src/app/servicios/back.service';
import Swal, { SweetAlertResult } from 'sweetalert2'

declare let $: any;

@Component({
  selector: 'app-modificarProf',
  templateUrl: './modificar.component.html',
  styles: [`
    .volverButton{ position: absolute; top: 75px; right: 25px; } 

    input.ng-invalid.ng-touched{ border: 1px solid red; }

    select.ng-invalid.ng-touched{ border: 1px solid red; }

    .tamanio_select{ width: 20rem; }

    .form-control {
      opacity: 1;
      border-color: rgb(177, 175, 175);
    }
    `]
})

export class ModProfesionalComponent implements OnInit {

  //atributos sueltos. Algunos nomencladores y otros datos de entidades foraneas.
  private loading: boolean;
  private formularioprofesional: FormGroup;

  //array de sexo y lugares para mostrar por lista.
  private localidadesDisponibles: Localidad[];
  private provinciasDisponibles: Provincia[];
  private paisesDisponibles: Pais[];
  private centrosDisponibles: Centro[];
  private sexosDisponibles: any[];
  private fechaFiltro: string;

  constructor(private _sesionSrv: SesionService, private _profSrv: ProfesionalService,
    private domSrv: DomicilioService, private _centro: CentroService, private _router: Router, private _backSrv: BackService) {

    this.formularioprofesional = new FormGroup({
      idProfesional: new FormControl(null),
      nombreProfesional: new FormControl('', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ \s]*')]),
      apellidoProfesional: new FormControl('', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ \s]*')]),
      dniProfesional: new FormControl(null, [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(7)]),
      fechaNacimiento: new FormControl('', Validators.required),
      matriculaProfesional: new FormControl(null, [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(2)]),
      telefonoProfesional: new FormControl(null, [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(6)]),
      tituloProfesional: new FormControl(null, Validators.required),
      fecha_modificado: new FormControl(''),
      centro: new FormControl(null, Validators.required),
      domicilio: new FormControl(null),
      sexo: new FormControl(null, Validators.required),
      residenciaFormulario: new FormGroup({
        domicilioForm: new FormGroup({
          idDomicilio: new FormControl(null),
          calle: new FormControl('', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ \s]*')]),
          departamento: new FormControl(null, [Validators.pattern('[0-9]*'), Validators.min(-1)]),
          nroDomicilio: new FormControl(null, [Validators.required, Validators.pattern('[0-9]*'), Validators.min(0)]),
          piso: new FormControl(null, [Validators.pattern('[0-9]*'), Validators.min(-1)]),
          localidad: new FormGroup({
            idLocalidad: new FormControl(null, Validators.required),
            nombreLocalidad: new FormControl(''),
            provincia: new FormControl(null)
          }),
        }),
        provinciaForm: new FormGroup({
          idProvincia: new FormControl(null, Validators.required),
          nombreProvincia: new FormControl(''),
          pais: new FormControl(null)
        }),
        paisForm: new FormGroup({
          idPais: new FormControl(null, Validators.required),
          nombrePais: new FormControl('')
        })
      })
    })
  }

  ngOnInit() {

    this.loading = true;
    this.mostrarDatos();

    let ayer = new Date();
    ayer.setDate(ayer.getDate() - 1);
    this.fechaFiltro = ayer.toISOString().slice(0, 10);

    console.log(this.fechaFiltro);

  }

  mostrarDatos() {

    let prof: Profesional = this._sesionSrv.getProf();

    this.formularioprofesional.controls['idProfesional'].setValue(prof.idProfesional);
    this.formularioprofesional.controls['nombreProfesional'].setValue(prof.nombreProfesional);
    this.formularioprofesional.controls['apellidoProfesional'].setValue(prof.apellidoProfesional);
    this.formularioprofesional.controls['dniProfesional'].setValue(prof.dniProfesional);
    this.formularioprofesional.controls['telefonoProfesional'].setValue(prof.telefonoProfesional);
    this.formularioprofesional.controls['centro'].setValue(prof.centro);
    this.formularioprofesional.controls['matriculaProfesional'].setValue(prof.matriculaProfesional);
    this.formularioprofesional.controls['tituloProfesional'].setValue(prof.tituloProfesional);
    this.formularioprofesional.controls['domicilio'].setValue(prof.domicilio);
    this.formularioprofesional.controls['sexo'].setValue(prof.sexo);
    this.formularioprofesional.controls['fechaNacimiento'].setValue(formatDate(prof.fechaNacimiento, 'yyyy-MM-dd', "en-US"));

    this.obtenerDomicilio(this.formularioprofesional.controls['domicilio'].value);


    //Traer los nomencladores.
    this._profSrv.getSexo()
      .subscribe((datos) => this.sexosDisponibles = datos);

    this._centro.getCentros()
      .subscribe((datos: Centro[]) => this.centrosDisponibles = datos);

  }

  obtenerDomicilio(id: number) {

    this.domSrv.getDomicilioProfesionalid(id)
      .pipe(flatMap((data: Domicilio) => {

        this.formularioprofesional.get('domicilio').setValue(data.idDomicilio);
        this.formularioprofesional.get('residenciaFormulario.domicilioForm.idDomicilio').setValue(data.idDomicilio); this.formularioprofesional.get('residenciaFormulario.domicilioForm.nroDomicilio').setValue(data.nroDomicilio)
        this.formularioprofesional.get('residenciaFormulario.domicilioForm.calle').setValue(data.calle); this.formularioprofesional.get('residenciaFormulario.domicilioForm.departamento').setValue(data.departamento)
        this.formularioprofesional.get('residenciaFormulario.domicilioForm.piso').setValue(data.piso)

        //Set localidad en formularios.
        this.formularioprofesional.get('residenciaFormulario.domicilioForm.localidad.idLocalidad').setValue(data.localidad.idLocalidad);
        this.formularioprofesional.get('residenciaFormulario.domicilioForm.localidad.provincia').setValue(data.localidad.provincia);
        this.formularioprofesional.get('residenciaFormulario.domicilioForm.localidad.nombreLocalidad').setValue(data.localidad.nombreLocalidad);

        return this.domSrv.getProvinciaId(data.localidad.provincia);

      }), flatMap((data: Provincia) => {

        this.formularioprofesional.get('residenciaFormulario.provinciaForm.idProvincia').setValue(data.idProvincia);
        this.formularioprofesional.get('residenciaFormulario.provinciaForm.nombreProvincia').setValue(data.nombreProvincia); //set provincia.
        this.formularioprofesional.get('residenciaFormulario.provinciaForm.pais').setValue(data.pais);

        this.traerLocLista(+this.formularioprofesional.get('residenciaFormulario.provinciaForm.idProvincia').value);

        return this.domSrv.getPaisId(data.pais);

      }))
      .subscribe((data: Pais) => {

        this.formularioprofesional.get('residenciaFormulario.paisForm.idPais').setValue(data.idPais);
        this.formularioprofesional.get('residenciaFormulario.paisForm.nombrePais').setValue(data.nombrePais);   //set pais.

        this.traerPaisLista();
        this.traerProvLista(+this.formularioprofesional.get('residenciaFormulario.paisForm.idPais').value);

        this.loading = false; //termina loading.
      });
  }

  traerPaisLista() {
    this.domSrv.getPais()
      .subscribe((datos: Pais[]) => {
        this.paisesDisponibles = datos;
      })
  }

  traerProvLista(idPais: number) {

    this.domSrv.getProvinciaIdPais(+idPais)
      .subscribe((datos: Provincia[]) => {

        if (this.formularioprofesional.get('residenciaFormulario.paisForm.idPais').dirty) {

          this.formularioprofesional.get('residenciaFormulario.provinciaForm.idProvincia').setValue(null);
          this.formularioprofesional.get('residenciaFormulario.domicilioForm.localidad.idLocalidad').setValue(null);
        }

        this.provinciasDisponibles = datos;

      });
  }

  traerLocLista(idProv: number) {

    this.domSrv.getLocalidadIdProv(+idProv)
      .subscribe((datos: Localidad[]) => {

        if (this.formularioprofesional.get('residenciaFormulario.paisForm.idPais').dirty) {
          this.formularioprofesional.get('residenciaFormulario.domicilioForm.localidad.idLocalidad').setValue(null);
        }

        this.localidadesDisponibles = datos;
      })
  }

  //Método que trae la fecha de nacimiento por defecto.
  parseDate(dateString: string): Date {
    if (dateString) {
      return new Date(dateString);
    } else {
      return null;
    }
  }

  confirmarCambios() {

    let profesional: Profesional = {
      idProfesional: this.formularioprofesional.get('idProfesional').value,
      nombreProfesional: this.formularioprofesional.get('nombreProfesional').value,
      apellidoProfesional: this.formularioprofesional.get('apellidoProfesional').value,
      dniProfesional: this.formularioprofesional.get('dniProfesional').value,
      fechaNacimiento: (this.formularioprofesional.get('fechaNacimiento').value),
      matriculaProfesional: this.formularioprofesional.get('matriculaProfesional').value,
      telefonoProfesional: this.formularioprofesional.get('telefonoProfesional').value,
      tituloProfesional: this.formularioprofesional.get('tituloProfesional').value,
      centro: this.formularioprofesional.get('centro').value,
      domicilio: this.formularioprofesional.get('domicilio').value,
      sexo: this.formularioprofesional.get('sexo').value
    };

    this._profSrv.putProfesional(profesional)
      .subscribe(() => {



        this.confirmarCambiosDom();

      }, (error) => {
        console.log('eror putProf', error);
        Swal({ title: 'Error al cargar datos del profesional.', type: 'error', showConfirmButton: true })
      });
  }

  confirmarCambiosDom() {

    let domicilio: Domicilio = {
      idDomicilio: this._sesionSrv.getProfesional().domicilio,
      calle: this.formularioprofesional.get('residenciaFormulario.domicilioForm.calle').value,
      departamento: this.formularioprofesional.get('residenciaFormulario.domicilioForm.departamento').value,
      nroDomicilio: this.formularioprofesional.get('residenciaFormulario.domicilioForm.nroDomicilio').value,
      piso: this.formularioprofesional.get('residenciaFormulario.domicilioForm.piso').value,
      localidad_id: this.formularioprofesional.get('residenciaFormulario.domicilioForm.localidad.idLocalidad').value
    }

    //Enviar datos modificados al back-end.
    this.domSrv.putDomicilio(domicilio)
      .subscribe(() => {

        Swal({ title: 'Profesional modificado.', type: 'success', showConfirmButton: true })
          .then((rdo: SweetAlertResult) => {
            if (rdo.value) this._router.navigate(['home', 'verProfesional']);
          })

      }, (error) => {
        console.log('error putDom', error);
        Swal({ title: 'Error al cargar datos del domicilio.', type: 'error', showConfirmButton: true })
      });
  }

}
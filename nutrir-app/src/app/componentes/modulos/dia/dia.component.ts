import { Component, OnInit, OnDestroy } from '@angular/core';
import { DiaService } from '../../../servicios/dia.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router'

@Component({
  selector: 'app-dia',
  templateUrl: './dia.component.html'
})

export class DiaComponent implements OnInit, OnDestroy{

  //private dia: Observable<Object>
  private diap: any[];
  private unsubscribe: Subscription= new Subscription();

  constructor(private _router:Router, private _diaService: DiaService) {
  
  }

  ngOnInit(){
    this.unsubscribe= this._diaService.getDia().subscribe((data: any)=> {
      this.diap= [];
      console.log(data);
      this.diap = data;
    })
 }

  ngOnDestroy(){
    this.unsubscribe.unsubscribe();
  }

  verDia(day: number){ 
    this._router.navigate(['/dia', day]);
  }
}

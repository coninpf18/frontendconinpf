import { Component, Input, OnInit, Output, OnChanges } from '@angular/core';
import { FormGroup, FormControl, Validators, Form } from '@angular/forms';
import { CampoBase } from '../campos/campobase';
import { FormGroupCreadorService } from '../campos/form-group-creador.service';
import { Observable, forkJoin } from 'rxjs';
import { FormularioService } from '../../../../servicios/formulario.service';
import { formatDate } from '@angular/common';
import { NotificacionService } from 'src/app/servicios/notificacion.service';
import { Router } from '@angular/router';
import Swal, { SweetAlertResult } from 'sweetalert2';


@Component({
  selector: 'app-form-dinamico',
  templateUrl: './form-dinamico.component.html',
  styleUrls: ['./form-dinamico.component.css'],
  providers: [FormGroupCreadorService]
})
export class FormDinamicoComponent implements OnChanges {

  //el decorador @Input es una forma facil de recibir atributos de otros componentes, se envian especificandolos en el html del componente emisor y se reciben en el receptor de esta manera
  @Input() permiso: number;  //saber si tiene permiso de escritura o solo de lectura
  @Input() paciente_id: number; // id del paciente que estamos viendo el formulario
  @Input() questions: CampoBase<any>[] = []; //estos son los atributos que se mandan para que segun el tipo, se carguen en el html de determinda forma
  @Input() metadataForm: { tipoForm: number, idForm: number, estado: number }; //metadata del formulario para mostrar o procesar informacion
  @Input() estadoPaciente_id: number;
  @Input() tipoPaciente_id: number;
  @Input() tabla: any;
  @Output() form: FormGroup; //el formgroup ya creado por formGroupCreador
  hoyfecha = new Date().toISOString().split('.', 1)[0];
  nuevasFilas: Array<any> = [];

  constructor(private fgcs: FormGroupCreadorService, private formServ: FormularioService, private notifServ: NotificacionService, private _router: Router) {
  }

  ngOnChanges() { //se utiliza on changes porque es cuando llegan los @Input es decir, que se asigne el form group cuando esten los Input correspondiente para que funcione
    this.asignarFormGroup();
  }

  onSubmit(salir: boolean) {
    //metodo comun en los formularios de Angular para establecer que hacer al submitiar
    this.postear(salir);
  }

  asignarFormGroup() {
    console.log(this.estadoPaciente_id);

    this.form = this.fgcs.toFormGroup(this.questions, this.permiso, this.estadoPaciente_id)
  }

  postear(salir: boolean) { //metodo que tiene la logica de como postear cada campo, toma el array de questions y por cada elemento, segun el controltype crea un json para ser agregado a un array que contendra todos los json para enviar al servidor
    let posteo: any = { pacienteAtrForm: [] }
    this.questions.forEach(element => {
      if (element.table !== true && element.controlType !== 'title') {
        console.log("elem", element, this.form);


        if (!this.form.controls[element.key].pristine && this.form.controls[element.key].value != null) {
          let jsoncampo: any;

          if (element.controlType === 'dropdown') {

            jsoncampo = {
              formAtr_id: element.key,
              paciente_id: this.paciente_id,
              valoresPosiblesElegidos: [this.form.controls[element.key].value['key']]
            }
            //en la llamada a la api, si no se elige ningun valor, se debe pasar un array vacio
            if (this.form.controls[element.key].value === null) { jsoncampo.valoresPosiblesElegidos = []; }
            posteo.pacienteAtrForm.push(jsoncampo);
          }

          if (element.controlType === 'textbox') {
            jsoncampo = {
              formAtr_id: element.key,
              paciente_id: this.paciente_id,
              valorEscr: this.form.controls[element.key].value
            }
            posteo.pacienteAtrForm.push(jsoncampo);
          }

          if (element.controlType === 'checkbox') {
            jsoncampo = {
              formAtr_id: element.key,
              paciente_id: this.paciente_id,
              valoresPosiblesElegidos: []
            };
            let valores = this.form.controls[element.key].value;
            //como solo obtenemos el valor true y false de los checkbox, se debe obtener las claves y agregar a valoresPosiblesElegidos solamente aquellas que esten en true
            valores.forEach(val => {
              jsoncampo.valoresPosiblesElegidos.push(val.key)
            });

            posteo.pacienteAtrForm.push(jsoncampo);
          }

          if (element.controlType === 'list') {
            if (this.form.controls[element.key].value[element.key + '_' + this.hoyfecha].length > 0) {
              jsoncampo = {
                formAtr_id: element.key,
                paciente_id: this.paciente_id,
                //para la lista solo se debe obtener el valor de el ultimo form control, que es aquel cuyo nombre es  <key de la question> + '_' + <la fecha de hoy>
                valorEscr: this.form.controls[element.key].value[element.key + '_' + this.hoyfecha]
              }
              posteo.pacienteAtrForm.push(jsoncampo);
            }
          }
        }
      }
    })

    if (this.tabla['headers'].length > 0) {
      this.nuevasFilas.forEach(fila => {
        fila.forEach(formcontrol => {
          let jsoncampo: any;
          jsoncampo = {
            formAtr_id: Number(formcontrol.fg),
            paciente_id: this.paciente_id,
            valorEscr: this.form.controls[formcontrol.fg].value[formcontrol.fc]
          }
          posteo.pacienteAtrForm.push(jsoncampo)
        });
      })
    }
    console.log(posteo)
    if (posteo['pacienteAtrForm'].length > 0) {
      this.formServ.postForm(posteo).subscribe((data) => { }
        , (error) => {
          Swal({ title: 'Error al guardar formulario, verifique los campos.', type: 'error', showConfirmButton: true })
          console.log(error)
        }
        , () => { this.cambiaEstado(salir); })
    }
    else {
      Swal({ title: 'No se ha completado o modificado ningun campo.', type: 'warning', showConfirmButton: true })
    }
  }

  cambiaEstado(salir: boolean) {
    // este metodo cambia los estados del formulario y del paciente, si es que corresponde cambiarlos
    // para el caso del formulario, que inicia con estado vacio, puede asignarse el estado "Lleno" o "Parcial"
    // lo que se hace es iterar por cada form control y verificar si es "" o null, si lo es entonces "lleno" es igual a false
    let lleno: boolean;
    let arrayValores = Object.values(this.form.value);
    for (const elem of arrayValores) {
      if (elem == "" || elem == null) {
        lleno = false; break;
      }
      else {
        if (lleno !== false) { lleno = true; }
      }
    }

    // aca segun "lleno" cambia el estado a "Parcial" o "Lleno"
    if (lleno == true) {
      this.formServ.cambiarEstadoForm(this.paciente_id, this.metadataForm.idForm, "Lleno").subscribe((data) => { console.log("estado lleno devuelve", data) },
        (error) => {
          Swal({ title: 'Error al guardar formulario, reinténtelo.', type: 'error', showConfirmButton: true })
          console.log(error)
        })
    }

    if (lleno == false) {
      this.formServ.cambiarEstadoForm(this.paciente_id, this.metadataForm.idForm, "Parcial").subscribe((data) => { console.log("estado parcial devuelve", data) },
        (error) => {
          Swal({ title: 'Error al guardar formulario, reinténtelo.', type: 'error', showConfirmButton: true })
          console.log(error)
        })
    }

    // para el cambio de estado del paciente, primero debemos obtener el estado actual del paciente, y el tipo de paciente,
    // ya que segun el tipo de paciente existen diferentes posibles cambios de estado

    // this.formServ.getEstadoActual(this.paciente_id).subscribe((data:any) =>{
    //   this.estadoPaciente_id = data.estadoHistorial.estadoPaciente.idEstadoPaciente;
    //   tipoPaciente_id = data.estadoHistorial.paciente.tipoPaciente.idTipoPaciente;
    let hayTrigger = false;
    // toma el array de questions y por cada question verifica si es un trigger (tipo de atributo que nos permite cambiar de estado)
    this.questions.forEach(question => {
      if (question.trigger == true) {
        hayTrigger = true;
        // segun el tipo de formulario (diagnostico, evolucion, test, alta)
        switch (this.metadataForm.tipoForm) {
          // 1=diagnostico
          case 1: {

            //si el valor del trigger es "Si" ,y si el tipo de paciente es "Niño", y si su estado actual es "No diagnosticado", cambia el estado a "Ingresado"                 
            if (this.form.controls[question.key].value['key'] == 1 && this.tipoPaciente_id == 1 && this.estadoPaciente_id == 1) {
              this.formServ.cambiarEstadoPac(this.paciente_id, "Ingresar").subscribe((data) => { console.log("se ingresa paciente", data) },
                (error) => {
                  Swal({ title: 'Error al guardar formulario, reinténtelo.', type: 'error', showConfirmButton: true })
                  console.log(error)
                }, () => { this.mostrarExito(salir); this.disableGuardados() }
              );
            } else {
              // si el valor del trigger es "No" ,y si el tipo de paciente es "Niño", y si su estado actual es "No diagnosticado",
              // verifica en los otros dos formularios de diagnostico que el valor sea "No", y si se cumple, cambia el estado a "No ingresado"
              if (this.form.controls[question.key].value['key'] == 2 && this.tipoPaciente_id == 1 && this.estadoPaciente_id == 1) {
                console.log("iria a buscar los otros valores");
                let bool: boolean
                let obs: Observable<any> = this.verificarNoIngreso(this.paciente_id)
                obs.subscribe((data: Array<any>) => {
                  console.log("verificar no ingreso devuelve", data);

                  data.forEach(element => {
                    if (element.formatr.valorpaciente == null) { bool = false; }
                    else {
                      // si el valor es no, evidentemente no ingresa
                      if (element.formatr.valorpaciente.valoresElegidos[0].idValorPosible == 2 && bool != false) {
                        bool = true;
                      }
                    }
                    console.log("deberia pasar a no ingresado = ", bool);

                  });
                  if (bool == true) {
                    this.formServ.cambiarEstadoPac(this.paciente_id, "Rechazar").subscribe((data) => { console.log("se rechaza paciente", data) },
                      (error) => {
                        Swal({ title: 'Error al guardar formulario, reinténtelo.', type: 'error', showConfirmButton: true });
                        console.log(error)
                      })
                  }
                },
                  (error) => {
                    Swal({ title: 'Error al guardar formulario, reinténtelo.', type: 'error', showConfirmButton: true });
                    console.log(error)
                  }, () => { this.mostrarExito(salir); this.disableGuardados() }
                )

              } else {
                // si el valor del trigger es "Si" ,y si el tipo de paciente es "Mujer", y si su estado actual no es "Embarazada",
                if (this.form.controls[question.key].value['key'] == 1 && this.tipoPaciente_id == 2 && this.estadoPaciente_id !== 5) {
                  // si el valor del trigger es "No" ,y si el tipo de paciente es "Mujer", y si su estado actual no es "Embarazada"
                  this.formServ.cambiarEstadoPac(this.paciente_id, "Embarazo").subscribe((data) => { console.log("se pasa a embarazada ", data); },
                    (error) => {
                      Swal({ title: 'Error al guardar formulario, reinténtelo.', type: 'error', showConfirmButton: true });
                      console.log(error)
                    },
                    () => { this.mostrarExito(salir); this.disableGuardados() }
                  );
                }
                else { this.mostrarExito(salir); this.disableGuardados() }
              }
            }
            break;
          }
          // 2=evolucion. evolucion no tiene ningun cambio de estado
          case 2: {
            this.mostrarExito(salir); this.disableGuardados()
            break
          }
          // 3=test. test no tiene ningun cambio de estado
          case 3: {

            this.mostrarExito(salir); this.disableGuardados()
            break;
          }
          // 4=alta
          case 4: {

            if (this.form.controls[question.key].value['key'] == 1 && (this.tipoPaciente_id == 1 || this.tipoPaciente_id == 2) && (this.estadoPaciente_id == 3 || this.estadoPaciente_id == 5)) {
              this.formServ.cambiarEstadoPac(this.paciente_id, "Alta").subscribe((data) => { console.log("se da de alta paciente ", data); },
                (error) => {
                  Swal({ title: 'Error al guardar formulario, reinténtelo.', type: 'error', showConfirmButton: true });
                  console.log(error)
                },
                () => { this.mostrarExito(salir); this.disableGuardados() }
              );
            } else {
              this.mostrarExito(salir); this.disableGuardados()
            }
            break;
          }
          default: {
            console.log("entra en defautl");

          }
        }

      }
    });
    if (hayTrigger == false) {
      this.mostrarExito(salir); this.disableGuardados()
    }

  }

  verificarNoIngreso(idPac): Observable<any> { // f16 es 295       f15 es 384      f05 296
    let bool: boolean;
    let obs
    if (this.metadataForm.idForm == 4) { // f15 hc pediatrica
      obs = forkJoin(
        this.formServ.getValorFormAtrPac(idPac, 296),
        this.formServ.getValorFormAtrPac(idPac, 295))
    }
    if (this.metadataForm.idForm == 7) {// f05 diagnostico nutricional
      obs = forkJoin(
        this.formServ.getValorFormAtrPac(idPac, 384),
        this.formServ.getValorFormAtrPac(idPac, 295))
    }
    if (this.metadataForm.idForm == 9) {// f16 planilla de admsion social
      obs = forkJoin(
        this.formServ.getValorFormAtrPac(idPac, 296),
        this.formServ.getValorFormAtrPac(idPac, 384))
    }
    return obs
  }

  mostrarExito(salir) {

    Swal({ title: 'Formulario guardado.', type: 'success', showConfirmButton: true })
      .then((rdo: SweetAlertResult) => {
        if (rdo.value) this._router.navigate(['home', 'paciente-detalle'], { queryParams: { idPac: this.paciente_id } });
      })

  }

  agregarFila() {
    let listaFormControls: Array<{ fc: string, fg: string }> = []
    this.tabla['headers'].forEach((header) => {
      let nombreFormControl = header.key + '_' + new Date().toISOString().split('.', 1)[0];
      (this.form.controls[header.key] as FormGroup).addControl(nombreFormControl, header.required ? new FormControl('', Validators.required)
        : new FormControl())
      listaFormControls.push({ fc: nombreFormControl, fg: this.splitFormAtr(nombreFormControl) })
    })
    this.nuevasFilas.push(listaFormControls)
    console.log(this.nuevasFilas);
    console.log("fooooorm", this.form)
  }

  splitFormAtr(str: string): string {
    return str.split('_')[0]
  }

  disableGuardados() {
    for (let i = 0; i < this.questions.length; i++) {
      Object.keys(this.form.controls).forEach((fg) => {
        if (this.questions[i].table && this.questions[i].key == fg) {
          this.form.get(fg).disable();
        }
      })
      if (this.questions[i].controlType == 'list') {
        this.form.controls[this.questions[i].key].disable();
        // this.agregarTextarea();
      }
    }
  }

  eliminarUltimaFila() {
    this.nuevasFilas[this.nuevasFilas.length - 1].forEach(elem => {
      (this.form.controls[elem.fg] as FormGroup).removeControl(elem.fc)
    })
    this.nuevasFilas.pop()
  }

  // agregarTextarea(){
  //   for (let i = 0; i < this.questions.length; i++) {
  //     if(this.questions[i].controlType=='list' && this.form.controls[this.questions[i].key].disabled){
  //       let nombreFormControl = this.questions[i].key+'_'+ this.ahora() ;
  //       (this.form.controls[this.questions[i].key] as FormGroup).addControl(nombreFormControl,this.questions[i].required ? new FormControl('',Validators.required) 
  //                                                                                               : new FormControl())
  //     //listaFormControls.push({fc:nombreFormControl,fg:this.splitFormAtr(nombreFormControl)})
  //     }
  //   }
  // }

}

import { Component, OnInit, Input } from '@angular/core';
import { CampoBase }     from '../../campos/campobase';
import { FormGroup } from '@angular/forms';
import { formatDate } from '@angular/common';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule} from 'primeng/dropdown';
import {PanelModule} from 'primeng/panel';
import {InputTextareaModule} from 'primeng/inputtextarea';

@Component({
  selector: 'app-campo-form-dinamico',
  templateUrl: './campo-form-dinamico.component.html',
  styleUrls: [
    './campo-form-dinamico.component.css'
  ],
  providers: []
})
export class CampoFormDinamicoComponent {

  @Input() permiso:number;
  @Input() question: CampoBase<any>;
  @Input() form: FormGroup;
  hoy = new Date().toISOString().split('.',1)[0]
  get isValid() { return this.form.controls[this.question.key].valid; }
  
  loguear(numero:number){
    console.log(this.form.controls[numero]);
    console.log(this.form);
    
  }
}

import { SesionService } from '../../../servicios/sesion.service';
import { Component, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormularioService } from '../../../servicios/formulario.service';
import { CampoBase, CampoTitulo } from './campos/campobase';
import { CampoCheckbox } from "./campos/campocheckbox";
import { CampoTextbox } from './campos/campotextbox';
import { CampoDropdown } from './campos/campodropdown';
import { formatDate } from '@angular/common';
import { CampoLista } from './campos/campolista';
import { PacienteService, Paciente } from '../../../servicios/paciente.service';
import { flatMap } from 'rxjs/operators';



@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})

export class FormularioComponent {
  idParams:any;
  @Output() tipoPermiso:number;
  formulario:{titulo?:string, fechaCreado?:string, fechaMod?:string, questions?: any[], metadataForm:{tipoForm?:number, idForm?:number, estadoForm?:number}, paciente_id?:number, estadoPaciente_id?:number, tipoPaciente_id?:number} = {questions:[],metadataForm:{}}
  promesa;
  loading;
  error;
  tabla:any;
  display:boolean;
  private areaForm: Array<{areaNom: string, areaId: number, forms: Array<{formNom: string, formCod: string, formId: number, formEstado: string}>}>;

  constructor(private _route:ActivatedRoute, private formServ:FormularioService, private _pacSrv: PacienteService, private sesionSrv: SesionService, private router:Router) {
    // override the route reuse strategy
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
    };

    this.loading= true;
    this.route();
    this.obtenerDatosPac(this.idParams.idPac); //este llama a verform
    this.obtenerAreaFormularios();

    //this.verForm(this.idParams.idForm, this.idParams.idPac);    
  }

  ngOnInit(){
    this.display=false;
  }


  route(){

    this._route.queryParams.pipe(flatMap((params)=> {
      // Defaults to 0 if no query param provided.
      this.idParams = {idPac:params.idPac , idForm:params.idForm};
      return this._pacSrv.getPacienteByID(params.idPac);
    })).subscribe((paciente: Paciente)=> {
      this._pacSrv.setPacNavbar(paciente);
    });

  }

  obtenerDatosPac(idPac){
    this.formServ.getEstadoActual(idPac).subscribe((data:any) =>{
      console.log(data);
      
      this.formulario.estadoPaciente_id = data.estadoHistorial.estadoPaciente.idEstadoPaciente;
      this.formulario.tipoPaciente_id = data.estadoHistorial.paciente.tipoPaciente.idTipoPaciente;},()=>{},()=>{this.verForm(this.idParams.idForm, this.idParams.idPac);    
      })

  }

  verForm(idForm,idPac){
    let termino=false;
    //si hay error en esta llamada mostrar mensaje que no tiene los permisos suficientes, seguro tipoPermiso es undefined o lo podemos setear como predef = 0
    this.formServ.getFormularioPermiso(idForm,idPac).subscribe((data:any)=> {
      console.log("data",data);
      console.log("estado",data.formulario.estado.estadoFormulario.idEstadoFormulario);
      
      // asignar permisos de lectura o escritura
      for(const key in data.permisos) {
        if(this.tipoPermiso===2){break;}
        else{this.tipoPermiso = data.permisos[key].tipoPermiso.idTipoPermiso;}
      }  //probarrrr

      //setear metadatos del formulario
      console.log("permiso", this.tipoPermiso);
      
      this.formulario.metadataForm = {tipoForm:data.formulario.tipoFormulario.idTipoFormulario , idForm:data.formulario.idFormulario, estadoForm:data.formulario.estado.estadoFormulario.idEstadoFormulario};  //2=evolucion
      this.formulario.titulo = data.formulario.codFormulario+" : "+ data.formulario.nombreFormulario;
      this.formulario.fechaCreado = "--";
      this.formulario.fechaMod = "--";
      this.formulario.paciente_id = this.idParams.idPac;

      //obtener questions o campos
      let campos:CampoBase<any>[]=[];
      let i=0;
      
      data.formulario.formatrs.forEach(element => {

        if (element.valorpaciente && element.tipoAtributo.nombreTipoAtributo!=="Lista" && element.tipoAtributo.nombreTipoAtributo!=="Tabla"){

          if (this.formulario.fechaMod=="--" || new Date(element.valorpaciente.fecha) > new Date(this.formulario.fechaMod)){
            this.formulario.fechaMod = element.valorpaciente.fecha
          }
          if (this.formulario.fechaCreado=="--" || new Date(element.valorpaciente.fecha) < new Date(this.formulario.fechaCreado) ){
            this.formulario.fechaCreado = element.valorpaciente.fecha
          }
        }

        let atr;
        //si el atributo no es lista, es unico (id=1 creo)
        if(element.tipoAtributo.nombreTipoAtributo=="Único" || element.tipoAtributo.nombreTipoAtributo=="Trigger"){

          let tipoDato;
          switch (element.atributo.tipoDatoAtributo) {
            case "Date":
            case "date":{
              tipoDato = "date";
              break;}

            case "Choice":
            case "choice":{
              tipoDato = "dropdown"
              break;
            }

            case "String":
            case "string":{
              tipoDato = "string";
              break;
            }

            case "Textxl":
            case "TextXL":
            case "TextXl":
            case "textxl":{
              tipoDato = "textxl";
              break;
            }

            case "numberwunit":
            case "Numberwunit":{
              tipoDato = "numberwunit";
              break;
            }

            case "naturalwunit":
            case "Naturalwunit":{
              tipoDato = "naturalwunit";
              break;
            }

            case "naturalnumber":
            case "Naturalnumber":{
              tipoDato = "naturalnumber";
              break;
            }

            case "Number":
            case "number":{
              tipoDato = "number";break;
            }

            case "Checkbox":
            case "checkbox":{
              tipoDato = "checkbox";break;
            }

            default:
              {console.log("error en el tipo Daato Atributo "+element.atributo.tipoDatoAtributo);
                break;}
          }

          if (tipoDato !== "dropdown" && tipoDato !== "checkbox"){

            atr = new CampoTextbox({
              type:tipoDato,
              key:element.idFormAtr,
              id:element.idFormAtr,
              required:element.obligatorio,
              order:element.orden,
              label:element.atributo.nombreAtributoFormulario,
              placeholder:element.descripcionAtributo
            })
            if(element.valorpaciente !== null) {
              atr.value = element.valorpaciente.valorEscr
            }
          }

          if (tipoDato === "dropdown"){
            let valoresPos:{key: string, value: string}[]=[];
            element.valoresposibles.forEach(valpos => {
              valoresPos.push({key:valpos.idValorPosible,value:valpos.nombreValorPosible})
            });

            //traerlos
            atr = new CampoDropdown({
              key:element.idFormAtr,
              id:element.idFormAtr,
              required:element.obligatorio,
              placeholder:element.descripcionAtributo,
              order:element.orden,
              label:element.atributo.nombreAtributoFormulario,
               //le asigno el id para qe se seleccione
              options:valoresPos
            })
            if(element.valorpaciente === null || element.valorpaciente.valoresElegidos.length == 0){
              console.log("el dropdown no tiene valor asignado");
            }
            else{            
              atr.value = {key:element.valorpaciente.valoresElegidos[0].idValorPosible,value:element.valorpaciente.valoresElegidos[0].nombreValorPosible}         
            } //probar algun caso
          }

          if (tipoDato === "checkbox"){
            let valoresPos:{key: string, value: string}[]=[];
            element.valoresposibles.forEach(valpos => {
              valoresPos.push({key:valpos.idValorPosible,value:valpos.nombreValorPosible})
            });
            //traerlos
            atr = new CampoCheckbox({
              key:element.idFormAtr,
              id:element.idFormAtr,
              required:element.obligatorio,
              placeholder:element.descripcionAtributo,
              order:element.orden,
              label:element.atributo.nombreAtributoFormulario,
                //le asigno el id para qe se seleccione
              options:valoresPos,
              value:[]
            })
            if(element.valorpaciente === null || element.valorpaciente.valoresElegidos.length == 0){
              console.log("checkbox sin valores asignados");}
            else{element.valorpaciente.valoresElegidos.forEach(element => {
              atr.value.push(element.idValorPosible);
            });
            } //probar algun caso
            
          }

        }

        if (element.tipoAtributo.nombreTipoAtributo=="Título"){
          atr = new CampoTitulo({
            type:"string",
            key:element.idFormAtr,
            id:element.idFormAtr,
            required:element.obligatorio,
            order:element.orden,
            label:element.atributo.nombreAtributoFormulario,
            placeholder:element.descripcionAtributo
          })
        }

        if(element.tipoAtributo.nombreTipoAtributo=="Lista"){
          
          if (element.valorpaciente){
            element.valorpaciente.forEach(val => {
              if (this.formulario.fechaMod=="--" || new Date(val.fecha) > new Date(this.formulario.fechaMod)){
                this.formulario.fechaMod = val.fecha
              }
              if (this.formulario.fechaCreado=="--" || new Date(val.fecha) < new Date(this.formulario.fechaCreado) ){
                this.formulario.fechaCreado = val.fecha
              }
          });
            
          }
          
          let tipoDato;
          switch (element.atributo.tipoDatoAtributo) {
            case "Date":
            case "date":{
              tipoDato = "date";
              break;}

            case "String":
            case "string":{
              tipoDato = "string";
              break;
            }

            case "Number":
            case "number":{
              tipoDato = "number";break;
            }
          }
        
          atr = new CampoLista({
            type:tipoDato,
            key:element.idFormAtr,
            id:element.idFormAtr,
            required:element.obligatorio,
            order:element.orden,
            label:element.atributo.nombreAtributoFormulario,
            placeholder:element.descripcionAtributo,
            fecha:element.fecha_creado,
            value:[]
          })
          if(element.valorpaciente === null || element.valorpaciente.length == 0){
            console.log("lista sin valores");}
          else{element.valorpaciente.forEach(element => {
            atr.value.push({ fecha:element.fecha , valor:element.valorEscr });
          });
          } //probar algun caso
        }

        if (element.tipoAtributo.nombreTipoAtributo == 'Tabla'){

          if (element.valorpaciente){
            element.valorpaciente.forEach(val => {
              if (this.formulario.fechaMod=="--" || new Date(val.fecha) > new Date(this.formulario.fechaMod)){
                this.formulario.fechaMod = val.fecha
              }
              if (this.formulario.fechaCreado=="--" || new Date(val.fecha) < new Date(this.formulario.fechaCreado) ){
                this.formulario.fechaCreado = val.fecha
              }
          });
            
          }

          let tipoDato;
          switch (element.atributo.tipoDatoAtributo) {
            case "Date":
            case "date":{
              tipoDato = "date";
              break;}

            case "String":
            case "string":{
              tipoDato = "string";
              break;
            }

            case "Number":
            case "number":{
              tipoDato = "number";break;
            }

            default:
              {console.log("error en el tipo Daato Atributo "+element.atributo.tipoDatoAtributo);
                break;}
          }

          atr = new CampoLista({
            type:tipoDato,
            key:element.idFormAtr,
            id:element.idFormAtr,
            required:element.obligatorio,
            order:element.orden,
            label:element.atributo.nombreAtributoFormulario,
            placeholder:element.descripcionAtributo,
            fecha:element.fecha_creado,
            value:[],
            table:true
          })

          if(element.valorpaciente == null || element.valorpaciente.length == 0) {
            console.log("tabla sin valores");
          } else {
            element.valorpaciente.forEach(elem => {
              atr.value.push({ fecha:elem.fecha , valor:elem.valorEscr , key:elem.formAtr.idFormAtr});
            });
          }
        
        }

        if(atr){
        if(element.tipoAtributo.nombreTipoAtributo=='Trigger'){atr.trigger = true;}
        else{atr.trigger = false;}
        }
        
        if(atr){campos.push(atr);console.log("se pushea atr con key = "+atr.key+" - es trigger: "+atr.trigger);}

        i++;
      })
      
      let headers:Array<{key:string,label:string,required:boolean,placeholder:string}>=[]
      let filas:Array<Array<{valor:string, fecha:string, key:string }>> = []

      for (let i = 0; i < campos.length; i++) {
        if (campos[i].table == true){

          headers.push({key:campos[i].key, label: campos[i].label, required:campos[i].required, placeholder:campos[i].placeholder})
          
          campos[i].value.forEach(val => {

              if (i==0){             
                let fila:Array<{valor:string,fecha:string,key:string}>=[]
                fila.push(val)
                filas.push(fila)
                console.log("creo la primera fila con campo "+ campos[i].label+" y con valor "+val.valor);
              }
              else {
                filas.forEach(fil => {
                  fil.forEach(el=>{
                    if ( ((new Date(val.fecha).getTime()-new Date(el.fecha).getTime()) < 200) && ((new Date(val.fecha).getTime()-new Date(el.fecha).getTime()) > 0) &&(!fil.includes(val))){
                      console.log(val.valor, el.valor, (new Date(val.fecha).getTime()-new Date(el.fecha).getTime()))
                      fil.push(val);
                    }
                  });
                })            
              }

            });

            }
            
        }
        this.tabla={headers,filas}
        console.log(this.tabla);       
      
      

      campos.sort((a, b) => {
        if (a.order > b.order) {
            return 1;
        }
    
        if (a.order < b.order){
            return -1;
        }
    
        return 0;
      });
     
      this.formulario.questions = campos;
      console.log(this.formulario.questions);

      termino =true;
      console.log("termino",termino);
      // this.loading=false;
      
      },(error)=>{this.error = true;this.loading=false;console.log(error)},()=>{this.loading=false;});
      return termino;
  }
  
  fechaLinda(date){
    let linda
    if (date == "--") {
      linda = "--"
    } else {      
      linda = formatDate(date,'dd/MM/yyyy',"en-US");
    }
    return linda;
  }

  obtenerAreaFormularios(){

    this.areaForm= [];

    this.formServ.getFormsPacProf(this.idParams['idPac']).subscribe((data:any)=> {
      let promesa: Promise<void>= new Promise((resolve) =>{
        resolve(data);
      })

      promesa.then((data:any) => {
        console.log("sidebar",data);
        
        data.forEach(element => {

          let index;
          let area;
          if ((element.formularios.length !== 0)){ //si no esta ya en el array o no es vacio
            area= {areaNom:element.nombreArea , areaId:element.idArea, forms:[]}
            this.areaForm.push(area);
            index= this.areaForm.findIndex((are)=>are.areaId == element.idArea);
          }

          element.formularios.forEach(formu => {

            let form= {
              formId:formu.idFormulario,
              formCod:formu.codFormulario,
              formNom:formu.nombreFormulario,
              formEstado:formu.estado.estadoFormulario.nombreEstadoFormulario,
              formPermiso:0
            }
           
            for(const key in formu.permisos) {
              if(form.formPermiso===2){break;}
              else{form.formPermiso = formu.permisos[key].tipoPermiso.idTipoPermiso;}
            }
            
            this.areaForm[index].forms.push(form);
            });
          });
      }, (error)=> {
        console.log(error);
        this.error= true;
        this.loading= false;
      })

    });
  }
  verFormulario(formulario: number, paciente: number){
    this.router.navigate(['home','formulario'], {queryParams: { idForm:formulario, idPac:paciente} });
  }

  irTitulo(num){
    let x = document.querySelector('#titulo'+num);
    if (x){
      x.scrollIntoView({block: "start", behavior: "auto"});
      setTimeout(() => {
        window.scrollBy({top:-90,behavior:"auto"})
      },50);
    }
    this.display=false;
  }

  botonUp(){
    window.scroll({top:0,behavior:"smooth"})
  }


}

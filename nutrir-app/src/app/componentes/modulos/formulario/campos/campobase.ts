export class CampoBase<T> {
    value: T; //valor del atributo
    key: string; //clave o nombre del atributo
    label: string; //algo que agregarle al nombre del atributo
    required: boolean; //si es obligatorio o no
    order: number; //el orden en que va a aparecer
    controlType: string; //tipo de campo: dropdown o textbox
    id:number; //id de FormAtr
    placeholder:string;
    trigger:boolean;
    table:boolean;
   
    constructor(options: {
        value?: T,
        key?: string,
        label?: string,
        required?: boolean,
        order?: number,
        controlType?: string,
        id?:number,
        placeholder?:string,
        trigger?:boolean,
        table?:boolean
      } = {}) {
      this.value = options.value;
      this.key = options.key || '';
      this.label = options.label || '';
      this.required = !!options.required;
      this.order = options.order === undefined ? 1 : options.order;
      this.controlType = options.controlType || '';
      this.id = options.id || 0;
      this.placeholder = options.placeholder || '';
      this.trigger = options.trigger || false;
      this.table = options.table || false;
    }
  }

  export class CampoTitulo extends CampoBase<string> {
    controlType = 'title'

    constructor(options: {} = {}) {
      super(options);
    }
  }

  export class CampoTabla extends CampoBase<Array<{fecha:string, valor:any}>>{
    controlType = 'table';
    type: string;

    constructor(options: {} = {}) {
      super(options);
      this.type = options['type'] || '';
    }
  }
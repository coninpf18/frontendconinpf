import { CampoBase } from './campobase';

export class CampoTextbox extends CampoBase<string> {
  controlType = 'textbox';
  type: string;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
  }
}
import { CampoBase } from './campobase';

export class CampoCheckbox extends CampoBase<string> {
  controlType = 'checkbox';
  options: {key: string, value: string}[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
  }
}
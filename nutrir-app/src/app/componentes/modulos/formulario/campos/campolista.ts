import { CampoBase } from './campobase';

export class CampoLista extends CampoBase<Array<{fecha:string, valor:any}>> { //{fecha:string, valor:any}
  controlType = 'list';
  fecha?: string;

  constructor(options: {} = {}) {
    super(options);
    this.fecha = options['fecha'] || ''; 
}
}
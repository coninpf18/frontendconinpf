import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, ValidatorFn, ValidationErrors } from '@angular/forms';
import { CampoBase, CampoTabla } from './campobase';
import { Observable } from 'rxjs';
import { CampoCheckbox } from './campocheckbox';
import { element } from '@angular/core/src/render3/instructions';
import { CampoLista } from './campolista';
import { formatDate } from '@angular/common';
import { CampoTextbox } from './campotextbox';

@Injectable({
  providedIn: 'root'
})
export class FormGroupCreadorService {

  hoy = new Date().toISOString().split('.',1)[0]
  constructor() { }

  toFormGroup(questions: CampoBase<any>[],permiso:number,estadoPac:number):FormGroup{
    let group: any = {};

    questions.forEach(question => {
      console.log(estadoPac);
      
      if (permiso === 1 || estadoPac == 4){ //si tiene solo permisos de lectura o esta dado de alta        
        if (question.table!==true){
          if(question.controlType==='textbox'){
            let campotext = <CampoTextbox>question

            if(campotext.type == 'naturalnumber' || campotext.type =='naturalwunit'){
              group[question.key] = new FormControl({value: question.value || '', disabled:true});              
            }
            else{
              group[question.key] = new FormControl({value: question.value || '', disabled:true});
            }
          }            
          
          if(question.controlType==='dropdown'){
            // let valor = {key:question.value['key'],value:question.value['value']}
            group[question.key] = new FormControl({value: question.value || '', disabled:true });}

          if(question.controlType==='checkbox'){
            // let arrayOpts:any = {};
            // let questionCheck = <CampoCheckbox>question
            // questionCheck.options.forEach(opt => {
            //   if (question.value.includes(opt.key)){                
            //     arrayOpts[question.key+'_'+opt.key] = new FormControl({value:true,disabled:true});}
            //     else{arrayOpts[question.key+'_'+opt.key] = new FormControl({disabled:true});
            //     }
            // });
            // group[question.key] = question.required ? new FormGroup(arrayOpts)
            //                                         : new FormGroup(arrayOpts);}
            
              let arrayOpts:any[] = [];
              let questionCheck = <CampoCheckbox>question
              if (questionCheck.options != undefined) {            
                questionCheck.options.forEach(opt => {
                if (question.value.includes(opt.key)){                
                  arrayOpts.push({key:opt.key, value:opt.value})}
                group[question.key] = new FormControl({value:arrayOpts,disabled:true})
                })
            }
            else {
              group[question.key] = new FormControl({value:null,disabled:true})
            }
            
          }

          if(question.controlType==='list'){
            let lista:any = {};
            let questionList = <CampoLista>question;
            questionList.value.forEach(valor => {
              lista[question.key+'_'+valor.fecha]= new FormControl({value:valor.valor || '',disabled:true});
            });
            
            lista[question.key+'_'+this.hoy]= new FormControl({value:'',disabled:true});
            
            group[question.key] = question.required ? new FormGroup(lista)
                                                    : new FormGroup(lista);
          }
        }
        else { //es tabla
          if(question.controlType==='list'){
            let lista:any = {};
            let questionList = <CampoLista>question;
            questionList.value.forEach(valor => {
              let idVal = question.key +'_'+ valor.fecha
              lista[idVal]= new FormControl({value:valor.valor || '',disabled:true})
            });          
            group[question.key] = new FormGroup(lista);
                                                    
          }
        }
      }
      if (permiso===2 && estadoPac != 4){ //si tiene permisos de edicion
        if (question.table!==true){
          if(question.controlType==='textbox'){
            let campotext = <CampoTextbox>question

            if(campotext.type == 'naturalnumber' || campotext.type =='naturalwunit'){
              group[question.key] = question.required ? new FormControl(question.value || '', [Validators.required,Validators.min(0)])
                                                    : new FormControl(question.value || '',Validators.min(0));              
            }
            else{
              group[question.key] = question.required ? new FormControl( question.value || '', Validators.required)
                                                    : new FormControl( question.value || '');
            }
          }            
          
          if(question.controlType==='dropdown'){
              group[question.key] = question.required ? new FormControl(question.value, Validators.required)
                                                      : new FormControl(question.value);}

          if(question.controlType==='checkbox'){
              // let arrayOpts:any = {};
              // let questionCheck = <CampoCheckbox>question
              // questionCheck.options.forEach(opt => {
              //   if (question.value.includes(opt.key)){                
              //   arrayOpts[question.key+'_'+opt.key] = new FormControl(true);}
              //   else{arrayOpts[question.key+'_'+opt.key] = new FormControl();
              //   }
              // });
              // group[question.key] = question.required ? new FormGroup(arrayOpts,atLeastOne(Validators.required))
              //                                         : new FormGroup(arrayOpts);}
              let arrayOpts:any[] = [];
              let questionCheck = <CampoCheckbox>question
              
              if (questionCheck.options != undefined) {
                questionCheck.options.forEach(opt => {
                  if (question.value.includes(opt.key)){                
                    arrayOpts.push({key:opt.key, value:opt.value})}
                  group[question.key] = question.required ? new FormControl({value:arrayOpts,disabled:false}, Validators.required)
                                                          : new FormControl({value:arrayOpts,disabled:false})
                  })
              }
              else {
                group[question.key] = question.required ? new FormControl({value:null,disabled:false}, Validators.required)
                                                          : new FormControl({value:null,disabled:false})
              }
              
            }

          if(question.controlType==='list'){
            let lista:any = {};
            let questionList = <CampoLista>question;
            questionList.value.forEach(valor => {
              lista[question.key+'_'+valor.fecha]= new FormControl({value:valor.valor || '',disabled:true});
            });

            lista[question.key+'_'+this.hoy]= new FormControl();
            
            group[question.key] = question.required ? new FormGroup(lista)
                                                    : new FormGroup(lista);
          }
        }
        else { //si es tabla
          if(question.controlType==='list'){
            let lista:any = {};
            let questionList = <CampoLista>question;
            questionList.value.forEach(valor => {
              let idVal = question.key +'_'+ valor.fecha
              lista[idVal]= question.required ? new FormControl({value:valor.valor || '',disabled:true}, Validators.required)
                                                                     : new FormControl({value:valor.valor || '',disabled:true})
            });
            group[question.key] = question.required ? new FormGroup(lista)
                                                    : new FormGroup(lista);
          }
        }
                
      } 
    });
    console.log(group);
    
    return new FormGroup(group)};
        
  }

  export const atLeastOne = (validator: ValidatorFn) => ( group: FormGroup): ValidationErrors | null => {
    const hasAtLeastOne = group && group.controls && Object.keys(group.controls)
      .some(k => !validator(group.controls[k]) && group.controls[k].value == true);
  
    return hasAtLeastOne ? null : {
      atLeastOne: true,
    };
  };


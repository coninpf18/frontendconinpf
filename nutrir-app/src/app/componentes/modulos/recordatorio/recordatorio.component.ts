import { Component, OnInit, OnDestroy } from '@angular/core';
import { RecordatorioService, Recordatorio } from '../../../servicios/recordatorio.service';
import { PacienteService, Paciente } from '../../../servicios/paciente.service';
import { Router } from '@angular/router'
import { NgForm } from '@angular/forms'
import { formatDate } from '@angular/common';
import { NotificacionService } from '../../../servicios/notificacion.service';
import { SesionService } from '../../../servicios/sesion.service';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2';

declare let $: any;

@Component({
  selector: 'app-recordatorio',
  templateUrl: './recordatorio.component.html',
  styles: [
    `.redClass { color: red }`
  ]
})

export class RecordatorioComponent implements OnInit, OnDestroy{

  private recordatorios: Recordatorio[];
  private pacientes: Paciente[];
  private loading: boolean;
  private archivado: boolean= true;
  private fechaHoy: any = new Date();
  private unsubscribe: Subscription= new Subscription();
  private pacienteselec: Paciente;
  private fechaFiltro: string;
  public config = {fullScreenBackdrop: 'true' };
  constructor(private _router: Router, private _recordatorioService: RecordatorioService, private _sesionSrv: SesionService,
              private _pacienteService: PacienteService, private _notifSrv: NotificacionService) {
    
                _sesionSrv.setNombreModulo('Recordatorios');
                _sesionSrv.getNombreModulo(); 
                _pacienteService.setPacNavbar(null);
  }

  ngOnInit(){
    this.loading= true;

    let tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    this.fechaFiltro= tomorrow.toISOString().slice(0,10);

    this.buscarPacienteTotal();
    this.fechaHoy= formatDate(this.fechaHoy,'yyyy-MM-dd',"en-US");
    this.obtenerRecordatorios();

  }

  ngOnDestroy(){
    if(this.unsubscribe){
      this.unsubscribe.unsubscribe();
    }
  }

  buscarPaciente(termino: String){
    this.pacientes= [];
    let unsubscribe= this._pacienteService.getDetallePacienteDos(termino).subscribe((data: Paciente[]) => {
          this.pacientes= data;
          this.unsubscribe.add(unsubscribe)
    }, ()=> this.unsubscribe.add(unsubscribe));
  }

  limpiar() {
    setTimeout('document.recordatorio.reset()', 2000);
  }

  buscarPacienteTotal(){
    this.pacientes= [];
    let unsubscribe= this._pacienteService.getPacienteIngresado().subscribe((data: Paciente[])=> {
          this.pacientes= data;
          this.pacientes.forEach(element => {
            element.nombreCompleto = element.nombrePaciente + ' ' + element.apellidoPaciente;
          });
          this.unsubscribe.add(unsubscribe)
    }, ()=> this.unsubscribe.add(unsubscribe));
  }

  archivar(rec: Recordatorio){
    rec.archivado= true;
    let fechaRecordatorio: string= formatDate(rec.fecha,'yyyy-MM-dd',"en-US");
    fechaRecordatorio= fechaRecordatorio.concat(" 00:00:00" ); //se aplica formato a la fecha para que se muestre como type=date
    let modificarRecordatorio= {
      descripcion:rec.descripcion,
      fecha:fechaRecordatorio,
      archivado:rec.archivado
    };
    let unsubscribe= this._recordatorioService.putRecordatorio(modificarRecordatorio, rec.idRecordatorio).subscribe(()=>{
      this.unsubscribe.add(unsubscribe)
    }, ()=> this.unsubscribe.add(unsubscribe));
  }

  obtenerRecordatorios(){
    this.unsubscribe= this._recordatorioService.getRecordatorio().subscribe((data: Recordatorio[])=> {
      new Promise((resolve)=> {resolve(data)}).then((data:any)=>{
        this.recordatorios= [];
        data.forEach(element=> {
          element.fecha= formatDate(element.fecha,'yyyy-MM-dd',"en-US");
          this.recordatorios.push(element);
        });
        //Ordenar el array de recordatorios por la fecha.
        this.recordatorios.sort(function(a: Recordatorio, b: Recordatorio){
          return (new Date(a.fecha).getTime())-(new Date(b.fecha).getTime());
        });
      })
      this.loading= false
    
    }, error=> {
      this.loading=false;
      console.log("Error obteniendo Recordatorios ",error)
    });
  }

  guardarRecordatorio(form: NgForm){
    
    let rec: Recordatorio= {
      descripcion: '',
      fecha: '',
      paciente_id: null,
      archivado: false
    };

    if(form.valid=== true){
      let fechaHoy= formatDate(new Date(),'yyyy-MM-dd',"en-US");
      let fechaRec: string= '';
      if (form.value.fecha <= fechaHoy){
          return Swal({title: 'Ingrese una fecha posterior a la actual.', type: 'warning', showConfirmButton: true})
      }
        fechaRec= formatDate(form.value.fecha,'yyyy-MM-dd',"en-US"); //se aplica formato a la fecha para que se muestre como type=date
        fechaRec= fechaRec.concat(" 00:00:00.000000");
        rec.descripcion= form.value.descripcion;
        rec.fecha= fechaRec;
        rec.paciente_id= this.pacienteselec.idPaciente;

        let unsubscribe= this._recordatorioService.postRecordatorio(rec).subscribe(()=> {
            Swal({title: 'Recordatorio guardado.', type: 'success', showConfirmButton: true})
            this.unsubscribe.add(unsubscribe)
            this.obtenerRecordatorios();
          }, error=> {
            Swal({title: 'Error al guardar recordatorio.', type: 'error', showConfirmButton: true})
            this.unsubscribe.add(unsubscribe)
            console.log(error);
          })
          this.limpiar();
    }
  }

  irPaciente(idPaciente: number){
    this._router.navigate(['home','paciente-detalle'], {queryParams: {idPac: idPaciente}});
  }
}

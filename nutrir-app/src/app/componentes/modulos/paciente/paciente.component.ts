import { SesionService } from '../../../servicios/sesion.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { PacienteService, Paciente} from '../../../servicios/paciente.service';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';

 @Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html'
})

export class PacienteComponent implements OnInit, OnDestroy {

  private pacientes: Paciente[];
  private idPaciente: number;
  private loading: boolean;
  private error: boolean;
  private sub: Subscription= new Subscription();
  private pageActual: number= 1;
  private mostrarPaginacion: boolean;

  constructor(private pacienteService: PacienteService, private router: Router, 
              private route: ActivatedRoute, private sesionSrv: SesionService) {
    
            this.loading= true;
            this.pacientes= [];
            this.sesionSrv.setNombreModulo('Paciente');
            this.sesionSrv.getNombreModulo();
            pacienteService.setPacNavbar(null);
                  
  }

  ngOnInit(){
    
    this.loading= false;
    this.mostrarPaginacion= false;
  }

  ngOnDestroy(){
    if(this.sub){ this.sub.unsubscribe(); }
  }

  buscarPacienteTotal(){ //Busca todos los pacientes
    this.pacientes= [];
    this.sub= this.pacienteService.getPaciente().subscribe((data: Paciente[]) => {
          this.pacientes = data;
          this.mostrarPaginacion= true;
    });

  }

  verPaciente(paciente: Paciente){
    this.router.navigate(['home','paciente-detalle'], {queryParams: { idPac: paciente.idPaciente}});
  }

  buscarPaciente(evento: NgForm){ //Busca los pacientes que cumplan con "termino"

    let termino: string= evento.value.paciente;
    console.log(termino);

    this.error= false;
    this.pacientes= [];
    let replaced= termino.split(' ').join('-');
    
    if(replaced.length>= 1){
      let unsubscribe= this.pacienteService.getDetallePacienteDos(replaced).subscribe((data: Paciente[])=> {
        this.pacientes= data;
        this.error= false;
        this.mostrarPaginacion= true;
        this.sub.add(unsubscribe)
      }, (error)=> {
        this.error= true;
        this.sub.add(unsubscribe)
        console.log("error: ", error)
      });
    } else{
      this.mostrarPaginacion= false;
    }
  }

  
}

import { SesionService } from '../../../../servicios/sesion.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { PacienteService, Paciente, Relacion } from '../../../../servicios/paciente.service';
import { FormularioService, AreaForm} from '../../../../servicios/formulario.service';
import { Domicilio, DomicilioService } from '../../../../servicios/domicilio.service';
import { Subscription } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { FamiliaService, Familia } from '../../../../servicios/familia.service';
import { DiaService, Dia } from '../../../../servicios/dia.service';
import { AsistenciaService } from '../../../../servicios/asistencia.service';
import Swal from 'sweetalert2';

 @Component({
  selector: 'app-pacientedetalle',
  templateUrl: './pacientedetalle.component.html',
  styleUrls: ['./pacientedetalle.component.css']
})
export class PacienteDetalleComponent implements OnInit, OnDestroy {

  private idParam: number;
  private paciente: { datos: Paciente, estado: string };
  private areaForm: Array<{areaNom: string, areaId: number, forms: Array<{formNom: string, formCod: string, formId: number, formEstado: string}>}>;
  private areaFormu: AreaForm;
  private loading: boolean;
  private error: boolean;
  private unsubscribe: Subscription= new Subscription();
  private familia: Familia;
  private diaAsistenciaPaciente: Dia[];
  private relaciones: Relacion[];
  private unsubscribeX: Subscription= new Subscription();
  private edad:string="";
  private domicilio:string="";
  constructor(private sesionSrv: SesionService, private router:Router, private _route:ActivatedRoute, private _pacienteService: PacienteService,
              private _formularioService: FormularioService, private _fliaSrv: FamiliaService, private _diaSrv: DiaService, private _asistSrv: AsistenciaService, private _domicilio:DomicilioService) {
        this.sesionSrv.setNombreModulo('Paciente');
        this.sesionSrv.getNombreModulo();

        this.loading= true;
        this.paciente= {
          datos:{
            nombrePaciente:'',
            apellidoPaciente:'',
            dniPaciente:null,
            fechaNacimiento: null,
            nroHistoriaClinica:'',
            telefonoPaciente:'',
            tipoPaciente: {
              idTipoPaciente:null,
              nombreTipoPaciente:''
            },
            pais: null, centro: null, sexo: null, grupoFamilia: null},
          estado:''}

        this.areaFormu= {area:'', formularios:[]}
        
  }

  ngOnInit(){
    
    
    this._route.queryParams.subscribe(params=> {
      this.idParam= +params['idPac'];

      this.mostrarPaciente(this.idParam);
      this.buscarRelaciones(this.idParam);
    });


  }

  ngOnDestroy(){
    this.unsubscribe.unsubscribe();
  }

  buscarRelaciones(idPac: number){
    let unsubscribe=
    this._pacienteService.getRelacionesPaciente(idPac).subscribe((data:any) => {
        new Promise((resolve)=> {resolve(data)}).then((data:any)=>{
          this.relaciones = [];
          this.unsubscribeX.add(unsubscribe);
          data.contenido.forEach(relacion => {
            this.relaciones.push(relacion);
          })
        })
    }, error=> {
      this.unsubscribeX.add(unsubscribe);
      console.log("Error obteniendo las relaciones ",error)
    });
  }
  obtenerDomicilio(){
    this._domicilio.getDomicilioPaciente(this.paciente.datos.idPaciente).subscribe((data:Domicilio) => {
      console.log(data);
      this.domicilio=data.calle+" "+data.nroDomicilio
      if (data.departamento) {
        this.domicilio=this.domicilio+" - Dpto "+data.departamento        
      }
      if (data.piso) {
        this.domicilio=this.domicilio+" - Piso "+data.piso        
      }
      setTimeout(() => {
        this.domicilio=this.domicilio+" - "+data.localidad.nombreLocalidad
      }, 100);
    })
  }
  mostrarPaciente(id: number){

    this.diaAsistenciaPaciente= []

    this._pacienteService.getDetallePaciente(id).pipe(flatMap((data: any)=> {
      this.paciente.datos= data.paciente;
      this.paciente.estado= data.estadoPaciente.nombreEstadoPaciente;
      
      return this._fliaSrv.getFlia(data.paciente.grupoFamilia);
    })).subscribe((data: any)=> {
      new Promise((resolve)=> {
        resolve(data.grupofamilia);
      }).then((datos: Familia)=> {
        this.edad=this._pacienteService.calcularEdad(this.paciente.datos.fechaNacimiento)
        this.obtenerDomicilio();
        this.familia= datos;
        
        this.obtenerAreaFormularios();
      })
    }, (error)=> {
      this.error= true;
      this.loading= false;
      console.log(error);
    });

    this.obtenerDiaPaciente();
  }

  obtenerDiaPaciente(){
    this._diaSrv.getDiaPaciente(this.idParam).subscribe((datos: Dia[])=> {
      this.diaAsistenciaPaciente = [];
      for(let i=0; i<datos.length; i++){
        //if(datos[i].pacienteViene){
          this.diaAsistenciaPaciente.push(datos[i]);
        //}
      }
    });
  }

  obtenerAreaFormularios(){

    this.areaForm= [];

    let unsubscribe= this._formularioService.getFormsPacProf(this.idParam).subscribe((data:any)=> {
      let promesa: Promise<void>= new Promise((resolve) =>{
        resolve(data);
      })

      promesa.then((data:any) => {

        data.forEach(element => {

          let index;
          let area;
          if ((element.formularios.length !== 0)){ //si no esta ya en el array o no es vacio
            area= {areaNom:element.nombreArea , areaId:element.idArea, forms:[]}
            this.areaForm.push(area);
            index= this.areaForm.findIndex((are)=>are.areaId == element.idArea);
          }

          element.formularios.forEach(formu => {

            let form= {
              formId:formu.idFormulario,
              formCod:formu.codFormulario,
              formNom:formu.nombreFormulario,
              formEstado:formu.estado.estadoFormulario.nombreEstadoFormulario,
              formPermiso:0
            }
           
            for(const key in formu.permisos) {
              if(form.formPermiso===2){break;}
              else{form.formPermiso = formu.permisos[key].tipoPermiso.idTipoPermiso;}
            }
            
            this.areaForm[index].forms.push(form);
            });
          });
        this.loading= false
      }, (error)=> {
        console.log(error);
        this.error= true;
        this.loading= false;
      })

      this.unsubscribe.add(unsubscribe);
    });
  }

  verFormulario(formulario: number, paciente: number){
    this.router.navigate(['home','formulario'], {queryParams: { idForm:formulario, idPac:paciente} });
  }

  irGrupoFamiliar(){
    this.router.navigate(['home','verFlia'], {queryParams: {idFam: this.familia.idGrupoFamilia}});
  }

  irPaciente(idPaciente: number){
    this.loading = true;
    this.router.navigate(['home','paciente-detalle'], {queryParams: {idPac: idPaciente}});
  }

  cambioDiaVisita(evento: Dia){

    if(this.diaAsistenciaPaciente.length=== 0){
      this.diaAsistenciaPaciente.push(evento)      
    } else if(evento.pacienteViene){
      new Promise((resolve)=>{
        this.diaAsistenciaPaciente.push(evento);
        resolve();
      }).then(()=>{
        this.diaAsistenciaPaciente.sort(function (a: Dia, b: Dia) {
          return a.idDia - b.idDia;
        });
      })
    } else{
      for(let i=0; i< this.diaAsistenciaPaciente.length; i++){
        if(this.diaAsistenciaPaciente[i].idDia=== evento.idDia){
          this.diaAsistenciaPaciente.splice(i,1);
        }
      }
    }
  }

  agregarDiaVisitaPrompt(dia: Dia){
    Swal({
      title: '¿Agregar el día '+ dia.nombreDia +' a los días de visita?',
      //text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      //confirmButtonColor: '#3085d6',
      //cancelButtonColor: '#d33',
      confirmButtonText: 'Confirmar.',
      cancelButtonText: 'Cancelar.',
    }).then((result) => {
      if (result.value) {
        this.agregarDiaVisita(dia.idDia);
      }
    })
  }

  eliminarDiaVisitaPrompt(dia: Dia){
    Swal({
      title: '¿Eliminar el día '+ dia.nombreDia +' de los días de visita?',
      //text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      //confirmButtonColor: '#3085d6',
      //cancelButtonColor: '#d33',
      confirmButtonText: 'Confirmar.',
      cancelButtonText: 'Cancelar.',
    }).then((result) => {
      if (result.value) {
        this.eliminarDiaVisita(dia.idDia);
      }
    })
  }

  agregarDiaVisita(idDia: number){
    let unsubscribe= this._asistSrv.postGenerarAsistente(this.idParam, idDia).subscribe(()=> {
        this.unsubscribe.add(unsubscribe);
        Swal({title: 'Día agregado.', type: 'success', showConfirmButton: true});
        this.obtenerDiaPaciente();
      }, ()=> this.unsubscribe.add(unsubscribe));
  }

  eliminarDiaVisita(idDia: number){
    let unsubscribe= this._asistSrv.eliminarDiaAsistencia(this.idParam, idDia).subscribe(()=> {
        this.unsubscribe.add(unsubscribe)
        Swal({title: 'Día eliminado.', type: 'success', showConfirmButton: true});
        this.obtenerDiaPaciente();
      }, ()=> this.unsubscribe.add(unsubscribe));
  }
}


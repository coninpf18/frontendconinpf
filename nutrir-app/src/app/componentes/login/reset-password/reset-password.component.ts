import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BackService } from 'src/app/servicios/back.service';
import { NotificacionService } from 'src/app/servicios/notificacion.service';
import Swal from 'sweetalert2'
declare let $: any;

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})

export class ResetPasswordComponent implements OnInit {
  private formPassword: FormGroup
  public loadingLogin = false;
  token:any=""
  alertError:boolean=false
  password1; 
  password2;
  constructor(private rutaActiva: ActivatedRoute, private _backSrv:BackService, private _notifSrv: NotificacionService, private router:Router) {
    this.formPassword = new FormGroup({
      password1: new FormControl('', [ Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ \s]*')]),
      password2: new FormControl('', [ Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-ZñÑáéíóúÁÉÍÓÚ \s]*')]),
    })
  } 

  ngOnInit() {
    this.token=this.rutaActiva.snapshot.params.token
    console.log(this.token);
    
  }
  renovarcontrasena(){
    // console.log(this.token, this.password1);
    this.loadingLogin=true
    this._backSrv.cambioDeContraseña(this.token, this.password1).subscribe((data)=>{
      this.loadingLogin=false
      // $('#modalConfirmar').modal('show');     
      // Tu contraseña se ha renovado exitosamente. Vuelve a iniciar sesión para ingresar al sistema
      this.alertError=true
      setTimeout(()=>{
        Swal({title: 'Su contraseña se ha renovado exitosamente.',type: 'success', showConfirmButton: true}) }, 900);       
      setTimeout(()=>{
        this.router.navigate(['login'])
      },4000)  
    }, error => {
      console.log("error", error.status);
      if (error.status == 404) {
        this.loadingLogin=false
        this.alertError=true
        setTimeout(()=>{
          Swal({title: 'El tiempo de cambio de contraseña, ha expirado. Reescriba su correo electrónico.', type: 'warning', showConfirmButton: true}); 
        },2000)       
      }else{
        setTimeout(()=>{
          Swal({title: 'Ha ocurrido un error de red, vuelva a intentarlo.', type: 'warning', showConfirmButton: true});
        },2000) 
      }
      
    })
  }
  confirmar(){
    this.router.navigate(['/login']);
  }
  
}

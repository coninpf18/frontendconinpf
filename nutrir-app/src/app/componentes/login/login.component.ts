import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BackService } from '../../servicios/back.service';
import { SesionService } from '../../servicios/sesion.service';
import { NotificacionService } from '../../servicios/notificacion.service';
import Swal from 'sweetalert2'

declare let $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {
  // @ViewChild('closeBtn') closeBtn: ElementRef;
  public loadingLogin = false;
  public mensaje = false;
  private smsErrorLogueo: string;
  private mostrarSmsError: boolean;
  private usuario: string = "";
  private password: string = "djangotest1.";

  constructor(private _router: Router, private _backServ: BackService, private sesionServ: SesionService,
    private _notifSrv: NotificacionService) {

    this.mostrarSmsError = false;
  }

  validarDatosIngresados() {

    //Transformo todos los datos a string.
    let us1 = this.usuario.toString();
    let pass1 = this.password.toString();

    if (us1.length === 0 || pass1.length === 0) {
      this.mostrarSmsError = true;
      this.smsErrorLogueo = "*Complete ambos casilleros.";
    } else {
      this.loadingLogin = true
      if (us1 == "administrador"){
        this.loadingLogin = false
        this.mostrarSmsError = true;
        this.smsErrorLogueo = "*Usuario es administrador del sistema.";
        return;
      }
      //Llamo al servicio para conectarme al servidor en backService. Asigno datos a variables del front.
      let usuario = { username: us1, password: pass1 }
      this._backServ.getUser(usuario).subscribe((datos: any) => {

        this.loadingLogin = false;
        let tkn: string = datos.token;
        this.sesionServ.setToken(tkn);
        this.sesionServ.setProfesional(datos.profesional);

        //setea session storage
        sessionStorage.setItem('token', tkn);
        sessionStorage.setItem('profesional', (JSON.stringify(datos.profesional)));
        this._router.navigate(['/home']);

      }, (error) => {
        console.log(error);
        if (error.status === 400 || error.status === 401) {
          this.smsErrorLogueo = "*Usuario o contraseña incorrecta.";
        } else {
          if (error.status === 403) {
            this.smsErrorLogueo = "*Usuario se encuentra inactivo.";
          }
        }

        this.loadingLogin = false
        this.mostrarSmsError = true;
        
      });
    }
  }

  reestablecerContrasenia(email: string) {
    this.loadingLogin = true;

    this._backServ.cambiarContraseña(email)
      .subscribe((data) => {

        this.loadingLogin = false;
        console.log(data);

        $('#reestablecerContraseñaModal').modal('hide');
        setTimeout(() => { Swal({ title: 'Revise su correo para cambiar la contraseña.', type: 'success', showConfirmButton: true }) }, 900);
      },
        (error) => {
          this.loadingLogin = false;
          if (error.status === 404) {
            Swal({ title: 'Correo electrónico no registrado.', type: 'warning', showConfirmButton: true });
          } else {
            Swal({ title: 'Error de conexión. Reintente luego.', type: 'warning', showConfirmButton: true });
          }
          console.log(error)
        });
  }
  confirmar() {
    $('#reestablecerContraseñaModal').modal('hide');
    $('#modalConfirmar').modal('hide');

  }

}

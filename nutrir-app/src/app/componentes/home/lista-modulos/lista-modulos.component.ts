import { Component, OnInit, OnDestroy } from '@angular/core';
import { PacienteService } from '../../../servicios/paciente.service';
import { SesionService } from '../../../servicios/sesion.service';
import { DiaService } from '../../../servicios/dia.service';


@Component({
  selector: 'app-lista-modulos',
  templateUrl: './lista-modulos.component.html'
})

export class ListaModulosComponent implements OnInit, OnDestroy {
  
  private loading: boolean;
  private saludo: string;
  private hoyEs: string;

  constructor(private _paciente: PacienteService, private _sesionSrv: SesionService, private diaService: DiaService) { 
    this._sesionSrv.setNombreModulo('Home');
    this._sesionSrv.getNombreModulo();
  }

  ngOnInit() {

    // this._sesionSrv.mostrarModuloNavbar= false;   //No muestra los módulos en el navbar cuando está en el home (lista-modulos).
    this.loading= true;
    this._paciente.setPacNavbar(null);
    setTimeout(() => { this.loading= false }, 200);
    var hoy = new Date();
    this.saludo = `${ this.getSaludo(hoy.getHours()) }, ${this._sesionSrv.getProfesional().nombreProfesional}.`;
    this.hoyEs = `Hoy es ${ this.getDiaPalabras(hoy.getDay()).toLowerCase() }, ${ hoy.getDate() } de ${ this.getMesPalabras(hoy.getMonth()).toLowerCase() }.`;
  }

  ngOnDestroy(){
    // this._sesionSrv.mostrarModuloNavbar= true;  //Con esto se muestran los módulos en el navbar, cuando ya sale del home (lista-modulos).
    
  }


  getSaludo(hora) {

    if (hora < 13) {
      return "Buenos días";
    } else if (hora < 20) {
      return "Buenas tardes";
    } else {
      return "Buenas noches";
    }
  }

  getDiaPalabras(diaSemana): string {
    return this.diaService.diaPalabras(diaSemana);
  }

  getMesPalabras(mesAño): string {
    switch(mesAño){
      case(mesAño = 0): { return "Enero";}
      case(mesAño = 1): { return "Febrero";}
      case(mesAño = 2): { return "Marzo";}
      case(mesAño = 3): { return "Abril";}
      case(mesAño = 4): { return "Mayo";}
      case(mesAño = 5): { return "Junio";}
      case(mesAño = 6): { return "Julio";}
      case(mesAño = 7): { return "Agosto";}
      case(mesAño = 8): { return "Septiembre";}
      case(mesAño = 9): { return "Octubre";}
      case(mesAño = 10): { return "Noviembre";}
      default: return "Diciembre";
    }
  }

}

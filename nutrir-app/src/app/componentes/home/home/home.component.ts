import { Component, OnInit } from '@angular/core';
import { SesionService } from 'src/app/servicios/sesion.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  private modulos: string[]

  constructor( private sesionServ: SesionService) { 
   sesionServ.nombreModulo = "Home";
  }

  ngOnInit() {
    this.modulos = this.traerModulos();
  }

  traerModulos(){
    return this.sesionServ.getModulos()
  }
}

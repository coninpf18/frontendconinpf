import { Component, OnInit } from '@angular/core';
import { AsistenciaService } from 'src/app/servicios/asistencia.service';
import { DiaService, Dia } from 'src/app/servicios/dia.service';
import { PacienteService, PacienteSemana } from 'src/app/servicios/paciente.service';
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { SesionService } from 'src/app/servicios/sesion.service';

@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.component.html',
  styleUrls: ['./calendario.component.css']
})
export class CalendarioComponent implements OnInit {

 
  private pacientesSemana: PacienteSemana [] = [];
  private dias: Dia [] = [];
  private hoyEs;
  private hoyMayuscula: string;
  private fechaSemana: any [] = [];
  private fechas: fechasSemana [] = [];
  private loading: boolean;
  private rol:any=""

  constructor(private asistenciaService: AsistenciaService,
              private diaService: DiaService,
              private pacienteService: PacienteService,
              private _router: Router,
              private sesionServ: SesionService) {
                this.rol=this.sesionServ.getProfesional().user.username
               }

  ngOnInit() {
    console.log(this.rol);
    
    this.getDias()
    this.getPacientesSemana()
    var hoy = new Date();
    this.hoyEs = this.getDiaPalabras(hoy.getDay()).toString() 
    this.hoyMayuscula = this.mayusculaPrimera(this.hoyEs)
  };

  mayusculaPrimera(palabra: string) {
    return palabra.charAt(0).toUpperCase() + palabra.slice(1);
  }

  getPacientesSemana(){
    this.loading=true;
    this.pacienteService.getPacienteSemana().subscribe(pacientes => {
      for(let i = 0; i < pacientes.length; i++){
        this.pacientesSemana.push(pacientes[i])
        this.fechaSemana.push((pacientes[i].fecha) = formatDate(pacientes[i].fecha, 'dd/MM', 'en-US'))
      }
      this.loading=false;
    })
  }

  getDiaPalabras(diaSemana): string {
    return this.diaService.diaPalabras(diaSemana);
  }

  irPaciente(idPaciente: number){
    this._router.navigate(['home','paciente-detalle'], {queryParams: {idPac: idPaciente}});
  }

  getDias(){
    this.diaService.getDia().subscribe(datos => {
      for(let i=0; i < datos.length; i++){
        this.dias.push(datos[i]);
      }
    })
  }

}

export interface fechasSemana{
  fecha: Date;
}

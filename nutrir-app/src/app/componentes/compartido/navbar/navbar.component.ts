import { Component, OnInit, Input, OnChanges, ViewChild, ElementRef, AfterViewInit, Renderer2 } from '@angular/core';
import { SesionService } from '../../../servicios/sesion.service';
import { PacienteService } from '../../../servicios/paciente.service';
import { Profesional } from '../../../servicios/profesional.service';
import { Router } from '@angular/router';
import { RecordatorioService, Recordatorio } from '../../../servicios/recordatorio.service';
import { Subject } from 'rxjs';
import { BackService } from '../../../servicios/back.service';
import Swal, { SweetAlertResult } from 'sweetalert2';

declare let $: any;

//.toISOString().slice(0,10) date format(YYYY-MM-DD).

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit, AfterViewInit, OnChanges {

  @Input() listaModulos;
  @Input() modActual;

  @ViewChild("divPaciente") divPac: ElementRef;
  @ViewChild("divFamilia") divFam: ElementRef;
  @ViewChild("divRecordatorio") divRecord: ElementRef;
  @ViewChild("divAsistencia") divAsist: ElementRef;
  @ViewChild("divEstadistica") divEstad: ElementRef;

  private profesionalNV: Profesional;
  private nombreUsuario: String;
  private recordatorios: Recordatorio[];
  private mostrarNotifRojo: boolean;
  private promesaBloquearBoton: Promise<{}>;
  private onchanges = new Subject<void>();

  constructor(private sesionServ: SesionService, private _paciente: PacienteService,
    private router: Router, private _recordSrv: RecordatorioService, private _sesionSrv: SesionService,
    private renderer: Renderer2, private _backSrv: BackService) {

    if (this.nombreUsuario == undefined) { this.profesionalNV = this.sesionServ.getProfesional(); }

    this.recordatorios = []
    this.mostrarNotifRojo = true;

  }

  ngOnChanges() {

    this.onchanges.next();
  }

  ngOnInit() {

    this.traerRecordatorio();
    this.promesaBloquearBoton.
      then((sizeArray: number) => {
        $(document).ready(function () {
          if (sizeArray === 0) {
            $("#botonNotif").prop("disabled", true);
          }
        })
      });
  }

  ngAfterViewInit() {

    this.onchanges.subscribe(() => {
      this.desmarcarModulo();
      this.marcarModulo();
    });

    this.desmarcarModulo();
    this.marcarModulo();
  }

  desmarcarModulo() {

    //Uso renderer para que también adapte el estilo cuando se usa el server. Da estilo de forma dinámica, divNombreModulo es para el efecto de cada div del modulo (que contiene cada botón).

    if (this.listaModulos.includes('Paciente')) {
      this.renderer.setStyle(this.divPac.nativeElement, 'background-color', 'dodgerblue');
      this.renderer.setStyle(this.divPac.nativeElement, 'color', 'white');
      this.renderer.setStyle(this.divPac.nativeElement, 'box-shadow', 'inset 0 0px dodgerblue');
    }

    if (this.listaModulos.includes('Familia')) {
      this.renderer.setStyle(this.divFam.nativeElement, 'background-color', 'dodgerblue');
      this.renderer.setStyle(this.divFam.nativeElement, 'color', 'white');
      this.renderer.setStyle(this.divFam.nativeElement, 'box-shadow', 'inset 0 0px dodgerblue');
    }

    if (this.listaModulos.includes('Asistencia')) {
      this.renderer.setStyle(this.divAsist.nativeElement, 'background-color', 'dodgerblue');
      this.renderer.setStyle(this.divAsist.nativeElement, 'color', 'white');
      this.renderer.setStyle(this.divAsist.nativeElement, 'box-shadow', 'inset 0 0px dodgerblue');
    }

    if (this.listaModulos.includes('Recordatorios')) {
      this.renderer.setStyle(this.divRecord.nativeElement, 'background-color', 'dodgerblue');
      this.renderer.setStyle(this.divRecord.nativeElement, 'color', 'white');
      this.renderer.setStyle(this.divRecord.nativeElement, 'box-shadow', 'inset 0 0px dodgerblue');
    }

    if (this.listaModulos.includes('Estadistica')) {
      this.renderer.setStyle(this.divEstad.nativeElement, 'background-color', 'dodgerblue');
      this.renderer.setStyle(this.divEstad.nativeElement, 'color', 'white');
      this.renderer.setStyle(this.divEstad.nativeElement, 'box-shadow', 'inset 0 0px dodgerblue');
    }

  }

  marcarModulo() {

    // Cada cambio en modulos (+/-), se debe actualizar acá en los cases y desmarcarModulo() para ver el efecto negro en la UI del frontEnd.

    switch (sessionStorage.getItem('ubicacion')) {
      case "Paciente": {
        this.renderer.setStyle(this.divPac.nativeElement, 'border-color', 'rgb(112, 174, 255)');
        this.renderer.setStyle(this.divPac.nativeElement, 'box-shadow', 'inset 0 -4px white');
        this.renderer.setStyle(this.divPac.nativeElement, 'background-color', 'rgb(112, 174, 255)');
        break;
      }
      case "Recordatorios": {
        this.renderer.setStyle(this.divRecord.nativeElement, 'box-shadow', 'inset 0 -4px white');
        this.renderer.setStyle(this.divRecord.nativeElement, 'border-color', 'rgb(112, 174, 255)');
        this.renderer.setStyle(this.divRecord.nativeElement, 'background-color', 'rgb(112, 174, 255)');
        break;
      }
      case "Asistencia": {
        this.renderer.setStyle(this.divAsist.nativeElement, 'box-shadow', 'inset 0 -4px white');
        this.renderer.setStyle(this.divAsist.nativeElement, 'border-color', 'rgb(112, 174, 255)');
        this.renderer.setStyle(this.divAsist.nativeElement, 'background-color', 'rgb(112, 174, 255)');
        break;
      }
      case "Estadistica": {
        this.renderer.setStyle(this.divEstad.nativeElement, 'box-shadow', 'inset 0 -4px white');
        this.renderer.setStyle(this.divEstad.nativeElement, 'border-color', 'rgb(112, 174, 255)');
        this.renderer.setStyle(this.divEstad.nativeElement, 'background-color', 'rgb(112, 174, 255)');
        break;
      }
      case "Familia": {
        this.renderer.setStyle(this.divFam.nativeElement, 'box-shadow', 'inset 0 -4px white');
        this.renderer.setStyle(this.divFam.nativeElement, 'border-color', 'rgb(112, 174, 255)');
        this.renderer.setStyle(this.divFam.nativeElement, 'background-color', 'rgb(112, 174, 255)');
        break;
      }
      default: {
        break;
      }
    }
  }

  cerrarSesion() {
    this._sesionSrv.logout();
  }

  irPacDetalle() {
    this.router.navigate(['home', 'paciente-detalle'], { queryParams: { idPac: this._paciente.getPacNavbar().idPaciente } });
  }

  traerRecordatorio() {

    this.promesaBloquearBoton = new Promise((resolve) => {
      this._recordSrv.getDetalleRecordatorio(new Date().toISOString().slice(0, 10)).subscribe((datos: Recordatorio[]) => {
        for (let i = 0; i < datos.length; i++) {
          if (datos[i].visto === false) {
            this.recordatorios.push(datos[i]);
          }
        }
        resolve(this.recordatorios.length);
      }, () => resolve(0));
    })
  }

  controlNotif() {
    //Controla localmente la visión de las notificaciones como alerta.
    this.mostrarNotifRojo = false;
  }

  irRecordatorio() {
    //Controla con el servidor la visión de las notificaciones como alerta.
    new Promise((resolve) => {
      for (let i = 0; i < this.recordatorios.length; i++) {
        this.recordatorios[i].visto = true;
        this._recordSrv.putRecordatorio(this.recordatorios[i], this.recordatorios[i].idRecordatorio).subscribe(() => {
          console.log('put hecho.');
        }, error => console.log('error put recordatorio visto', error));
      }
      resolve();
    }).then(() => {
      this.router.navigate(['home', 'recordatorio'])
    })
  }

  irHome() {
    this._sesionSrv.setNombreModulo('Home');
    this._sesionSrv.getNombreModulo();
    this.router.navigate(['home', 'lista_modulos']);
  }

  confirmarContrasena(passwordActual: string, passwordNueva: string) {

    this._backSrv.cambiarContraseñaVieja(passwordActual, passwordNueva)
      .subscribe(() => {

        $('#cambiarContrasena').modal('hide');

        Swal({ title: 'Contraseña modificada, ingrese nuevamente.', type: 'success', showConfirmButton: true })
          .then((resultado: SweetAlertResult) => {
            if (resultado.value) this._sesionSrv.logout();
          });

      }, error => {
        if (error.status === 400) {
          Swal({ title: 'Contraseña actual incorrecta, reinténtelo.', type: 'error', showConfirmButton: true })
        } else {
          Swal({ title: 'Error de conexión. Reintente luego.', type: 'error', showConfirmButton: true })
        }
      })
  }

}
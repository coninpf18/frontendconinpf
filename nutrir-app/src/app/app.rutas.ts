// ARCHIVO PARA ALMACENAR TODAS LAS RUTAS DE LA SPA.
import { RouterModule, Routes, ExtraOptions} from '@angular/router';

import { LoginComponent } from './componentes/login/login.component';
import { ListaModulosComponent } from './componentes/home/lista-modulos/lista-modulos.component';
import { AsistenciaComponent } from './componentes/modulos/asistencia/asistencia.component';
import { RecordatorioComponent } from './componentes/modulos/recordatorio/recordatorio.component';
import { HomeComponent } from './componentes/home/home/home.component';
import { DiaComponent } from './componentes/modulos/dia/dia.component';
import { NuevoIntegranteComponent } from './componentes/modulos/familia/integrante/nuevoIntegrante/nuevoIntegrante.component';
import { ProfesionalComponent } from './componentes/modulos/profesional/ver/profesional.component';
import { ModProfesionalComponent } from './componentes/modulos/profesional/modificar/modificar.component';
import { AsistenciaFechaComponent } from './componentes/modulos/asistencia/asistencia-fecha/asistencia-fecha.component';
// import { AsignarDiasComponent } from './componentes/modulos/asistencia/asignar-dias/asignar-dias.component';
import { AsistenciaAreaComponent } from './componentes/modulos/asistencia/asistencia-area/asistencia-area.component';
import { AsistenciaAreaFechaComponent } from './componentes/modulos/asistencia/asistencia-area-fecha/asistencia-area-fecha.component';
import { PacienteComponent } from './componentes/modulos/paciente/paciente.component';
import { PacienteDetalleComponent } from './componentes/modulos/paciente/pacientedetalle/pacientedetalle.component';
import { FormularioComponent } from './componentes/modulos/formulario/formulario.component';
import { FamiliaComponent } from './componentes/modulos/familia/familia.component';
import { VerFliaComponent } from './componentes/modulos/familia/verflia/verflia.component';
import { ModificarDiasComponent } from './componentes/modulos/asistencia/modificar-dias/modificar-dias.component';
import { BusquedaFamiliaComponent } from './componentes/modulos/familia/busquedafamilia/busquedafamilia.component';
import { NuevafliaComponent } from './componentes/modulos/familia/nuevaflia/nuevaflia.component';
import { ModificarIntegranteComponent } from './componentes/modulos/familia/integrante/modificarIntegrante/modificar-integrante.component';
import { EstadisticaComponent } from './componentes/modulos/estadistica/estadistica.component';
import { HomeEstaticoComponent } from './componentes/home/home-estatico/home-estatico.component';
import { ResetPasswordComponent } from './componentes/login/reset-password/reset-password.component';
import { EditarRelFamComponent } from './componentes/modulos/familia/verflia/editar-rel-fam/editar-rel-fam.component';
import { EstadisticaPdfComponent } from './componentes/modulos/estadistica/estadistica-pdf/estadistica-pdf.component';

const APP_ROUTES: Routes= [
    {path: 'login', component: LoginComponent },
    {path: 'cambiarpass/:token', component: ResetPasswordComponent },
    {path: 'home', component: HomeComponent,
        children:[
            {path: 'lista_modulo', component: ListaModulosComponent },
            {path: 'paciente', component: PacienteComponent },
            {path: 'homeEstatico', component: HomeEstaticoComponent},
            {path: 'listaModulo', component: ListaModulosComponent },
            {path: 'asistencia', component: AsistenciaComponent },
            {path: 'asistencia-fecha', component: AsistenciaFechaComponent },
            // {path: 'asignar-dia', component: AsignarDiasComponent },
            {path: 'modificar-dia', component: ModificarDiasComponent },
            {path: 'asistencia-area', component: AsistenciaAreaComponent },
            {path: 'asistencia-area-fecha', component: AsistenciaAreaFechaComponent },
            {path: 'recordatorio', component: RecordatorioComponent },
            {path: 'paciente', component: PacienteComponent },
            {path: 'paciente-detalle', component: PacienteDetalleComponent },
            {path: 'dia', component: DiaComponent },
            {path: 'dia/:id', component: DiaComponent },
            {path: 'verProfesional', component: ProfesionalComponent },
            {path: 'modProfesional', component: ModProfesionalComponent}, 
            {path: 'verFlia', component: VerFliaComponent},
            {path: 'editarRelFam', component: EditarRelFamComponent},       //editar relaciones familiares.
            {path: 'integrante', component: NuevoIntegranteComponent},
            {path: 'mod-integrante', component: ModificarIntegranteComponent},
            {path: 'familia', component: FamiliaComponent,
                children:[
                    {path: 'familia-busqueda', component: BusquedaFamiliaComponent},
                    {path: 'familia-nueva', component: NuevafliaComponent},
                    {path: '**', pathMatch: 'full', redirectTo: 'familia-busqueda'}
                ]
            },

            {path: 'formulario', component: FormularioComponent },
            {path: 'estadisticas', component: EstadisticaComponent, 
                children:[
                    {path: 'exportarestadisticas', component: EstadisticaPdfComponent} 
                ]
            },
            {path: 'exportarestadisticas', component: EstadisticaPdfComponent},
            {path: '', pathMatch: 'full', redirectTo: 'lista_modulo'},
            {path: '**', pathMatch: 'full', redirectTo: 'lista_modulo'}
        ]
    },

    // otherwise redirect to login
    {path: '', pathMatch: 'full', redirectTo: 'login'}, 
    {path: '**', pathMatch: 'full', redirectTo: 'login'},   
];

const routerOptions: ExtraOptions= {
    useHash: false
  };

export const APP_ROUTING= RouterModule.forRoot(APP_ROUTES , routerOptions);

 // Componentes nativos de angular.
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxPaginationModule } from 'ngx-pagination';
import { APP_ROUTING } from './app.rutas';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

registerLocaleData(localeEs);
// import {MultiSelectModule} from 'primeng/multiselect';


// Componentes.
import { AppComponent } from './app.component';
import { NavbarComponent } from './componentes/compartido/navbar/navbar.component';
import { ListaModulosComponent } from './componentes/home/lista-modulos/lista-modulos.component';
import { RecordatorioComponent } from './componentes/modulos/recordatorio/recordatorio.component';
import { AsistenciaComponent } from './componentes/modulos/asistencia/asistencia.component';
import { LoginComponent } from './componentes/login/login.component';
import { HomeComponent } from './componentes/home/home/home.component';
import { EstadisticaComponent } from './componentes/modulos/estadistica/estadistica.component';
import { DiaComponent } from './componentes/modulos/dia/dia.component';
import { NuevafliaComponent } from './componentes/modulos/familia/nuevaflia/nuevaflia.component';
import { NuevoIntegranteComponent } from './componentes/modulos/familia/integrante/nuevoIntegrante/nuevoIntegrante.component';
import { ProfesionalComponent } from './componentes/modulos/profesional/ver/profesional.component';
import { ModProfesionalComponent } from './componentes/modulos/profesional/modificar/modificar.component';
import { PacienteComponent } from './componentes/modulos/paciente/paciente.component';
import { PacienteDetalleComponent } from './componentes/modulos/paciente/pacientedetalle/pacientedetalle.component';
import { FormularioComponent } from './componentes/modulos/formulario/formulario.component';
import { FamiliaComponent } from './componentes/modulos/familia/familia.component';
import { AsistenciaFechaComponent } from './componentes/modulos/asistencia/asistencia-fecha/asistencia-fecha.component';
// import { AsignarDiasComponent } from './componentes/modulos/asistencia/asignar-dias/asignar-dias.component';
import { ModificarDiasComponent } from './componentes/modulos/asistencia/modificar-dias/modificar-dias.component';
import { LoadingComponent } from './componentes/compartido/loading/loading.component';
import { VerFliaComponent } from './componentes/modulos/familia/verflia/verflia.component';
import { BusquedaFamiliaComponent } from './componentes/modulos/familia/busquedafamilia/busquedafamilia.component';
import { FormDinamicoComponent } from './componentes/modulos/formulario/form-dinamico/form-dinamico.component';
import { CampoFormDinamicoComponent } from './componentes/modulos/formulario/form-dinamico/campo-form-dinamico/campo-form-dinamico.component';
// import { CampoBase } from './componentes/modulos/formulario/campos/campobase';
import { ModificarIntegranteComponent } from './componentes/modulos/familia/integrante/modificarIntegrante/modificar-integrante.component';
import { AsistenciaAreaComponent } from './componentes/modulos/asistencia/asistencia-area/asistencia-area.component';
import { AsistenciaAreaFechaComponent } from './componentes/modulos/asistencia/asistencia-area-fecha/asistencia-area-fecha.component';
import { HomeEstaticoComponent } from './componentes/home/home-estatico/home-estatico.component';
import { DropdownModule } from 'primeng/dropdown';
import { ResetPasswordComponent } from './componentes/login/reset-password/reset-password.component';
import { CheckPasswordDirective } from './componentes/login/reset-password/check-password.directive';
import { MultiSelectModule } from 'primeng/multiselect';
import {CalendarModule} from 'primeng/calendar';
import { PanelModule } from 'primeng/panel';
import {ListboxModule} from 'primeng/listbox';
import {ChartModule} from 'primeng/chart';
import {ToastModule, Toast} from 'primeng/toast';
import { CalendarioComponent } from './componentes/home/calendario/calendario.component';
import { SidebarModule } from 'primeng/sidebar';
import { EditarRelFamComponent } from './componentes/modulos/familia/verflia/editar-rel-fam/editar-rel-fam.component';
import { AsistenciaHistoricoComponent } from './componentes/modulos/asistencia/asistencia-historico/asistencia-historico.component';
//import { ExportAsModule } from 'ngx-export-as';
import { EstadisticaPdfComponent } from './componentes/modulos/estadistica/estadistica-pdf/estadistica-pdf.component';
// import { ResetPasswordComponent } from './componentes/login/Reset Passsword/reset-password/reset-password.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ListaModulosComponent,
    RecordatorioComponent,
    AsistenciaComponent,
    LoginComponent,
    HomeComponent,
    EstadisticaComponent,
    DiaComponent,
    NuevafliaComponent,
    NuevoIntegranteComponent,
    ProfesionalComponent,
    ModProfesionalComponent,
    PacienteComponent,
    PacienteDetalleComponent,
    FormularioComponent,
    FamiliaComponent,
    LoadingComponent,
    AsistenciaFechaComponent,
    // AsignarDiasComponent,
    ModificarDiasComponent,
    VerFliaComponent,
    BusquedaFamiliaComponent,
    FormDinamicoComponent,
    CampoFormDinamicoComponent,
    ModificarIntegranteComponent,
    AsistenciaAreaComponent,
    AsistenciaAreaFechaComponent,
    HomeEstaticoComponent,
    ResetPasswordComponent,
    CheckPasswordDirective,
    CalendarioComponent,
    EditarRelFamComponent,
    AsistenciaHistoricoComponent,
    EstadisticaPdfComponent,
    ],

  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    APP_ROUTING,
    FormsModule,
    HttpModule,
    BrowserModule,
    FormsModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    CommonModule,
    NgxLoadingModule,
    DropdownModule,
    MultiSelectModule,
    CalendarModule,
    PanelModule,
    ListboxModule,
    ChartModule,
    ToastModule,
    SidebarModule,
    NgxPaginationModule,
  ],

  providers: [{provide: LOCALE_ID, useValue: 'es-ES'}],

  bootstrap: [AppComponent],

  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})

export class AppModule { }

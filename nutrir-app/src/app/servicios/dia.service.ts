import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SesionService } from './sesion.service';
import { map} from 'rxjs/operators'
import { environment } from '../../environments/environment';
//import { constructor } from 'events';

@Injectable({
  providedIn: "root"
})

export class DiaService {
  cabecera = new HttpHeaders({'Authorization':'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json'});

  constructor(private _http:HttpClient, private sesionServ: SesionService) {}

  getDia(){
    return this._http.get(environment.linkUrl + '/dia/',{headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })}).pipe(map(datos=>{
      return datos['contenido'];
    }));
  }

  postMarcarDiaAsistir(idPaciente: number, idDia: number) {
        let cuerpo = {
          "diaPaciente":  {
              "paciente_id": idPaciente,
              "dia_id": idDia
          }
    }
    let cuerpoJson = JSON.stringify(cuerpo);
    return this._http.post(environment.linkUrl + '/diapaciente/', cuerpoJson, {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })});
  }

  getDiaPaciente(idPaciente: number) {
    return this._http.get(environment.linkUrl + '/dia/pacienteviene/' + idPaciente + '/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })}).pipe(map(datos=>{
      return datos['contenido'];
    }));
  }

  diaPalabras(diaSemana): string {
    switch (diaSemana) {
      case diaSemana = 0: {
        return 'domingo';
      }
      case diaSemana = 1: {
        return 'lunes';
      }
      case diaSemana = 2: {
        return 'martes';
      }
      case diaSemana = 3: {
        return 'miércoles';
      }
      case diaSemana = 4: {
        return 'jueves';
      }
      case diaSemana = 5: {
        return 'viernes';
      }
      default: {
        return 'sábado';
      }
   }
  }
}

export interface Dia {
  idDia: number,
  nombreDia: string,
  fecha: string, 
  pacienteViene?: boolean,
  fecha_creado?: string,
  fecha_modificado?: string
}

import { Injectable } from '@angular/core';

declare var $: any;

@Injectable({
  providedIn: "root"
})

export class NotificacionService {

    constructor() {}

    //Muestra el alert dinamicamente.
    showalert(message: string, alertType: string, timeout?: number) { //con tiempo le establecemos un tiempo especifico, sino se pasa como parametro es de 2 seg
        $('#alertdiv').remove();
        let tiempo;
        let imagenUrl;
        switch (alertType) {
            case 'success':
                imagenUrl= "/src/assets/iconos/checked.png"
                break;
            case 'danger':
                imagenUrl= "/src/assets/iconos/cancel.png"
                break;
            case 'warning':
                imagenUrl= "/src/assets/iconos/warning.png"
                break;  
            case 'info':
                imagenUrl= "/src/assets/iconos/question.png"
                break;
        }

        if(timeout){
            tiempo= timeout;
        } else{
            tiempo= 2500;
        }

        $('#alert_mensaje').append(
            '<a id="alertdiv" class="alert alert-' +  alertType + ' col-auto text-center animated fadeIn fade" data-dismiss="alert">'+
            '<img src=" '+ imagenUrl +' " width:30 height:30 style="margin-right:1vw"><span>' + message + ' </span></a>');

        setTimeout(function() { // entra a la pagina
            $('#alertdiv').addClass('on');
           }, 0);

        setTimeout(function() { // sale por donde enttro
            $('#alertdiv').removeClass('on');
            }, tiempo+500);
    }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SesionService } from './sesion.service';
import { map } from 'rxjs/operators'
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: "root"
})

export class RecordatorioService {

  private cabeceraT= new HttpHeaders({'Authorization':'Token ' + this.sesionServ.getToken()});
  private cabecera= new HttpHeaders({'Authorization':'Token '+this.sesionServ.getToken(),'Content-Type':'application/json'});

  constructor(private _http: HttpClient, private sesionServ: SesionService) {
  }

  getRecordatorio(){
    return this._http.get(environment.linkUrl + '/recordatorios/',{headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })}).
          pipe(map(data => {
            return data['contenido'];

          }))
  }

  getDetalleRecordatorio(idRecordatorio: any){
    return this._http.get(environment.linkUrl + '/recordatorios/' + idRecordatorio + '/',{headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })}).
          pipe(map(data => {
            return data['contenido'];

          }))
  }

  postRecordatorio(rec:Recordatorio){
    let cuerpoJson = JSON.stringify(rec)
    let recor="{\"recordatorio\"\:" + cuerpoJson + "}";
      return this._http.post(environment.linkUrl + '/recordatorios/', recor, {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
  }

  putRecordatorio(rec: Recordatorio, idRecordatorio){
    let cuerpoJson = JSON.stringify(rec)
    let recor="{\"recordatorio\"\:" + cuerpoJson + "}";
    return this._http.put(environment.linkUrl + '/recordatorios/'+idRecordatorio+'/', recor, {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
  }
}

export interface Recordatorio{
  idRecordatorio?: number
  descripcion: string;
  fecha: string;
  paciente_id?: number;
  archivado?: boolean;
  visto?: boolean
  paciente?: {
      apellidoPaciente: string;
      asistencia: boolean;
      asistencia_area: boolean;
      centro: number;
      dniPaciente: number;
      fechaNacimiento: string;
      grupoFamilia: number;
      idPaciente: number;
      nombrePaciente: string;
      nroHistoriaClinica: string;
      pais: number;
      sexo: number;
      telefonoPaciente: string;
  }
  tipoPaciente?: {
      idTipoPaciente: number;
      nombreTipoPaciente: string;
  }
}

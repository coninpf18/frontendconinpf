import { Injectable } from '@angular/core';
import { SesionService } from './sesion.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DomicilioService {
  cabecera= new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' });
  private paisidProf: number;

  constructor (private sesionServ: SesionService, private _http:HttpClient) {

  }

  //ARRANCAN LOS GET DE DOMICILIO

  //Domicilio de familia y profesional.
  getDomicilioFamilia(idFam: number){

    return this._http.get(environment.linkUrl+'/domicilio/familiares/' + idFam + '/', {headers:new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })});
  }

  getDomicilioPaciente(id: number){

    return this._http.get(environment.linkUrl+'/domicilio/paciente/'+id+'/', {headers:new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
            .pipe(map(datos=>{
              return datos['domicilio'];
            }));
  }

  getDomicilioProfesionalid(idProf: number){

    return this._http.get(environment.linkUrl+'/domicilio/'+idProf+'/', {headers:new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
          .pipe(map(datos=> {
              return datos['domicilio'];
          }));
  }
//----------------------------------------------------------------------------------------------------------------------
//GET PROVINCIA, PAIS Y LOCALIDAD.

  //Provincia
  getProvincia(){

    return this._http.get(environment.linkUrl+'/provincia/', {headers:new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
          .pipe(map(datos=> {
            return datos['contenido'];
      }));
  }

  getProvinciaId(idProv: number){

    return this._http.get(environment.linkUrl + '/provincia/' + idProv+'/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
          .pipe(map(datos=> {
            return datos['contenido'];
    }));
  }

  getProvinciaIdPais(idPais: number){

    return this._http.get(environment.linkUrl + '/provincia/pais/' + idPais +'/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
          .pipe(map(datos=> {
            return datos['contenido'];
    }));
  }
//-------------------------------------------------------------------------------------------------------------
//Pais

  getPais(){

    return this._http.get(environment.linkUrl+ '/pais/', {headers:new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
          .pipe(map(datos=> {
              let algo: Pais[]= datos['contenido'];
              return datos['contenido'];
    }));
  }

  getPaisId(idPais: number){

    return this._http.get(environment.linkUrl+ '/pais/' + idPais+'/', {headers:new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
          .pipe(map(datos=> {
              this.paisidProf= datos['pais'].idPais;
              return datos['pais'];
    }));
  }

//--------------------------------------------------------------------------------------------------------------
//Localidad

  getLocalidad(){

    return this._http.get(environment.linkUrl+'/localidad/', {headers:new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
          .pipe(map(datos=> {
              return datos['contenido'];
    }));
  }

  getLocalidadId(idLoc: number){

    return this._http.get(environment.linkUrl + '/localidad/' + idLoc+'/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
        .pipe(map(datos=> {
          return datos['contenido'];
    }));
  }

  getLocalidadIdProv(idProv: number){

    return this._http.get(environment.linkUrl + '/localidad/provincia/' + idProv+'/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
        .pipe(map(datos=> {
            return datos['contenido'];
    }));
  }

//------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------
//ARRANCAN LOS POST

  putDomicilio(dom: Domicilio){

    let cabecera= new HttpHeaders({'Authorization':'Token '+ this.sesionServ.getToken(), 'Content-Type':'application/json'});
    let cuerpo= this.cuerpoConsultaDom(dom);

    return this._http.put(environment.linkUrl + '/domicilio/' + dom.idDomicilio + '/',
           JSON.stringify(cuerpo), {headers: cabecera});
  }

  postDomicilio(dom: Domicilio){
    let cabecera= new HttpHeaders({'Authorization':'Token '+ this.sesionServ.getToken(), 'Content-Type':'application/json'});
    let cuerpo= this.cuerpoConsultaDom(dom);

    return this._http.post(environment.linkUrl + '/domicilio/',
           JSON.stringify(cuerpo), {headers: cabecera});
  }

  cuerpoConsultaDom(domicilio: Domicilio){

    let cuerpo= {
            "domicilio": {
              "calle": domicilio.calle,
              "nroDomicilio": domicilio.nroDomicilio,
              "piso": domicilio.piso,
              "departamento": domicilio.departamento,
              "localidad_id": domicilio.localidad_id,
              "fecha_modificado": domicilio.fecha_modificado,
            }
    }
    return cuerpo;
  }

}

  export interface Domicilio{

    idDomicilio?: number,
    calle: string,
    nroDomicilio: number,
    departamento?: number,
    piso?: number,
    fecha_modificado?: string,
    localidad?: {
        idLocalidad: number,
        nombreLocalidad: string,
        fecha_modificado: string,
        provincia: number
        },
    localidad_id?: number
  }

  export interface Pais{
    idPais: number,
    nombrePais?: string,
    fecha_modificado?: string,
  }

  export interface Provincia{
    idProvincia: number,
    nombreProvincia?: string,
    fecha_modificado?: string,
    pais?: number
  }

  export interface Localidad{
    idLocalidad: number,
    nombreLocalidad?: string,
    fecha_modificado?: string,
    provincia?: number
  }

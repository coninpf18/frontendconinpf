import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { SesionService } from './sesion.service';

@Injectable({
  providedIn: 'root'
})

export class BackService {

  private cabecera = new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' });

  constructor(private _http: HttpClient, private sesionService: SesionService) {

  }

  getUser(usuarioPass: Object) {
    return this._http.post(environment.linkUrl + '/login/', JSON.stringify(usuarioPass), { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) });
  }

  //Cambiar nombre de los metodos

  //Envio de email para obtener token
  cambiarContraseña(emailProfesional: string) {

    let email = { email: emailProfesional };
    return this._http.post(environment.linkUrl + '/profesional/cambiarcontrasena/generartoken/', JSON.stringify(email), { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) });
  }

  //Envio de contraseña nueva con token
  cambioDeContraseña(token, contraseña) {
    let cuerpo: Contraseña = { token: token, pass_nueva: contraseña }
    console.log(JSON.stringify(cuerpo));
    return this._http.post(environment.linkUrl + '/profesional/cambiarcontrasena/confirmar/', JSON.stringify(cuerpo), { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) });

  }
  cambiarContraseñaVieja(passVieja, passNueva) {
    console.log(this.sesionService.getToken());

    let cuerpo: ContraseñaNuevaXVieja = { pass_vieja: passVieja, pass_nueva: passNueva }
    console.log(JSON.stringify(cuerpo));
    this.cabecera = new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' });
    return this._http.post(environment.linkUrl + '/cambiarpass/', cuerpo, { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) });

  }


}

interface Contraseña {
  token?: string,
  pass_nueva?: string
}
interface ContraseñaNuevaXVieja {
  pass_vieja?: string,
  pass_nueva?: string
}

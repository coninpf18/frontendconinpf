import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SesionService } from './sesion.service';
import { environment } from '../../environments/environment';
import { Subject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Paciente } from './paciente.service';

@Injectable({
  providedIn: "root"
})

export class AsistenciaService {

  //area-asistencia.
  subject: Subject<any> = new Subject<any>();
  observable: Observable<any> = this.subject.asObservable();

  //area-asistencia-fecha.
  subject2: Subject<any> = new Subject<any>();
  observable2: Observable<any> = this.subject2.asObservable();

  private cabecera = new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' });

  constructor(private _http: HttpClient, private sesionService: SesionService) {

  }

  getAsistencias(diaNombre: string) {
    return this._http.get(environment.linkUrl + '/paciente/asistencia/' + diaNombre + '/', { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) })
      .pipe(
        map(data => {
          return data['contenido'];
        })
      );
  }

  getResponsable(diaNombre: string) {
    return this._http.get(environment.linkUrl + '/paciente/asistencia/responsable/' + diaNombre + '/', { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) });
  }

  getInstanciaAsistencia(idPaciente: number) {
    return this._http.get(environment.linkUrl + '/asistencia/', { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) })
  }

  getCausasInasistencia() {
    return this._http.get(environment.linkUrl + '/causainasistencia/', { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) })
  }

  getAsistentesFecha(fecha: string) {
    return this._http.get(environment.linkUrl + '/paciente/asistencia/responsable/' + fecha + '/', { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) })
      .pipe(map(datos => {
        return datos['contenido'];
      }));
  }

  getAsistentesArea(fecha: string, idArea: number) {
    return this._http.get(environment.linkUrl + '/paciente/asistencia/responsable/' + fecha + '/area/' + idArea + '/', { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) })
      .pipe(map(datos => {
        return datos['contenido'];
      }));
  }

  getHistoricoAsistencia(idPac: number) {
    return this._http.get(environment.linkUrl + '/asistencia/paciente/' + idPac + '/', { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) })
      .pipe(map(datos => {
        return datos['contenido'];
      }));
  }

  postGenerarAsistente(idPaciente: number, idDia: number) {
    let cuerpo = {
      "diaPaciente": {
        "paciente_id": idPaciente,
        "dia_id": idDia
      }
    };

    return this._http.post(environment.linkUrl + '/diapaciente/', JSON.stringify(cuerpo), { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) })
  }

  postMarcarAsistencia(boolAsistio: boolean, fechaAsistencia: string, idPaciente: number) {
    let cuerpo = {
      "asistencia": {
        "paciente_id": idPaciente,
        "fechaAsistencia": fechaAsistencia,
        "asistio": boolAsistio
      }
    }

    return this._http.post(environment.linkUrl + '/asistencia/', JSON.stringify(cuerpo), { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) });
  }

  postMarcarAsistenciaArea(boolAsistio: boolean, fechaAsistencia: string, idPaciente: number, idArea: number) {
    let cuerpo = {
      "asistenciaArea": {
        "paciente_id": idPaciente,
        "fechaAsistencia": fechaAsistencia,
        "asistio": boolAsistio,
        "area_id": idArea
      }
    }

    return this._http.post(environment.linkUrl + '/asistencia/area/', JSON.stringify(cuerpo), { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) });
  }

  putEditarAsistencia(asistioBool: boolean, idAsistencia: number) {

    let cuerpo = {
      "asistencia": {
        "asistio": asistioBool
      }
    }

    return this._http.put(environment.linkUrl + '/asistencia/' + idAsistencia + '/', JSON.stringify(cuerpo), { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) })
  }

  putEditarCausaInasistencia(idCausaInasistencia: number, idAsistencia: number) {

    let cuerpo = {
      "asistencia": {
        "causaInasistencia": idCausaInasistencia
      }
    }

    return this._http.put(environment.linkUrl + '/asistencia/' + idAsistencia + '/', JSON.stringify(cuerpo), { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) });
  }

  putEditarDiaAsistencia(idPaciente: number, idDia: number, idDiaPaciente: number) {
    let cuerpo = {
      "diaPaciente": {
        "paciente_id": idPaciente,
        "dia_id": idDia
      }
    };
    return this._http.put(environment.linkUrl + '/diapaciente/' + idDiaPaciente + '/', JSON.stringify(cuerpo), { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) });
  }

  eliminarDiaAsistencia(idPaciente: number, idDia: number) {
    return this._http.delete(environment.linkUrl + '/diapaciente/' + idDia + '/' + idPaciente + '/', { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) });
  }
}

export interface Asistencia {
  idAsistencia?: number,
  paciente?: Paciente,
  fechaAsistencia: string,
  asistio: boolean,
  fecha_creado?: string,
  fecha_modificado?: string,
  causaInasistencia?: number

}

export interface CausaInasistencia {
  idCausaInasistencia: number,
  nombreCausaInasistencia: string,
}
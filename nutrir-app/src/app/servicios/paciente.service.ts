import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { SesionService } from './sesion.service';
import { environment } from "../../environments/environment";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators/';
import { Asistencia } from './asistencia.service';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {

  private paciente: Paciente;
  cabecera= new HttpHeaders({'Authorization':'Token '+ this.sesionServ.getToken(), 'Content-Type':'application/json'});
  cabeceraT= new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' });


  constructor(private _http:HttpClient, private sesionServ:SesionService) { }

    postPaciente(paciente: Paciente){
      let cuerpo= this.cuerpoConsulta(paciente);
      console.log(cuerpo);
      
      return this._http.post(environment.linkUrl + '/paciente/', JSON.stringify(cuerpo), {headers:new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })} );
    }

    putPaciente(paciente: Paciente){
      let cuerpo= this.cuerpoConsulta(paciente);
      console.log('desde el srvPaciente: ', cuerpo);
      return this._http.put(environment.linkUrl+'/paciente/'+ paciente.idPaciente + '/', JSON.stringify(cuerpo), {headers:new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })} );
    }

    putNuevoDomPaciente(paciente: Paciente){
      let cuerpo= this.cuerpoConsultaDom(paciente);
      return this._http.put(environment.linkUrl+'/paciente/'+ paciente.idPaciente + '/', JSON.stringify(cuerpo), {headers:new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })} );
    }

    getDetallePaciente(idPac: number){
      return this._http.get(environment.linkUrl+'/estadohistorial/paciente/actual/' + idPac + '/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })}).
            pipe(map((data: any)=> {
                this.paciente= data['estadoHistorial'].paciente;
              return data['estadoHistorial'];
            }));
    }

    getPacienteByID(idPac: number){
      return this._http.get(environment.linkUrl + '/estadohistorial/paciente/actual/' + idPac + '/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })}).
            pipe(map((data: any)=> {
              return data['estadoHistorial'].paciente;
            }));
    }
    //TipoPacienteEstado
    getTipoPacienteEstado(idPaciente){
      return this._http.get(environment.linkUrl + '/estadohistorial/paciente/actual/' + idPaciente + '/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
    }

    getTiposPaciente(){
      return this._http.get(environment.linkUrl+'/tipo/paciente/', {headers:new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })});
    }

    getPaciente(){
        let cabecera= new HttpHeaders({'Authorization':'Token ' + this.sesionServ.getToken()}); //Se arma una cabecera para poder hacer la peticion get cumpliendo con la autorizacion de Token
        return this._http.get(environment.linkUrl + '/paciente/',{headers: cabecera}).
              pipe(map(data => {              //map lo que hace es filtrar lo que nosotros queramos, en este caso el contenido queremos, dentro de todo el json crudo que devuelve
                return data['contenido'];
              }))

      }
    getPacienteIngresado(){
        let cabecera= new HttpHeaders({'Authorization':'Token ' + this.sesionServ.getToken()}); //Se arma una cabecera para poder hacer la peticion get cumpliendo con la autorizacion de Token
        return this._http.get(environment.linkUrl + '/paciente/ingresados',{headers: cabecera}).
              pipe(map(data => {              //map lo que hace es filtrar lo que nosotros queramos, en este caso el contenido queremos, dentro de todo el json crudo que devuelve
                return data['contenido'];
              }))

      }
    getDetallePacienteDos(termino: String){
          let cabecera= new HttpHeaders({'Authorization':'Token ' + this.sesionServ.getToken()}); //Se arma una cabecera para poder hacer la peticion get cumpliendo con la autorizacion de Token
          return this._http.get(environment.linkUrl + '/paciente/busqueda/' + termino + '/', {headers: cabecera}).
                pipe(map(data=> {              //map lo que hace es filtrar lo que nosotros queramos, en este caso el contenido queremos, dentro de todo el json crudo que devuelve
                  return data['contenido'];
                }))

        }
    
    getPacienteSemana(){
      return this._http.get(environment.linkUrl + '/paciente/semana/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })}).
        pipe(map(data => {
          return data['dias'];
        }))
    }

    getResponsable(idPaciente): Observable<Object>{
      return this._http.get(environment.linkUrl + '/paciente/responsable/' + idPaciente + '/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })});
    }

    getRelacionesPaciente(idPaciente){
      return this._http.get(environment.linkUrl + '/relacion/paciente/'+idPaciente+'/',{headers:new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })});
    }

    getPacNavbar(){
      return this.paciente;
    }

    setPacNavbar(pac: Paciente){
      this.paciente= pac;
    }

    calcularEdad(fechaNacimiento) {
      var fechaNacio: any = new Date(Date.parse(fechaNacimiento));
      var hoy: any = new Date();
      var tiempoPasado = hoy - fechaNacio;
      var segs = 1000;
      var mins = segs * 60;
      var hours = mins * 60;
      var days = hours * 24;
      var months = days * 30.416666666666668;
      var years = months * 12;
      var anios = Math.floor(tiempoPasado / years);
      tiempoPasado = tiempoPasado - (anios * years);
      var meses = Math.floor(tiempoPasado / months)
      tiempoPasado = tiempoPasado - (meses * months);
      var dias = Math.floor(tiempoPasado / days)
      var edadPaciente;
      if (anios<= 5) {
        if (anios === 1 && meses === 1 && dias === 1) {
          edadPaciente = anios + ' año, ' + meses + ' mes, ' + dias + ' día'
      } else {
        if (anios != 1 && meses === 1 && dias === 1) {
          edadPaciente = anios + ' años, ' + meses + ' mes, ' + dias + ' día'
        } else {
          if (anios != 1 && meses != 1 && dias === 1) {
            edadPaciente = anios + ' años, ' + meses + ' meses, ' + dias + ' día'
          } else {
            if(anios != 1 && meses === 1 && dias!= 1){  //Son 8 combinaciones posibles, faltaba una. Acá está agregada.
              edadPaciente = anios + ' años, ' + meses + ' mes, ' + dias + ' días'
            } else {
              if (anios === 1 && meses != 1 && dias === 1) {
                edadPaciente = anios + ' año, ' + meses + ' meses, ' + dias + ' día'
              } else {
                if (anios === 1 && meses != 1 && dias != 1) {
                  edadPaciente = anios + ' año, ' + meses + ' meses, ' + dias + ' días'
                } else {
                  if (anios === 1 && meses === 1 && dias != 1) {
                    edadPaciente = anios + ' año, ' + meses + ' mes, ' + dias + ' días'
                  } else {
                    edadPaciente = anios + ' años, ' + meses + ' meses, ' + dias + ' días'
                    }
                  }
                }
              }
            }
          }
        }

      }else{
        edadPaciente = anios + ' años'
      }
    
      return edadPaciente;
    }

    cuerpoConsulta(paciente: Paciente){
      let cuerpo= {
            "paciente":{
              "nombrePaciente": paciente.nombrePaciente,
              "apellidoPaciente": paciente.apellidoPaciente,
              "fechaNacimiento": paciente.fechaNacimiento + "T00:00:00-03:00",
              "dniPaciente": paciente.dniPaciente,
              "nroHistoriaClinica": paciente.nroHistoriaClinica,
              "telefonoPaciente": paciente.telefonoPaciente,
              "tipoPaciente_id": paciente.tipoPaciente_id,
              "pais": paciente.pais,
              "centro": paciente.centro,
              "sexo": paciente.sexo,
              "grupoFamilia": paciente.grupoFamilia,
              "domicilio_id": paciente.domicilio_id
            }
      }
      return cuerpo;
    }

    cuerpoConsultaDom(paciente: Paciente) {
      let cuerpo= {
        "paciente":{
          "domicilio_id": paciente.domicilio_id
        }
      }
      return cuerpo;
    }

}


export interface Paciente{
    idPaciente?: number,
    nombrePaciente?: string,
    apellidoPaciente?: string,
    nombreCompleto?:string
    dniPaciente?: number,
    asistencia?: Asistencia,
    asistencia_area?: number,
    fechaNacimiento?: string,
    nroHistoriaClinica?: string,
    telefonoPaciente?: string,
    tipoPaciente?: {
      idTipoPaciente: number,
      nombreTipoPaciente: string
    },
    tipoPaciente_id?: number,
    pais?: number,
    centro?: number,
    sexo?: number,
    responsable?: Paciente,
    grupoFamilia?: number,
    seleccionado?: boolean,
    domicilio_id?: number,
  }

export interface PacienteSemana {
  idDia: number,
  pacienteViene: boolean,
  pacientes?: Paciente [],
  nombreDia: string,
  fecha_creado: Date,
  fecha_modificado: Date
}

export interface TiposRelacion{
  idTipoRelacion: number,
  nombre: string,
  unico: boolean
}

export interface Relacion{
  idRelacion?: number,
  pacienteUno?: Paciente,
  pacienteDos?: Paciente,
  tipoRelacion?: TiposRelacion,
  tipoRelacion_id?: number,
  responsable?: boolean,
  pacienteUno_id?: number,
  pacienteDos_id?: number,
}

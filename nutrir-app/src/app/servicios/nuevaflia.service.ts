import { Injectable } from '@angular/core';
import { SesionService } from './sesion.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';


const linkUrl: string= 'http://francoance.pythonanywhere.com';
@Injectable({
  providedIn: 'root'
})

export class NuevafliaService {

  constructor(private sesionServ:SesionService, private _http:HttpClient) { }

  postFlia(apellido:String){
    console.log(this.sesionServ.getToken);
    let cabecera= new HttpHeaders({'Authorization':'Token '+ this.sesionServ.getToken(), 'Content-Type':'application/json'});
    let cuerpo = {
      "grupofamilia":  {
    "apellidoGrupoFamilia": apellido}
      }

    return this._http.post(environment.linkUrl + '/grupofamilia/',JSON.stringify(cuerpo) ,{headers: cabecera}) 

  }
}

import { Injectable } from '@angular/core';
import { SesionService } from './sesion.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CentroService {

  private centroid: number;
  cabecera= new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' });

  constructor (private sesionServ: SesionService, private _http: HttpClient) {

  }

  getCentros(){
    return this._http.get(environment.linkUrl+ '/centro/', {headers:new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
           .pipe(map(datos=> {
            return datos['contenido'];
    }));
  }

  getCentroId(id: number){
    return this._http.get(environment.linkUrl+ '/centro/' + id +'/', {headers:new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
           .pipe(map(datos=> {
            this.centroid= datos['centro'].idCentro;
            return datos['centro'];
    }));
  }

  getCentroidModProf(){
    return this.centroid;
  }

}

export interface Centro{
    idCentro: number,
    nombreCentro: string,
    fecha_creado: string,
    fecha_modificado: string,
    domicilio: number
}

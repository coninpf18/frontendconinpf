import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SesionService } from './sesion.service';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/internal/operators/map';
import { SelectItem } from 'primeng/api';


@Injectable({
providedIn: "root"
})


export class EstadisticaService {
chartService:Estadistica[]=[]
public fechaHoy: any = new Date().toISOString().split('Z')[0];;
cabecera = new HttpHeaders({'Authorization':'Token ' + this.sesionServ.getToken(), 'Content-Type':'application/json'});

constructor(private _http: HttpClient, private sesionServ: SesionService) {}

getEstadistica(periodo ,fechaDesde, fechaHasta) {
    // console.log("periodo", periodo, "fecha desde", fechaDesde, "fecha hasta", fechaHasta);
    return this._http.get(environment.linkUrl + '/estadistica/' + periodo + '/' + fechaDesde + '/' + fechaHasta + '/',{headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })}).
    pipe( map( data => { //map lo que hace es filtrar lo que nosotros queramos, en este caso el contenido queremos, dentro de todo el json crudo que devuelve
    return data['contenido'];
    }))
}
getEstadisticaId(idEstadistica, periodo ,fechaDesde, fechaHasta) {
    // console.log("periodo", periodo, "fecha desde", fechaDesde, "fecha hasta", fechaHasta);
    return this._http.get(environment.linkUrl + '/estadistica/' + periodo + '/' + fechaDesde + '/' + fechaHasta + '/' + idEstadistica + '/',{headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })}).
    pipe( map( data => { //map lo que hace es filtrar lo que nosotros queramos, en este caso el contenido queremos, dentro de todo el json crudo que devuelve
    return data['estadistica'];}))
    
}
getEstadisticaPorId(fechaDesde, fechaHasta, idEstadistica){
console.log("test");    
return this._http.get(environment.linkUrl + '/estadistica/' + fechaDesde + '/' + fechaHasta + '/' + idEstadistica + '/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })}).
pipe( map( data => { //map lo que hace es filtrar lo que nosotros queramos, en este caso el contenido queremos, dentro de todo el json crudo que devuelve
return data['estadistica.data'];

}))
}

getEstadisticaDiaria(fechaDesde, fechaHasta){
    return this._http.get(environment.linkUrl + '/estadistica/diaria'+fechaDesde+'/'+fechaHasta+'/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })}).
    pipe( map( data => { //map lo que hace es filtrar lo que nosotros queramos, en este caso el contenido queremos, dentro de todo el json crudo que devuelve
    return data['contenido'];    
    }))   
}

getEstadisticaMetadata() {

return this._http.get(environment.linkUrl + '/estadistica/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })}).
pipe( map( data => { //map lo que hace es filtrar lo que nosotros queramos, en este caso el contenido queremos, dentro de todo el json crudo que devuelve
return data['contenido'];

}))
}

calculoEstadistica(fechaDesde, fechaHasta){
    let test={}
    return this._http.post(environment.linkUrl + '/estadistica/calcular/' + fechaDesde + '/' + fechaHasta + '/',JSON.stringify(test), {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
}
setChart(chart:Estadistica[]){
this.chartService=chart
console.log(this.chartService);

}
getChart(){
    return this.chartService;
}

//A implementar.
// getPeriodo(){
//     let periodo:SelectItem[]=[
//         {label:'Diario', value:{id:1, name: 'Diario'}},
//         {label:'Semanal', value:{id:2, name: 'Semanal'}},
//         {label:'Quincenal', value:{id:3, name: 'Quincenal'}},
//         {label:'Mensual', value:{id:3, name: 'Mensual'}},
//         {label:'Anual', value:{id:3, name: 'Anual'}}
//       ]
//     return periodo

// }
}

export interface Estadistica{
data?: Array<{ name:string, value:number}>,
// data:Array<string>,
nombre?:string,
xAxisLabel?:string,
yAxisLabel?:string,

// totalValor?:number,
// valorMinimo?:number,
// valorMaximo?:number,
// promedio?:number,
// ultimo?:number
}

export interface EstadisticaMetadata{
idEstadistica?:number,
nombreEstadistica:string,
tipoEstadistica:number

}

export interface EstadisticaMultiSelect{
    name:string,
    // value:number
}

export interface Chart{
 labels:string[],
 datasets:{
     label:string,
     data:Int16Array[],
     fill:boolean,
     borderColor:string
 }
}
export interface EstadisticaCabecera{
    nombreEstadistica:string,
    valorEstadistica:number,
    valorEstadisticaUltMes:number,
    valorDiferenciaUltMes:number
}
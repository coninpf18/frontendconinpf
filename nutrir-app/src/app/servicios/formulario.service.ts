import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { SesionService } from './sesion.service';
import { environment } from "../../environments/environment";
import { map } from 'rxjs/operators/';

@Injectable({
  providedIn: 'root'
})

export class FormularioService {

  cabecera= new HttpHeaders({'Authorization':'Token '+ this.sesionServ.getToken(), 'Content-Type':'application/json'});

  constructor(private _http:HttpClient, private sesionServ:SesionService) { }

  getFormularios(){
    let cabecera= new HttpHeaders({'Authorization':'Token '+this.sesionServ.getToken()}); //Se arma una cabecera para poder hacer la peticion get cumpliendo con la autorizacion de Token
    return this._http.get(environment.linkUrl + '/formulario/',{headers: cabecera}).
          pipe( map( data => {              //map lo que hace es filtrar lo que nosotros queramos, en este caso el contenido queremos, dentro de todo el json crudo que devuelve
            return data['contenido'];
          }))
  }

  getAreaFormularios(){
    let cabecera= new HttpHeaders({'Authorization':'Token '+this.sesionServ.getToken()}); //Se arma una cabecera para poder hacer la peticion get cumpliendo con la autorizacion de Token
    return this._http.get(environment.linkUrl + '/areaformulario/',{headers: cabecera}).
          pipe( map( data => {              //map lo que hace es filtrar lo que nosotros queramos, en este caso el contenido queremos, dentro de todo el json crudo que devuelve
            return data['contenido'];
          }))
  }

  getFormularioPermiso(idFormulario, idPaciente){ //esta es la llamada mas larga do mundo, trae hasta la factura del gas
    ///verformulario/$idform/paciente/$idpac/
    let cabecera= new HttpHeaders({'Authorization':'Token '+this.sesionServ.getToken()}); //Se arma una cabecera para poder hacer la peticion get cumpliendo con la autorizacion de Token
    return this._http.get(environment.linkUrl + '/verformulario/'+idFormulario+'/paciente/'+idPaciente+'/',{headers: cabecera}).pipe(map(data => {return data['contenido']}))
    //aca tenemos que traer la info necesaria solamente
  }

  getDetalleFormulario(idFormulario:number){
    let cabecera= new HttpHeaders({'Authorization':'Token '+this.sesionServ.getToken()}); //Se arma una cabecera para poder hacer la peticion get cumpliendo con la autorizacion de Token
    return this._http.get(environment.linkUrl + '/formulario/'+idFormulario+'/',{headers: cabecera}).
          pipe( map( data => {              //map lo que hace es filtrar lo que nosotros queramos, en este caso el contenido queremos, dentro de todo el json crudo que devuelve
            return data['formulario'];

          }))
  }

  getAtributoFormulario(idFormulario:number){
    let cabecera= new HttpHeaders({'Authorization':'Token '+this.sesionServ.getToken()}); //Se arma una cabecera para poder hacer la peticion get cumpliendo con la autorizacion de Token
    return this._http.get(environment.linkUrl + '/formatr/formulario/'+idFormulario+'/',{headers: cabecera}).
          pipe( map( data => {              //map lo que hace es filtrar lo que nosotros queramos, en este caso el contenido queremos, dentro de todo el json crudo que devuelve
            return data['contenido'];

          }))
  }

  getAtributoValorFormulario(idFormulario:number){
    let cabecera= new HttpHeaders({'Authorization':'Token '+this.sesionServ.getToken()}); //Se arma una cabecera para poder hacer la peticion get cumpliendo con la autorizacion de Token
    return this._http.get(environment.linkUrl + '/formatr/formulario/'+idFormulario+'/',{headers: cabecera}).
          pipe( map( data => {              //map lo que hace es filtrar lo que nosotros queramos, en este caso el contenido queremos, dentro de todo el json crudo que devuelve
            return data['contenido'];

          }))
  }

  getFormularioByArea(id: number){
    return this._http.get(environment.linkUrl + '/formulario/area/' + id, {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
            .pipe(map(datos=> {
                return datos['contenido'];
        }));
  }

  postForm(posteo:any){
    return this._http.post(environment.linkUrl + '/pacienteatrform/', JSON.stringify(posteo),  {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })});
  }

  cambiarEstadoPac(paciente_id:number, estado:String){
    return this._http.post(environment.linkUrl + '/estado/paciente/'+paciente_id+'/'+estado+'/',JSON.stringify("{}"),{headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
    .pipe(map(datos=> {
        return datos;
    }));
  }

  cambiarEstadoForm(paciente_id:number,form_id:number, estado:String){
    return this._http.post(environment.linkUrl + '/estado/formulario/'+form_id+'/'+paciente_id+'/'+estado+'/',JSON.stringify("{}"), {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
    .pipe(map(datos=> {
        return datos;
    }));
  }

  getEstadoActual(paciente_id){
    return this._http.get(environment.linkUrl + '/estadohistorial/paciente/actual/'+paciente_id+'/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
    .pipe(map(datos=> {
      return datos;
  }));
  }

  getValorFormAtrPac(idPac,idFormAtr){
    return this._http.get(environment.linkUrl + '/pacienteatrform/atr/'+idFormAtr+'/pac/'+idPac+'/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
    .pipe(map(datos=> {
      return datos;
  }));
  }

  getFormsPacProf(paciente_id:number){
    return this._http.get(environment.linkUrl + '/formulario/paciente/'+paciente_id+'/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
    .pipe(map(datos=> {
        return datos['contenido'];
    }));
  }

}
export interface Formulario{
  idFormulario?:number
  fecha_creado?: Date, // Los dejo tipo date pero puede fallar, en area los dejé any, no sé si ts los gestiona.
  fecha_modificado?: Date,
  tipoFormulario: {
      idTipoFormulario?: number,
      nombreTipoFormulario: String,
      fecha_creado?: Date, // Los dejo tipo date pero puede fallar, en area los dejé any, no sé si ts los gestiona.
      fecha_modificado?: Date,
      },
  codFormulario: String
  nombreFormulario: String,
}

export interface FormAtr{
  idFormAtr?:number
  tipoAtributo: {
    idTipoAtributo?: number
    nombreTipoAtributo: String
  }
  atributo:{
    idAtributoFormulario?: number
    nombreAtributoFormulario: String
    tipoDatoAtributo: String
  }
  obligatorio: boolean
  formulario: number
}

export interface AreaFormulario{
  area: {
    idArea: number
    nombreArea: String
  }
  formulario: {
    codFormulario:number
    idFormulario?: number
    nombreFormulario: String
    tipoFormulario: {
      idTipoFormulario: number
      nombreTipoFormulario: String
    }
    idAreaFormulario?: number
  }
}

 export interface AreaForm {
  area: String;
  formularios:Array<any>;
}

 export interface FormatrValor{
  idFormAtr?:number,
  nombreAtributo:String
  valorAtributo:String[]
 }

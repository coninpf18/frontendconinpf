import { Injectable } from '@angular/core';
import { SesionService } from './sesion.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class AreaService {

  cabecera = new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken() });

  constructor(private sesionService: SesionService, private _http: HttpClient) {

  }


  getAreas() {
    return this._http.get(environment.linkUrl + '/area/', { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) })
      .pipe(map(datos => {
        return datos['contenido'];
      }));
  }

  getAreaProfesional(idAreaProfesional: number) {
    return this._http.get(environment.linkUrl + '/area/' + idAreaProfesional + '/', { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) })

  }

  getAreaPrincipal() {
    return this._http.get(environment.linkUrl + '/area/principal/profesional/', { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionService.getToken(), 'Content-Type': 'application/json' }) })
      .pipe(map(datos => {
        return datos['contenido'];
      }));
  }

}

export interface Area {
  idArea: number,
  nombreArea: String,
  fecha_creado: any, //deberia ser date.
  fecha_modificado: any //deberia ser date. dejemoslo asi para probar.

}

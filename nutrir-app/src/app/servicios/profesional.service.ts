import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SesionService } from './sesion.service';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ProfesionalService {

  private sexoid: number;
  private rolid: number;
  private cabeceraToken = new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken() });
  private cabecera = new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), "Content-Type": "application/json" });

  constructor(private sesionServ: SesionService, private _http: HttpClient) {

  }

  getSexoId(id: number) {
    return this._http.get(environment.linkUrl + '/sexo/' + id, { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' }) })
      .pipe(map(datos => {
        this.sexoid = datos['Sexo'].idSexo;
        return datos['Sexo'];
      }));
  }

  getSexo() { //Trae todos los sexos.

    return this._http.get(environment.linkUrl + '/sexo/', { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' }) })
      .pipe(map(datos => {
        return datos['contenido'];
      }));
  }

  getRol() {
    return this._http.get(environment.linkUrl + '/rol/', { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' }) })
      .pipe(map(datos => {
        return datos['contenido'];
      }));
  }

  getRolId(idRol: number) {
    return this._http.get(environment.linkUrl + '/rol/' + idRol + '/', { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' }) })
      .pipe(map(datos => {
        this.rolid = datos['Rol'].idRol;
        return datos['Rol'];
      }));

  }

  getRolidModProf() {
    return this.rolid;
  }

  getsexoidModProf() {
    return this.sexoid;
  }

  getProfesionalid(id: number){
    return this._http.get(environment.linkUrl + '/profesional/' + id + '/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
          .pipe(map(datos=> {
            return datos['Profesional'];
        }));
  }

  //------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------------------------------------------------------------------------

  putProfesional(profesional: Profesional) {

    const cuerpo = this.cuerpoProfesional(profesional);

    return this._http.put(environment.linkUrl + '/profesional/' + profesional.idProfesional + '/',
      JSON.stringify(cuerpo), { headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' }) });
  }

  cuerpoProfesional(profesional: Profesional) {
    let cuerpo = {
      profesional: {
        nombreProfesional: profesional.nombreProfesional,
        apellidoProfesional: profesional.apellidoProfesional,
        dniProfesional: profesional.dniProfesional,
        fechaNacimiento: profesional.fechaNacimiento,
        matriculaProfesional: profesional.matriculaProfesional,
        telefonoProfesional: profesional.telefonoProfesional,
        tituloProfesional: profesional.tituloProfesional,
        centro: profesional.centro,
        domicilio: profesional.domicilio,
        sexo: profesional.sexo
      }
    }
    
    return cuerpo;
  }

}

export interface Profesional {
  idProfesional: number,
  modulos?: string[],
  user?: {
    username: string
  },
  nombreProfesional: string,
  apellidoProfesional: string,
  dniProfesional: number,
  fechaNacimiento: Date,
  matriculaProfesional: number,
  telefonoProfesional: number,
  tituloProfesional: number,
  fecha_creado?: string,
  fecha_modificado?: string,
  centro: number,
  domicilio: number,
  sexo: number
}

export interface Rol {
  idRol: number,
  nombreRol: string,
  fecha_creado?: string,
  fecha_modificado?: string
}

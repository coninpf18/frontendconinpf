import { Injectable } from '@angular/core';
import { SesionService } from './sesion.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { PacienteService } from './paciente.service';
// import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})

export class FamiliaService {

  public cabeceraT= new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' });
  public cabecera= new HttpHeaders({'Authorization':'Token '+ this.sesionServ.getToken(), 'Content-Type':'application/json'});

  public relacionesFamEditar: any;
  public familia: Familia;

  constructor(private sesionServ:SesionService, private _http:HttpClient, private _pacSrv:PacienteService) { }

  postFlia(apellido: String){
    let cuerpo = {
      "grupofamilia":  {
      "apellidoGrupoFamilia": apellido}
      }

    return this._http.post(environment.linkUrl  + '/grupofamilia/',JSON.stringify(cuerpo) ,{headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
  }

  getFlia(idFam: number){
    return this._http.get(environment.linkUrl + '/grupofamilia/' + idFam + '/' ,{headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })});  
  }

  getTodasFamilias(){
    return this._http.get(environment.linkUrl + '/grupofamilia/' , {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })})
      .pipe(map(data => {
        return data['contenido'];
      }));
  }

  getFliaTermino(termino: String){
    return this._http.get(environment.linkUrl +'/grupofamilia/'+termino+'/' ,{headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })}).pipe(map(data => {              //map lo que hace es filtrar lo que nosotros queramos, en este caso el contenido queremos, dentro de todo el json crudo que devuelve
      return data['contenido'];
    }));
  }
  getTiposRelacion(){
    return this._http.get(environment.linkUrl +'/tipo/relacion/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })}).pipe(map(datos=> {
      return datos['contenido'];
    }))
  }
  getRelFamiliares(idFam: number){
    return this._http.get(environment.linkUrl +'/relacion/familia/' + idFam + '/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })}).pipe(map(datos=> {
      return datos['contenido'];
    }))
  }
  postRelacion(relpost: Relacion){
    let cuerpo= this.cuerpoConsulta(relpost);
    return this._http.post(environment.linkUrl +'/relacion/', JSON.stringify(cuerpo), {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })});
  }

  putRelacion(relpost: Relacion, idrel: number){
    let cuerpo= this.cuerpoConsulta(relpost);
    return this._http.put(environment.linkUrl +'/relacion/' + idrel + '/', JSON.stringify(cuerpo), {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })});
  }

  deleteRelacion(idxRel: number){
    return this._http.delete(environment.linkUrl + '/relacion/' + idxRel + '/', {headers: new HttpHeaders({ 'Authorization': 'Token ' + this.sesionServ.getToken(), 'Content-Type': 'application/json' })});
  }

  cuerpoConsulta(relacion: Relacion): any{
    let cuerpoJson= {
              "relacion": {
                  "responsable": relacion.responsable,
                  "pacienteUno_id": relacion.pacienteUno_id,
                  "pacienteDos_id": relacion.pacienteDos_id,
                  "tipoRelacion_id": relacion.tipoRelacion_id
                          }
                    }
    return cuerpoJson;
  }
}

export interface Familia{
  idGrupoFamilia?: number;
  integrantesFam?: Array <Integrante>;    //cambie el nombre initegrantes--> integrantesFam.
  apellidoGrupoFamilia: string;
  fecha_creado?: string;
  fecha_modificado?: string
}

export interface Integrante{
  nombre:  string;
  idPaciente: number;
  tipoPaciente:	{idTP:number, nombreTP: string}
  apellido: string
  dni: string
  fechaNac: string
  nHC: number
  telef: string
  pais: number
  centro: number
  sexo: number
  responsable?:string
  domicilio?
}

export interface TiposRelacion{
  idTipoRelacion: number,
  nombre: string,
  unico: boolean
}

export interface Relacion{
  idRelacion?: number,
  paciente1?: Integrante,
  paciente2?: Integrante,
  pacienteUno?: Integrante,
  pacienteDos?: Integrante,
  tipoRelacion?: TiposRelacion,
  tipoRelacion_id?: number,
  responsable?: boolean,
  pacienteUno_id?: number,
  pacienteDos_id?: number,
  fecha_creado?: string,
  fecha_modificado?: string
}

import { Injectable } from '@angular/core';
import { Profesional } from '../servicios/profesional.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class SesionService {

  private token: string;
  private sexoProf: number;
  private prof: Profesional;
  public nombreModulo: string;

  constructor(private router: Router) {

    if (this.token == undefined) {
      this.setToken(sessionStorage.getItem('token'));
      this.setProfesional(JSON.parse(sessionStorage.getItem('profesional')));
    }

  }

  logout() {

    // puse un timeout xq al trabajar solo en memoria desloguea muy rápido.

    setTimeout(() => {
    sessionStorage.clear();
    this.router.navigate(['/login'])
    }, 250);
    
  }

  setProfesional(profesional: Profesional) {
    this.prof = profesional;
  }

  getProfesional() {
    return JSON.parse(sessionStorage.getItem('profesional'));
  }

  getToken(): string {
    return sessionStorage.getItem('token');
  }

  setToken(tkn: string) {
    this.token = tkn;
  }

  setSexoProf(usu: number) {
    this.sexoProf = usu;
  }

  getSexoProf() {
    return this.sexoProf;
  }

  getModulos() {
    return this.prof.modulos;
  }

  getNombreModulo() {
    return this.nombreModulo = sessionStorage.getItem('ubicacion');
  }

  setNombreModulo(modulo: string) {
    this.nombreModulo = modulo;
    sessionStorage.setItem('ubicacion', this.nombreModulo);
  }

  setProf(prof: Profesional) {
    sessionStorage.setItem('profesional', JSON.stringify(prof));
  }

  getProf(): Profesional {
    return JSON.parse(sessionStorage.getItem('profesional'));
  }
}